package com.softdesign.goodshop.ui.activities

import android.os.Bundle
import com.softdesign.goodshop.di.DaggerService
import com.softdesign.goodshop.di.scopes.DaggerScope
import com.softdesign.goodshop.di.screens.SplashViewComponent
import com.softdesign.goodshop.di.screens.SplashViewModule
import com.softdesign.goodshop.mvp.presenters.interfaces.ISplashPresenter
import com.softdesign.goodshop.mvp.views.screens.ISplashView

/**
 * @author Space
 * @date 11.12.2016
 */

@DaggerScope(ISplashView::class)
class SplashActivity : BaseActivity<ISplashPresenter, ISplashView>(), ISplashView {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        DaggerService.getComponent(SplashViewComponent::class.java, SplashViewModule::class.java).inject(this)
    }
}
