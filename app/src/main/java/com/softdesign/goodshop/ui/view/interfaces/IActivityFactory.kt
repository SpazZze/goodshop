package com.softdesign.goodshop.ui.view.interfaces

import android.content.Context
import com.softdesign.goodshop.ui.activities.AuthActivity
import com.softdesign.goodshop.ui.activities.RootActivity
import com.softdesign.goodshop.utils.extentions.showAppSettings
import org.jetbrains.anko.startActivity


/**
 * @author Space
 * @date 02.02.2017
 */

interface IActivityFactory {

    val ctx: Context

    fun showAuthScreen() = ctx.startActivity<AuthActivity>()

    fun showRootScreen() = ctx.startActivity<RootActivity>()

    fun showAppSettings() = ctx.showAppSettings()
}