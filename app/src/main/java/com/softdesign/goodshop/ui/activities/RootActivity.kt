package com.softdesign.goodshop.ui.activities

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.FragmentManager
import android.support.v4.view.GravityCompat
import android.support.v4.view.MenuItemCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.view.Menu
import android.view.MenuItem
import android.widget.TextView
import com.github.spazzze.exto.extensions.isBackStackEmpty
import com.softdesign.goodshop.R
import com.softdesign.goodshop.databinding.ContentNavHeaderRootBinding
import com.softdesign.goodshop.di.DaggerService
import com.softdesign.goodshop.di.modules.FragmentManagerModule
import com.softdesign.goodshop.di.scopes.RootScope
import com.softdesign.goodshop.di.screens.RootScreenComponent
import com.softdesign.goodshop.di.screens.RootScreenModule
import com.softdesign.goodshop.mvp.models.NavHeaderModel
import com.softdesign.goodshop.mvp.presenters.interfaces.IRootPresenter
import com.softdesign.goodshop.mvp.views.screens.IRootView
import com.softdesign.goodshop.ui.view.elements.ToolbarBuilder
import kotlinx.android.synthetic.main.activity_root.*
import javax.inject.Inject

/**
 * @author Space
 * @date 11.12.2016
 */

@RootScope(RootScope::class)
class RootActivity : BindingActivity<IRootPresenter, IRootView, NavHeaderModel, ContentNavHeaderRootBinding>(), IRootView {

    @Inject
    lateinit override var presenter: IRootPresenter

    override val sfm: FragmentManager by lazy { supportFragmentManager }
    override val containerId = R.id.container

    override val drawerLayout: DrawerLayout by lazy { drawer }
    override val navMenu: Menu by lazy { navView.menu }

    private val toolbarBuilder: ToolbarBuilder by lazy {
        setSupportActionBar(toolbar)
        val drawerToggle = ActionBarDrawerToggle(this, drawer, toolbar, R.string.open_drawer, R.string.close_drawer)
        ToolbarBuilder(drawer, drawerToggle, supportActionBar!!, tb_titleIcon) { onBackPressed() }
    }

    private var tv_cartItemsCount: TextView? = null

    //region :::::::::::::::::::::::::::::::::: Life cycle
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_root)
        initToolbar()
        DaggerService.getComponent(RootScreenComponent::class.java,
                arrayOf<Any>(FragmentManagerModule(this)), RootScreenModule::class.java).inject(this)
        binding = DataBindingUtil.bind<ContentNavHeaderRootBinding>(navView.getHeaderView(0))!!
        navView.setNavigationItemSelectedListener(this)
        if (isBackStackEmpty()) showCatalogScreen()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.toolbar_root, menu)
        val item = MenuItemCompat.setActionView(menu.findItem(R.id.menu_cart), R.layout.item_menu_cart)
        tv_cartItemsCount = MenuItemCompat.getActionView(item)?.findViewById(R.id.menu_cart_count) as? TextView
        updateCartProductCounter(presenter.getCartItemsCount())
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.menu_cart -> showCartScreen()
        else -> super.onOptionsItemSelected(item)
    }

    //endregion :::::::::::::::::::::::::::::::::: Life cycle
    private fun initToolbar() {
        toolbarBuilder.build()
    }

    override fun setupToolbar(title: String, isDrawerLocked: Boolean) {
        toolbarBuilder.init()
                .setTitle(title)
                .setDrawerLocked(isDrawerLocked)
                .build()
    }

    override fun onBackPressed() {
        if (closeDrawer()) return
        if (!onFragmentBackPressed()) super.onBackPressed()
        if (isBackStackEmpty()) navMenu.findItem(R.id.nav_catalog).isChecked = true
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        val redirected = when (item.itemId) {
            R.id.nav_catalog -> showCatalogScreen()
            R.id.nav_favourites -> showFavouritesScreen()
            R.id.nav_cart -> showCartScreen()
            R.id.nav_options -> {
                showAppSettings(); false
            }
            R.id.nav_logout -> {
                presenter.logout(); false
            }
            R.id.nav_account -> when (presenter.checkUserAuth()) {
                true -> showAccountScreen()
                false -> {
                    showAuthScreen(); false
                }
            }
            else -> {
                showMessage(getString(R.string.notify_not_implemented)); false
            }
        }
        drawerLayout.closeDrawer(GravityCompat.START)
        return redirected
    }

    override fun showMessage(message: String) = Snackbar.make(coordinator, message, Snackbar.LENGTH_LONG).show()

    override fun updateCartProductCounter(count: Int) = when (count < 1000) {
        true -> tv_cartItemsCount?.text = count.toString()
        else -> tv_cartItemsCount?.text = "..."
    }
}