package com.softdesign.goodshop.ui.activities

import android.databinding.ViewDataBinding
import com.softdesign.goodshop.BR
import com.softdesign.goodshop.mvp.models.abs.BaseModel
import com.softdesign.goodshop.mvp.presenters.abs.interfaces.IBindingPresenter
import com.softdesign.goodshop.mvp.views.IBindingView

/**
 * @author Space
 * @date 05.01.2017
 */

abstract class BindingActivity<
        P : IBindingPresenter<V, M>,
        V : IBindingView<M>,
        M : BaseModel,
        B : ViewDataBinding> : BaseActivity<P, V>(), IBindingView<M> {

    lateinit open var binding: B

    override fun setViewModel(model: M) {
        binding.setVariable(BR.viewModel, model)
    }
}