package com.softdesign.goodshop.ui.view.interfaces

import android.support.v4.app.FragmentManager
import com.github.spazzze.exto.extensions.clearBackStack
import com.github.spazzze.exto.extensions.replaceFragment
import com.softdesign.goodshop.data.storage.dto.Address
import com.softdesign.goodshop.data.storage.dto.Comment
import com.softdesign.goodshop.data.storage.dto.Product
import com.softdesign.goodshop.ui.fragments.*

/**
 * @author Space
 * @date 31.01.2017
 */

interface IFragmentFactory {

    val sfm: FragmentManager

    val containerId: Int

    fun showCatalogScreen() =
            sfm.replaceFragment(containerId, false, "CatalogScreen") { CatalogFragment.newInstance() }

    fun showAccountScreen(addToBackStack: Boolean = true, tag: String? = "AccountScreen") =
            sfm.apply { clearBackStack() }
                    .replaceFragment(containerId, addToBackStack, tag) { AccountFragment.newInstance() }

    fun showFavouritesScreen(addToBackStack: Boolean = true, tag: String? = "FavouritesScreen") =
            sfm.apply { clearBackStack() }
                    .replaceFragment(containerId, addToBackStack, tag) { FavouritesFragment.newInstance() }

    fun showCartScreen(addToBackStack: Boolean = true, tag: String? = "CartScreen") =
            sfm.apply { clearBackStack() }
                    .replaceFragment(containerId, addToBackStack, tag) { CartFragment.newInstance() }

    fun showAddAddressScreen(addToBackStack: Boolean = true, tag: String? = "AddressScreen") =
            sfm.replaceFragment(containerId, addToBackStack, tag) { AddressFragment.newInstance() }

    fun showEditAddressScreen(address: Address, addToBackStack: Boolean = true, tag: String? = "AddressScreen") =
            sfm.replaceFragment(containerId, addToBackStack, tag) { AddressFragment.newInstance(address) }

    fun showCommentAddScreen(productId: String, addToBackStack: Boolean = true, tag: String? = "CommentAddScreen") =
            sfm.replaceFragment(containerId, addToBackStack, tag) { CommentAddFragment.newInstance(productId) }

    fun showCommentDetailsScreen(comment: Comment, addToBackStack: Boolean = true, tag: String? = "CommentDetailsScreen") =
            sfm.replaceFragment(containerId, addToBackStack, tag) { CommentFragment.newInstance(comment) }

    fun showProductInfoScreen(product: Product, addToBackStack: Boolean = true, tag: String? = "ProductInfoScreen") =
            sfm.replaceFragment(containerId, addToBackStack, tag) { ProductInfoFragment.newInstance(product) }

    fun onFragmentBackPressed(): Boolean = (sfm.findFragmentById(containerId) as? INavigableFragment)?.run { onBackPressed() } ?: false
}