package com.softdesign.goodshop.ui.view.interfaces

/**
 * @author Space
 * @date 21.02.2017
 */

interface INavigableFragment {

    fun onBackPressed(): Boolean
}