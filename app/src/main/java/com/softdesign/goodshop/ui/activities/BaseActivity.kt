package com.softdesign.goodshop.ui.activities

import android.content.Context
import android.support.annotation.CallSuper
import android.support.annotation.StringRes
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import com.github.spazzze.exto.extensions.isBackStackEmpty
import com.softdesign.goodshop.R
import com.softdesign.goodshop.di.DaggerService
import com.softdesign.goodshop.mvp.presenters.abs.interfaces.IBasePresenter
import com.softdesign.goodshop.mvp.views.IBaseView
import com.softdesign.goodshop.utils.extentions.networkErrorMsg
import org.jetbrains.anko.longToast
import org.jetbrains.anko.toast
import timber.log.Timber
import java.io.IOException
import javax.inject.Inject

/**
 * @author Space
 * @date 11.12.2016
 */


abstract class BaseActivity<P : IBasePresenter<V>, V : IBaseView> : AppCompatActivity(), IBaseView {

    @Inject
    lateinit open var presenter: P

    override val ctx: Context
        get() = this

    //region :::::::::::::::::::::::::::::::::: LifeCycle
    @Suppress("UNCHECKED_CAST")
    @CallSuper
    override fun onStart() {
        super.onStart()
        presenter.takeView(this as V)
        presenter.initView()
    }

    @CallSuper
    override fun onStop() {
        presenter.dropView()
        if (isFinishing) onFinish()
        super.onStop()
    }

    @CallSuper
    override fun onDestroy() {
        if (isFinishing) onFinish()
        super.onDestroy()
    }

    private fun onFinish() {
        presenter.onDestroy()
        DaggerService.unregisterScope(this.javaClass.declaredAnnotations)
    }
    //endregion :::::::::::::::::::::::::::::::::: LifeCycle

    override fun onBackPressed() = when (isBackStackEmpty()) {
        true -> showDialog(R.string.notify_confirm_exit_application, { super.onBackPressed() })
        else -> super.onBackPressed()
    }

    override fun handleBadIntent(key: String) = Timber.e(IOException(), "$javaClass handleBadIntent error: wrong args by key $key")
            .run { finish() }

    override fun showMessage(message: String) = longToast(message)

    override fun showMessage(@StringRes messageId: Int) = showMessage(getString(messageId))

    override fun showNotification(message: String) = toast(message)

    override fun showNotification(@StringRes messageId: Int) = showNotification(getString(messageId))

    override fun showError(@StringRes messageId: Int, e: Throwable) {
        Timber.e(e, "$javaClass showError error: ${getString(messageId)}")
        showMessage(messageId)
    }

    override fun showNetworkError(t: Throwable) = with(t.networkErrorMsg()) {
        if (isNotBlank()) showMessage(this)
    }

    override fun showNetworkError(messageId: Int, t: Throwable) = with(t.networkErrorMsg()) {
        when (isBlank()) {
            true -> showMessage(messageId)
            else -> showMessage(getString(messageId).plus(": $this"))
        }
    }

    override fun showDialog(messageId: Int, onPositive: () -> Unit, onCancel: () -> Unit) = AlertDialog.Builder(this)
            .setMessage(messageId)
            .setCancelable(true)
            .setPositiveButton(android.R.string.ok) { _, _ -> onPositive() }
            .setNegativeButton(android.R.string.cancel) { d, _ -> d.cancel() }
            .setOnCancelListener { onCancel() }
            .create()
            .show()
}