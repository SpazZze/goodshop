package com.softdesign.goodshop.ui.fragments

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.github.spazzze.exto.extensions.readArgs
import com.softdesign.goodshop.R
import com.softdesign.goodshop.data.storage.dto.Comment
import com.softdesign.goodshop.databinding.FragmentCommentBinding
import com.softdesign.goodshop.di.DaggerService
import com.softdesign.goodshop.di.scopes.RootScope
import com.softdesign.goodshop.di.screens.CommentDetailsComponent
import com.softdesign.goodshop.di.screens.CommentDetailsModule
import com.softdesign.goodshop.mvp.models.CommentModel
import com.softdesign.goodshop.mvp.presenters.interfaces.ICommentPresenter
import com.softdesign.goodshop.mvp.views.screens.ICommentView
import com.softdesign.goodshop.ui.fragments.abs.RootChildFragment
import org.jetbrains.anko.support.v4.withArguments

/**
 * @author Space
 * @date 05.01.2017
 */

@RootScope(ICommentView::class)
class CommentFragment :
        RootChildFragment<ICommentPresenter, ICommentView, CommentModel, FragmentCommentBinding>(),
        ICommentView {

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val comment = readArgs<Comment>(PARCELABLE_KEY) { handleBadIntent(PARCELABLE_KEY) }
        if (!comment.isDefined()) return null
        DaggerService.getComponent(CommentDetailsComponent::class.java,
                arrayOf<Any>(CommentDetailsModule(comment.get()))).inject(this)
        setHasOptionsMenu(true)
        binding = DataBindingUtil.inflate<FragmentCommentBinding>(inflater!!, R.layout.fragment_comment, container, false)
        return binding.root
    }

    override fun setupToolbar() = presenter.setupToolbar()

    companion object {
        private val PARCELABLE_KEY = "PARCELABLE_KEY_COMMENT_SCREEN"
        fun newInstance(comment: Comment) = CommentFragment().withArguments(PARCELABLE_KEY to comment)
    }
}