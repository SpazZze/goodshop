package com.softdesign.goodshop.ui.fragments

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.github.spazzze.exto.extensions.readArgs
import com.softdesign.goodshop.R
import com.softdesign.goodshop.common.App
import com.softdesign.goodshop.databinding.FragmentCommentsListBinding
import com.softdesign.goodshop.di.DaggerService
import com.softdesign.goodshop.di.screens.CommentsListViewComponent
import com.softdesign.goodshop.di.screens.CommentsListViewModule
import com.softdesign.goodshop.mvp.models.CommentsListModel
import com.softdesign.goodshop.mvp.presenters.interfaces.ICommentsListPresenter
import com.softdesign.goodshop.mvp.views.screens.ICommentsListView
import com.softdesign.goodshop.ui.fragments.abs.RootChildFragment
import org.jetbrains.anko.support.v4.withArguments

/**
 * @author Space
 * @date 19.12.2016
 */

class CommentsListFragment : RootChildFragment<ICommentsListPresenter, ICommentsListView, CommentsListModel, FragmentCommentsListBinding>(), ICommentsListView {

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val productId = readArgs(PARCELABLE_KEY, "", { handleBadIntent(PARCELABLE_KEY) })
        if (productId.isBlank()) return null
        DaggerService.getComponent(CommentsListViewComponent::class.java,
                arrayOf<Any>(CommentsListViewModule(productId))).inject(this)
        binding = DataBindingUtil.inflate<FragmentCommentsListBinding>(inflater!!, R.layout.fragment_comments_list, container, false)
        return binding.root
    }

    companion object {
        val TITLE = App.ctx.getString(R.string.header_comments).capitalize()
        private val PARCELABLE_KEY = "PARCELABLE_KEY_PRODUCT_COMMENTS"
        fun newInstance(productId: String) = CommentsListFragment().withArguments(PARCELABLE_KEY to productId)
    }
}