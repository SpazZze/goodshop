package com.softdesign.goodshop.ui.fragments

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.databinding.DataBindingUtil
import android.net.Uri
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.softdesign.goodshop.R
import com.softdesign.goodshop.data.managers.FileManager
import com.softdesign.goodshop.databinding.FragmentAccountBinding
import com.softdesign.goodshop.di.DaggerService
import com.softdesign.goodshop.di.scopes.RootScope
import com.softdesign.goodshop.di.screens.AccountViewComponent
import com.softdesign.goodshop.di.screens.AccountViewModule
import com.softdesign.goodshop.mvp.models.AccountModel
import com.softdesign.goodshop.mvp.presenters.interfaces.IAccountPresenter
import com.softdesign.goodshop.mvp.views.screens.IAccountView
import com.softdesign.goodshop.ui.fragments.abs.RootChildFragment
import com.softdesign.goodshop.utils.extentions.chooseFromGallery
import com.softdesign.goodshop.utils.extentions.takePicture
import permissions.dispatcher.*
import java.io.File


/**
 * @author Space
 * @date 11.12.2016
 */

@RootScope(IAccountView::class)
@RuntimePermissions
class AccountFragment :
        RootChildFragment<IAccountPresenter, IAccountView, AccountModel, FragmentAccountBinding>(),
        IAccountView {

    private var tempPhotoFile: File? = null

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        DaggerService.getComponent(AccountViewComponent::class.java, AccountViewModule::class.java).inject(this)
        setHasOptionsMenu(true)
        binding = DataBindingUtil.inflate<FragmentAccountBinding>(inflater!!, R.layout.fragment_account, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.appBar.addOnOffsetChangedListener { appBarLayout, i ->
            val totalScrollRange = appBarLayout.totalScrollRange
            val percentage = 1f - Math.abs(i).toFloat() / totalScrollRange

            listOf(binding.ivUserAvatar, binding.ivAvatarEdit).forEach {
                it.animate().scaleY(percentage).scaleX(percentage).setDuration(0).start()
            }
        }
    }

    override fun onBackPressed(): Boolean = presenter.cancelEditMode(binding.fab)

    override fun setupToolbar() = presenter.setupToolbar(getString(R.string.header_personal_cabinet), false)

    override fun showChangePhotoDialog() = AlertDialog.Builder(activity)
            .setTitle(getString(R.string.header_change_photo))
            .setItems(R.array.change_photo, { d, i ->
                when (i) {
                    0 -> AccountFragmentPermissionsDispatcher.takePhotoWithCheck(this)
                    1 -> AccountFragmentPermissionsDispatcher.chooseGalleryImageWithCheck(this)
                    2 -> d.cancel()
                }
            })
            .create()
            .show()

    override fun onActivityResult(requestCode: Int, resultCode: Int, intent: Intent?) {
        if (resultCode != Activity.RESULT_OK) return
        when (requestCode) {
            REQUEST_IMAGEFROMGALLERY -> intent?.data?.let { presenter.saveAvatar(it) }
                    ?: showMessage(R.string.error_avatar_not_saved)
            REQUEST_IMAGEFROMCAMERA -> tempPhotoFile?.let { presenter.saveAvatar(Uri.fromFile(it)) }
                    ?: showMessage(R.string.error_avatar_not_saved)
        }
    }

    //region :::::::::::::::::::::::::::::::::: Runtime Permissions
    @NeedsPermission(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
    fun takePhoto() = FileManager.createImageFile()
            ?.let { tempPhotoFile = it; takePicture(it, REQUEST_IMAGEFROMCAMERA) }
            ?: showMessage(R.string.error_cache_file_not_created)

    @NeedsPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
    fun chooseGalleryImage() = chooseFromGallery(R.string.header_choose_image, REQUEST_IMAGEFROMGALLERY)

    @OnShowRationale(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
    fun showRationaleForCamera(request: PermissionRequest) {
        showDialog(R.string.permission_rationale_camera, { request.proceed() }, { request.cancel() })
    }

    @OnShowRationale(Manifest.permission.READ_EXTERNAL_STORAGE)
    fun showRationaleForReadFromSdCard(request: PermissionRequest) {
        showDialog(R.string.permission_rationale_read_storage, { request.proceed() }, { request.cancel() })
    }

    @OnNeverAskAgain(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
    fun showNeverAskForCamera() {
        showDialog(R.string.permission_never_ask_camera, { showAppSettings() })
    }

    @OnNeverAskAgain(Manifest.permission.READ_EXTERNAL_STORAGE)
    fun showNeverAskForReadFromSdCard() {
        showDialog(R.string.permission_never_ask_read_storage, { showAppSettings() })
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        AccountFragmentPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults)
    }
    //endregion :::::::::::::::::::::::::::::::::: Runtime Permissions

    companion object {
        private const val REQUEST_IMAGEFROMCAMERA = 999
        private const val REQUEST_IMAGEFROMGALLERY = 998
        fun newInstance() = AccountFragment()
    }
}