package com.softdesign.goodshop.ui.fragments

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.github.spazzze.exto.extensions.readArgs
import com.softdesign.goodshop.R
import com.softdesign.goodshop.common.App
import com.softdesign.goodshop.data.storage.dto.Product
import com.softdesign.goodshop.databinding.FragmentProductAboutBinding
import com.softdesign.goodshop.di.DaggerService
import com.softdesign.goodshop.di.screens.ProductAboutViewComponent
import com.softdesign.goodshop.di.screens.ProductAboutViewModule
import com.softdesign.goodshop.mvp.models.ProductAboutModel
import com.softdesign.goodshop.mvp.presenters.interfaces.IProductAboutPresenter
import com.softdesign.goodshop.mvp.views.screens.IProductAboutView
import com.softdesign.goodshop.ui.fragments.abs.RootChildFragment
import org.jetbrains.anko.support.v4.withArguments

/**
 * @author Space
 * @date 19.12.2016
 */

class ProductAboutFragment :
        RootChildFragment<IProductAboutPresenter, IProductAboutView, ProductAboutModel, FragmentProductAboutBinding>(), IProductAboutView {

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val product = readArgs<Product>(PARCELABLE_KEY, { handleBadIntent(PARCELABLE_KEY) })
        if (!product.isDefined()) return null
        DaggerService.getComponent(ProductAboutViewComponent::class.java,
                arrayOf<Any>(ProductAboutViewModule(product.get()))).inject(this)
        binding = DataBindingUtil.inflate<FragmentProductAboutBinding>(inflater!!, R.layout.fragment_product_about, container, false)
        return binding.root
    }

    companion object {
        val TITLE = App.ctx.getString(R.string.header_description).capitalize()
        private val PARCELABLE_KEY = "PARCELABLE_KEY_PRODUCT_ABOUT"
        fun newInstance(product: Product) = ProductAboutFragment().withArguments(PARCELABLE_KEY to product)
    }
}


