package com.softdesign.goodshop.ui.fragments

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.github.spazzze.exto.extensions.readArgs
import com.softdesign.goodshop.R
import com.softdesign.goodshop.data.storage.dto.Product
import com.softdesign.goodshop.databinding.FragmentProductBinding
import com.softdesign.goodshop.di.DaggerService
import com.softdesign.goodshop.di.screens.ProductViewComponent
import com.softdesign.goodshop.di.screens.ProductViewModule
import com.softdesign.goodshop.mvp.models.ProductModel
import com.softdesign.goodshop.mvp.presenters.interfaces.IProductPresenter
import com.softdesign.goodshop.mvp.views.screens.IProductView
import com.softdesign.goodshop.ui.fragments.abs.RootChildFragment
import org.jetbrains.anko.support.v4.withArguments

/**
 * @author Space
 * @date 11.12.2016
 */

class ProductFragment : RootChildFragment<IProductPresenter, IProductView, ProductModel, FragmentProductBinding>(), IProductView {

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val product = readArgs<Product>(PARCELABLE_KEY, { handleBadIntent(PARCELABLE_KEY) })
        if (!product.isDefined()) return null
        DaggerService.getProductComponent(ProductViewComponent::class.java, product.get(),
                arrayOf<Any>(ProductViewModule(product.get()))).inject(this)
        binding = DataBindingUtil.inflate<FragmentProductBinding>(inflater!!, R.layout.fragment_product, container, false)
        return binding.root
    }

    override fun onResume() {
        super.onResume()
        presenter.update()
    }

    override fun onDetach() {
        if (isRemoving) DaggerService.unregisterProduct(presenter.product.id)
        super.onDetach()
    }

    companion object {
        private val PARCELABLE_KEY = "PARCELABLE_KEY_PRODUCT"
        fun newInstance(product: Product) = ProductFragment().withArguments(PARCELABLE_KEY to product)
    }
}
