package com.softdesign.goodshop.ui.view.elements

import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBar
import android.support.v7.app.ActionBarDrawerToggle
import android.view.View
import android.widget.ImageView

/**
 * @author Space
 * @date 05.02.2017
 */

class ToolbarBuilder(private val drawerLayout: DrawerLayout,
                     private val drawerToggle: ActionBarDrawerToggle,
                     private val actionBar: ActionBar,
                     private val menuTitleImage: ImageView? = null,
                     private val onBackArrowClick: () -> Unit) {

    private var toolbarTitle = ""
    private var isDrawerLocked = false

    init {
        drawerLayout.addDrawerListener(drawerToggle)
    }

    fun setTitle(title: String) = apply { toolbarTitle = title }

    fun setDrawerLocked(isLocked: Boolean) = apply { isDrawerLocked = isLocked }

    fun init() = apply {
        toolbarTitle = ""
        isDrawerLocked = false
    }

    fun build() {
        setToolbarTitle()
        setupDrawer()
        drawerToggle.syncState()
    }

    private fun setToolbarTitle() = actionBar.apply {
        with(toolbarTitle.isNullOrBlank()) {
            setDisplayShowTitleEnabled(!this)
            menuTitleImage?.visibility = if (this) View.VISIBLE else View.GONE
        }
        title = toolbarTitle
    }

    private fun setupDrawer() = with(isDrawerLocked) {
        drawerToggle.isDrawerIndicatorEnabled = !this
        actionBar.setDisplayHomeAsUpEnabled(this)
        when {
            this -> {
                drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
                drawerToggle.setToolbarNavigationClickListener { onBackArrowClick() }
            }
            else -> {
                drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED)
                drawerToggle.toolbarNavigationClickListener = null
            }
        }
    }
}