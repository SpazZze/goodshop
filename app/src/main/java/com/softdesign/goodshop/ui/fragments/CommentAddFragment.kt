package com.softdesign.goodshop.ui.fragments

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.github.spazzze.exto.extensions.readArgs
import com.softdesign.goodshop.R
import com.softdesign.goodshop.databinding.FragmentCommentAddBinding
import com.softdesign.goodshop.di.DaggerService
import com.softdesign.goodshop.di.scopes.RootScope
import com.softdesign.goodshop.di.screens.CommentAddViewComponent
import com.softdesign.goodshop.di.screens.CommentAddViewModule
import com.softdesign.goodshop.mvp.models.CommentAddModel
import com.softdesign.goodshop.mvp.presenters.interfaces.ICommentAddPresenter
import com.softdesign.goodshop.mvp.views.screens.ICommentAddView
import com.softdesign.goodshop.mvp.views.screens.ICommentView
import com.softdesign.goodshop.ui.fragments.abs.RootChildFragment
import org.jetbrains.anko.support.v4.withArguments

/**
 * @author Space
 * @date 05.01.2017
 */

@RootScope(ICommentView::class)
class CommentAddFragment :
        RootChildFragment<ICommentAddPresenter, ICommentAddView, CommentAddModel, FragmentCommentAddBinding>(),
        ICommentAddView {

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val productId = readArgs(PARCELABLE_KEY, "", { handleBadIntent(PARCELABLE_KEY) })
        if (productId.isBlank()) return null
        DaggerService.getComponent(CommentAddViewComponent::class.java,
                arrayOf<Any>(CommentAddViewModule(productId))).inject(this)
        setHasOptionsMenu(true)
        binding = DataBindingUtil.inflate<FragmentCommentAddBinding>(inflater!!, R.layout.fragment_comment_add, container, false)
        return binding.root
    }

    override fun setupToolbar() = presenter.setupToolbar(getString(R.string.header_add_comment))

    companion object {
        private val PARCELABLE_KEY = "PARCELABLE_KEY_COMMENT"
        fun newInstance(productId: String) = CommentAddFragment().withArguments(PARCELABLE_KEY to productId)
    }
}