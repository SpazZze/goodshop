package com.softdesign.goodshop.ui.fragments.abs

import android.content.Context
import android.databinding.ViewDataBinding
import android.os.Bundle
import android.support.annotation.CallSuper
import android.support.annotation.StringRes
import android.support.v4.app.Fragment
import android.view.View
import com.softdesign.goodshop.BR
import com.softdesign.goodshop.di.DaggerService
import com.softdesign.goodshop.mvp.models.abs.BaseModel
import com.softdesign.goodshop.mvp.presenters.abs.interfaces.IRootChildPresenter
import com.softdesign.goodshop.mvp.views.IRootChildView
import com.softdesign.goodshop.mvp.views.screens.IRootView
import com.softdesign.goodshop.ui.view.interfaces.INavigableFragment
import timber.log.Timber
import java.io.IOException
import javax.inject.Inject

/**
 * @author Space
 * @date 11.12.2016
 */


abstract class RootChildFragment<P : IRootChildPresenter<V, M>, V : IRootChildView<M>,
        M : BaseModel, B : ViewDataBinding> : Fragment(), IRootChildView<M>, INavigableFragment {

    @Inject
    lateinit open var presenter: P

    lateinit open var binding: B

    override val ctx: Context
        get() = context

    private var rootView: IRootView? = null
        get() = presenter.rootPresenter.getView()

    //region :::::::::::::::::::::::::::::::::: Life cycle
    @Suppress("UNCHECKED_CAST")
    @CallSuper
    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.takeView(this as V)
        presenter.initView()
    }

    @CallSuper
    override fun onDestroyView() {
        presenter.dropView()
        super.onDestroyView()
    }

    @CallSuper
    override fun onDetach() {
        if (isRemoving) {
            presenter.onDestroy()
            DaggerService.unregisterScope(this.javaClass.declaredAnnotations)
        }
        super.onDetach()
    }
    //endregion :::::::::::::::::::::::::::::::::: Life cycle

    override fun handleBadIntent(key: String) = Timber.e(IOException(), "$javaClass handleBadIntent error: wrong args by key $key")
            .run { finish() }

    override fun setViewModel(model: M) {
        binding.setVariable(BR.viewModel, model)
    }

    override fun showMessage(message: String) {
        rootView?.showMessage(message)
    }

    override fun showMessage(@StringRes messageId: Int) {
        rootView?.showMessage(messageId)
    }

    override fun showNotification(message: String) {
        rootView?.showNotification(message)
    }

    override fun showNotification(@StringRes messageId: Int) {
        rootView?.showNotification(messageId)
    }

    override fun showAuthScreen() {
        rootView?.showAuthScreen()
    }

    override fun showRootScreen() {
        rootView?.showRootScreen()
    }

    override fun showError(messageId: Int, e: Throwable) {
        rootView?.showError(messageId, e)
    }

    override fun showNetworkError(t: Throwable) {
        rootView?.showNetworkError(t)
    }

    override fun showNetworkError(messageId: Int, t: Throwable) {
        rootView?.showNetworkError(messageId, t)
    }

    override fun showDialog(messageId: Int, onPositive: () -> Unit, onCancel: () -> Unit) {
        rootView?.showDialog(messageId, onPositive, onCancel)
    }

    override fun finish() = activity.onBackPressed()

    override fun onBackPressed() = false
}
