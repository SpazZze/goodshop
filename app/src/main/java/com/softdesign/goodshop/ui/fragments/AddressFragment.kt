package com.softdesign.goodshop.ui.fragments

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.github.spazzze.exto.extensions.readArgs
import com.softdesign.goodshop.R
import com.softdesign.goodshop.data.storage.dto.Address
import com.softdesign.goodshop.databinding.FragmentAddressBinding
import com.softdesign.goodshop.di.DaggerService
import com.softdesign.goodshop.di.scopes.RootScope
import com.softdesign.goodshop.di.screens.AddressViewComponent
import com.softdesign.goodshop.di.screens.AddressViewModule
import com.softdesign.goodshop.mvp.models.AddressModel
import com.softdesign.goodshop.mvp.presenters.interfaces.IAddressPresenter
import com.softdesign.goodshop.mvp.views.screens.IAddressView
import com.softdesign.goodshop.ui.fragments.abs.RootChildFragment
import org.jetbrains.anko.support.v4.withArguments

/**
 * @author Space
 * @date 11.12.2016
 */

@RootScope(IAddressView::class)
class AddressFragment :
        RootChildFragment<IAddressPresenter, IAddressView, AddressModel, FragmentAddressBinding>(),
        IAddressView {

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        DaggerService.getComponent(AddressViewComponent::class.java,
                arrayOf<Any>(AddressViewModule(readArgs(PARCELABLE_KEY, Address("", ""))))).inject(this)
        setHasOptionsMenu(true)
        binding = DataBindingUtil.inflate<FragmentAddressBinding>(inflater!!, R.layout.fragment_address, container, false)
        return binding.root
    }

    override fun setupToolbar() = when (presenter.addressId == -1) {
        true -> presenter.setupToolbar(getString(R.string.header_add_shipping_address))
        else -> presenter.setupToolbar(getString(R.string.header_edit_shipping_address))
    }

    companion object {
        private val PARCELABLE_KEY = "AddressFragment"
        fun newInstance() = AddressFragment()
        fun newInstance(address: Address) = AddressFragment().withArguments(PARCELABLE_KEY to address)
    }
}