package com.softdesign.goodshop.ui.fragments

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.softdesign.goodshop.R
import com.softdesign.goodshop.databinding.FragmentCartBinding
import com.softdesign.goodshop.di.DaggerService
import com.softdesign.goodshop.di.scopes.RootScope
import com.softdesign.goodshop.di.screens.CartViewComponent
import com.softdesign.goodshop.di.screens.CartViewModule
import com.softdesign.goodshop.mvp.models.CartModel
import com.softdesign.goodshop.mvp.presenters.interfaces.ICartPresenter
import com.softdesign.goodshop.mvp.views.screens.ICartView
import com.softdesign.goodshop.ui.fragments.abs.RootChildFragment

/**
 * @author Space
 * @date 15.04.2017
 */

@RootScope(ICartView::class)
class CartFragment :
        RootChildFragment<ICartPresenter, ICartView, CartModel, FragmentCartBinding>(),
        ICartView {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        DaggerService.getComponent(CartViewComponent::class.java, CartViewModule::class.java).inject(this)
        binding = DataBindingUtil.inflate<FragmentCartBinding>(inflater, R.layout.fragment_cart, container, false)
        return binding.root
    }

    override fun setupToolbar() = presenter.setupToolbar(getString(R.string.header_cart), false)

    override fun showRemovedFromCartMessage(productName: String) =
            showNotification("$productName ${getString(R.string.notify_removed_from_cart)}")

    companion object {
        fun newInstance() = CartFragment()
    }
}