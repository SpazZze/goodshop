package com.softdesign.goodshop.ui.activities

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.design.widget.Snackbar
import com.softdesign.goodshop.R
import com.softdesign.goodshop.databinding.ActivityAuthBinding
import com.softdesign.goodshop.di.DaggerService
import com.softdesign.goodshop.di.scopes.DaggerScope
import com.softdesign.goodshop.di.screens.AuthViewComponent
import com.softdesign.goodshop.di.screens.AuthViewModule
import com.softdesign.goodshop.mvp.models.AuthModel
import com.softdesign.goodshop.mvp.presenters.interfaces.IAuthPresenter
import com.softdesign.goodshop.mvp.views.screens.IAuthView

/**
 * @author Space
 * @date 11.12.2016
 */

@DaggerScope(IAuthView::class)
class AuthActivity : BindingActivity<IAuthPresenter, IAuthView, AuthModel, ActivityAuthBinding>(), IAuthView {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        DaggerService.getComponent(AuthViewComponent::class.java, AuthViewModule::class.java).inject(this)
        binding = DataBindingUtil.setContentView<ActivityAuthBinding>(this, R.layout.activity_auth)
    }

    override fun onBackPressed() {
        if (!presenter.hideLoginCard()) super.onBackPressed()
    }

    override fun showMessage(message: String) {
        Snackbar.make(binding.coordinator, message, Snackbar.LENGTH_LONG).show()
    }
}
