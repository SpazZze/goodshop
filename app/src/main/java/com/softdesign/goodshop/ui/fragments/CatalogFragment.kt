package com.softdesign.goodshop.ui.fragments

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.softdesign.goodshop.R
import com.softdesign.goodshop.common.databinding.abs.BindingPagerAdapter
import com.softdesign.goodshop.data.storage.dto.Product
import com.softdesign.goodshop.databinding.FragmentCatalogBinding
import com.softdesign.goodshop.di.DaggerService
import com.softdesign.goodshop.di.scopes.RootScope
import com.softdesign.goodshop.di.screens.CatalogViewComponent
import com.softdesign.goodshop.di.screens.CatalogViewModule
import com.softdesign.goodshop.mvp.models.CatalogModel
import com.softdesign.goodshop.mvp.presenters.interfaces.ICatalogPresenter
import com.softdesign.goodshop.mvp.views.screens.ICatalogView
import com.softdesign.goodshop.ui.fragments.abs.RootChildFragment

/**
 * @author Space
 * @date 11.12.2016
 */

@RootScope(ICatalogView::class)
class CatalogFragment :
        RootChildFragment<ICatalogPresenter, ICatalogView, CatalogModel, FragmentCatalogBinding>(),
        ICatalogView {

    private val currentPagerFragment get() = adapter.getCurrentFragment() as? ProductFragment

    private val adapter by lazy {
        object : BindingPagerAdapter<Product>(childFragmentManager) {
            override fun createFragment(item: Product) = ProductFragment.newInstance(item)
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        DaggerService.getComponent(CatalogViewComponent::class.java, CatalogViewModule::class.java).inject(this)
        binding = DataBindingUtil.inflate<FragmentCatalogBinding>(inflater!!, R.layout.fragment_catalog, container, false)
        binding.btnAddToCart.setOnClickListener { onClickAtBuyButton() }
        return binding.root
    }

    override fun onClickAtBuyButton() = currentPagerFragment?.presenter?.let {
        presenter.addItemToCart(it.product, it.itemsCount)
        showAddedToCartMessage(it.product.productName)
    } ?: Unit

    override fun showAddedToCartMessage(productName: String) =
            showNotification(productName + getString(R.string.notify_product_added_to_cart))

    override fun setupToolbar() = presenter.setupToolbar(isDrawerLocked = false)

    override fun setupViewPager() {
        binding.vpCatalog.adapter = adapter
        binding.tabDots.setupWithViewPager(binding.vpCatalog, true)
    }

    companion object {
        fun newInstance() = CatalogFragment()
    }
}
