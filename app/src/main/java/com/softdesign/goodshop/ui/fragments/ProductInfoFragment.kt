package com.softdesign.goodshop.ui.fragments

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.github.spazzze.exto.extensions.readArgs
import com.softdesign.goodshop.R
import com.softdesign.goodshop.data.storage.dto.Product
import com.softdesign.goodshop.databinding.FragmentProductInfoBinding
import com.softdesign.goodshop.di.DaggerService
import com.softdesign.goodshop.di.scopes.RootScope
import com.softdesign.goodshop.di.screens.ProductInfoViewComponent
import com.softdesign.goodshop.di.screens.ProductInfoViewModule
import com.softdesign.goodshop.mvp.models.ProductInfoModel
import com.softdesign.goodshop.mvp.presenters.interfaces.IProductInfoPresenter
import com.softdesign.goodshop.mvp.views.screens.IProductInfoView
import com.softdesign.goodshop.ui.fragments.abs.RootChildFragment
import org.jetbrains.anko.support.v4.withArguments

/**
 * @author Space
 * @date 18.12.2016
 */

@RootScope(IProductInfoView::class)
class ProductInfoFragment :
        RootChildFragment<IProductInfoPresenter, IProductInfoView, ProductInfoModel, FragmentProductInfoBinding>(),
        IProductInfoView {

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val product = readArgs<Product>(PARCELABLE_KEY, { handleBadIntent(PARCELABLE_KEY) })
        if (!product.isDefined()) return null
        DaggerService.getComponent(ProductInfoViewComponent::class.java,
                arrayOf<Any>(ProductInfoViewModule(product.get()))).inject(this)
        binding = DataBindingUtil.inflate<FragmentProductInfoBinding>(inflater!!, R.layout.fragment_product_info, container, false)
        return binding.root
    }

    override fun setupToolbar() = presenter.setupToolbar(presenter.product.productName)

    override fun setupViewPager() {
        binding.pager.adapter = ProductInfoPagerAdapter(childFragmentManager, 2, presenter.product)
        binding.tabLayout.setupWithViewPager(binding.pager)
    }

    class ProductInfoPagerAdapter(fm: FragmentManager, val numOfTabs: Int, val product: Product) : FragmentStatePagerAdapter(fm) {

        override fun getItem(position: Int): Fragment = when (position) {
            0 -> ProductAboutFragment.newInstance(product)
            else -> CommentsListFragment.newInstance(product.id)
        }

        override fun getCount() = numOfTabs

        override fun getPageTitle(position: Int): CharSequence = when (position) {
            0 -> ProductAboutFragment.TITLE
            else -> CommentsListFragment.TITLE
        }
    }

    companion object {
        private val PARCELABLE_KEY = "PARCELABLE_KEY_PRODUCT_INFO"
        fun newInstance(product: Product) = ProductInfoFragment().withArguments(PARCELABLE_KEY to product)
    }
}
