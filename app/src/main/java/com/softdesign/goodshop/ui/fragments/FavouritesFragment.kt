package com.softdesign.goodshop.ui.fragments

import android.content.res.Configuration.ORIENTATION_PORTRAIT
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout.HORIZONTAL
import com.softdesign.goodshop.R
import com.softdesign.goodshop.data.storage.dto.Product
import com.softdesign.goodshop.databinding.FragmentFavouritesBinding
import com.softdesign.goodshop.di.DaggerService
import com.softdesign.goodshop.di.scopes.RootScope
import com.softdesign.goodshop.di.screens.FavouritesViewComponent
import com.softdesign.goodshop.di.screens.FavouritesViewModule
import com.softdesign.goodshop.mvp.models.FavouritesModel
import com.softdesign.goodshop.mvp.presenters.interfaces.IFavouritesPresenter
import com.softdesign.goodshop.mvp.views.screens.IFavouritesView
import com.softdesign.goodshop.ui.fragments.abs.RootChildFragment

/**
 * @author Space
 * @date 03.04.2017
 */


@RootScope(IFavouritesView::class)
class FavouritesFragment :
        RootChildFragment<IFavouritesPresenter, IFavouritesView, FavouritesModel, FragmentFavouritesBinding>(),
        IFavouritesView {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        DaggerService.getComponent(FavouritesViewComponent::class.java, FavouritesViewModule::class.java).inject(this)
        binding = DataBindingUtil.inflate<FragmentFavouritesBinding>(inflater, R.layout.fragment_favourites, container, false)
        initRecyclerView()
        return binding.root
    }

    private fun initRecyclerView() {
        val layoutManager = when (resources.configuration.orientation) {
            ORIENTATION_PORTRAIT -> GridLayoutManager(activity, 2)
            else -> LinearLayoutManager(context, HORIZONTAL, false)
        }
        binding.recycler.layoutManager = layoutManager
    }

    override fun setupToolbar() = presenter.setupToolbar(getString(R.string.header_favourites_screen_title), false)

    override fun showAddedToCartMessage(productName: String) =
            showNotification(productName + getString(R.string.notify_product_added_to_cart))

    override fun showRemovedFromFavouritesMessage(product: Product) =
            showNotification("${getString(R.string.header_product)} ${product.productName} ${getString(R.string.notify_removed_from_favourites)}")

    companion object {
        fun newInstance() = FavouritesFragment()
    }
}