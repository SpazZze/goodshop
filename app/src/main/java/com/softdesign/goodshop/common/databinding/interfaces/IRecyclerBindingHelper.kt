package com.softdesign.goodshop.common.databinding.interfaces

import com.softdesign.goodshop.common.databinding.interfaces.IRecyclerItemViewModel

/**
 * @author Space
 * @date 06.04.2017
 */

interface IRecyclerBindingHelper<out I : IRecyclerItemViewModel> {

    val layoutRes: Int

    val variableId: Int
}