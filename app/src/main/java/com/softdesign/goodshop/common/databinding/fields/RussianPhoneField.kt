package com.softdesign.goodshop.common.databinding.fields

import android.content.Context
import android.databinding.ObservableField
import com.softdesign.goodshop.R
import com.softdesign.goodshop.utils.extentions.match

/**
 * @author Space
 * @date 29.01.2017
 */

class RussianPhoneField(private val context: Context,
                        val t: String,
                        private val observableError: ObservableField<String>) : ObservableField<String>(t) {

    val isValid: Boolean get() = observableError.get()?.isEmpty() ?: false

    @Volatile
    var fieldValue = reformatPhone(t)
        @Synchronized set(value) {
            if (field !== value) {
                observableError.set(getError(value))
                field = value
                notifyChange()
            }
        }

    override fun get() = fieldValue

    override fun set(value: String?) {
        fieldValue = value ?: return
    }

    fun applyFormatting(): String = reformatPhone(fieldValue).apply { fieldValue = this }

    private fun getError(phone: String) = with(phone.replace("[()\\-+\\s]".toRegex(), "")) {
        when {
            isEmpty() -> context.getString(R.string.error_editText_phone_wrong_code)
            match("(\\d*\\D\\d*)*") -> context.getString(R.string.error_editText_phone_wrong_symbols)
            !startsWith(ru_phone_code) && !phone.startsWith(ru_phone_code2) -> context.getString(R.string.error_editText_phone_wrong_code)
            length != maxDigitsCount -> context.getString(R.string.error_editText_phone_wrong_length)
            else -> ""
        }
    }

    companion object {
        private val maxDigitsCount = 11
        private val ru_phone_code = "7"
        private val ru_phone_code2 = "8"

        fun reformatPhone(phone: String): String {
            if (phone.isEmpty()) return phone
            val onlyDigits = phone.replace("\\D".toRegex(), "")
            val code = getCode(onlyDigits)
            return when {
                onlyDigits.isEmpty() -> phone
                code.isNotEmpty() -> code + composePhone(onlyDigits.substring(code.length - 2))
                else -> composePhone(onlyDigits)
            }
        }

        private fun getCode(phone: String): String = when {
            phone.startsWith(ru_phone_code) || phone.startsWith(ru_phone_code2) -> "+$ru_phone_code "
            else -> ""
        }

        private fun composePhone(phone: String): String = when (phone.length) {
            0 -> ""

            in 1..3 -> StringBuilder().append("(").append(phone.substring(0, phone.length)).toString()

            in 1..6 -> StringBuilder().append("(").append(phone.substring(0, 3)).append(") ")
                    .append(phone.substring(3, phone.length)).toString()

            in 1..8 -> StringBuilder().append("(").append(phone.substring(0, 3))
                    .append(") ").append(phone.substring(3, 6))
                    .append("-").append(phone.substring(6, phone.length)).toString()

            else -> StringBuilder().append("(").append(phone.substring(0, 3))
                    .append(") ").append(phone.substring(3, 6))
                    .append("-").append(phone.substring(6, 8))
                    .append("-").append(phone.substring(8, phone.length)).toString()
        }
    }
}