package com.softdesign.goodshop.common.databinding.interfaces

/**
 * @author Space
 * @date 28.01.2017
 */

interface IValidable<T> {

    val fieldValue: T

    val isValid: Boolean

    fun validateField(value: T = fieldValue): Boolean
}