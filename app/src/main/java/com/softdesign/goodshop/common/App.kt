package com.softdesign.goodshop.common

import android.content.Context
import android.support.multidex.MultiDexApplication
import android.support.v7.app.AppCompatDelegate
import com.crashlytics.android.Crashlytics
import com.facebook.stetho.Stetho
import com.softdesign.goodshop.BuildConfig.DEBUG
import com.softdesign.goodshop.data.managers.PreferencesManager.ACCESS_TOKEN
import com.softdesign.goodshop.data.managers.PreferencesManager.USER_ID
import com.softdesign.goodshop.data.managers.PreferencesManager.prefsName
import com.softdesign.goodshop.di.DaggerService
import com.softdesign.goodshop.di.components.AppComponent
import com.softdesign.goodshop.di.modules.AppModule
import com.softdesign.goodshop.utils.logging.CrashlyticsTree
import com.squareup.leakcanary.LeakCanary
import com.uphyca.stetho_realm.RealmInspectorModulesProvider
import io.fabric.sdk.android.Fabric
import io.realm.Realm
import io.realm.RealmConfiguration
import timber.log.Timber


/**
 * @author Space
 * @date 10.12.2016
 */

class App : MultiDexApplication() {

    val userId: String
        get() = getSharedPreferences(prefsName, Context.MODE_PRIVATE).getString(USER_ID, "")

    val userAccessToken: String
        get() = getSharedPreferences(prefsName, Context.MODE_PRIVATE).getString(ACCESS_TOKEN, "")

    init {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
    }

    override fun onCreate() {
        super.onCreate()
        ctx = applicationContext
        appComponent = DaggerService.getComponent(AppComponent::class.java, arrayOf<Any>(AppModule(applicationContext)))
        Fabric.with(this, Crashlytics())
        Timber.plant(CrashlyticsTree())
        Realm.init(this)
        Realm.setDefaultConfiguration(RealmConfiguration.Builder().deleteRealmIfMigrationNeeded().build())
        Stetho.initialize(
                Stetho.newInitializerBuilder(this)
                        .enableDumpapp(Stetho.defaultDumperPluginsProvider(this))
                        .enableWebKitInspector(RealmInspectorModulesProvider.builder(this)
                                .withMetaTables()
                                .build())
                        .build())
        if (DEBUG && !LeakCanary.isInAnalyzerProcess(this)) LeakCanary.install(this)
    }

    companion object {
        lateinit var appComponent: AppComponent
        lateinit var ctx: Context
        val userId get() = (ctx as App).userId
        val userAccessToken get() = (ctx as App).userAccessToken
    }
}