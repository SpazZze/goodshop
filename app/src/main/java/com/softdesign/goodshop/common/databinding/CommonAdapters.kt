package com.softdesign.goodshop.common.databinding

import android.databinding.BindingAdapter
import android.databinding.BindingConversion
import android.view.View

/**
 * @author elena
 * @date 23/12/16
 */

@BindingConversion
fun convertBooleanToVisibility(visible: Boolean): Int {
    return if (visible) View.VISIBLE else View.GONE
}

@BindingAdapter("onClick")
fun bindOnClick(view: View, runnable: Runnable?) {
    runnable?.let { view.setOnClickListener { runnable.run() } }
}