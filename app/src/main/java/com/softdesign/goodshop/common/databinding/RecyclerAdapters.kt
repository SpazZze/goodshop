package com.softdesign.goodshop.common.databinding

import android.databinding.BindingAdapter
import android.databinding.ObservableList
import android.support.v7.widget.RecyclerView
import com.softdesign.goodshop.common.databinding.abs.RecyclerBindingAdapter
import com.softdesign.goodshop.common.databinding.interfaces.IRecyclerBindingAdapter
import com.softdesign.goodshop.common.databinding.interfaces.IRecyclerBindingHelper
import com.softdesign.goodshop.common.databinding.interfaces.IRecyclerItemViewModel

/**
 * @author elena
 * @date 23/12/16
 */

@Suppress("UNCHECKED_CAST")
@BindingAdapter("items")
fun <T : IRecyclerItemViewModel> setItems(recyclerView: RecyclerView, items: ObservableList<T>?) {
    (recyclerView.adapter as? IRecyclerBindingAdapter<T>)?.setItems(items ?: return)
}

@Suppress("UNCHECKED_CAST")
@BindingAdapter("bindingHelper")
fun <I : IRecyclerItemViewModel> setupRecyclerView(recyclerView: RecyclerView,
                                                   bindingHelper: IRecyclerBindingHelper<I>) {
    if (recyclerView.adapter == null) recyclerView.adapter = RecyclerBindingAdapter(bindingHelper)
}