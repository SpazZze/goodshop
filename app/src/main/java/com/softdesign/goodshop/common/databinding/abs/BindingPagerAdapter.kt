package com.softdesign.goodshop.common.databinding.abs

import android.databinding.ObservableArrayList
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import android.support.v4.view.PagerAdapter
import android.view.ViewGroup
import com.softdesign.goodshop.utils.extentions.onChange
import com.softdesign.goodshop.utils.extentions.replaceAllBy

/**
 * @author Space
 * @date 24.12.2016
 */

abstract class BindingPagerAdapter<in T : Any>(fm: FragmentManager) : FragmentStatePagerAdapter(fm) {

    private @Volatile var items = ObservableArrayList<T>().onChange { notifyDataSetChanged() }

    private var currentFragment: Fragment? = null

    abstract fun createFragment(item: T): Fragment

    override fun getItemPosition(item: Any?): Int = PagerAdapter.POSITION_NONE

    override fun getCount(): Int = items.size

    override fun getItem(position: Int) = createFragment(items[position])

    override fun setPrimaryItem(container: ViewGroup?, position: Int, item: Any?) {
        if (currentFragment != item) currentFragment = item as? Fragment
        super.setPrimaryItem(container, position, item)
    }

    fun setItems(newItems: List<T>) = items.replaceAllBy(newItems)

    fun getCurrentFragment() = currentFragment
}
