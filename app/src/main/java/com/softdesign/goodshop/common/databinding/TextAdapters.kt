package com.softdesign.goodshop.common.databinding

import android.databinding.BindingAdapter
import android.graphics.PorterDuff
import android.graphics.Typeface
import android.support.annotation.ColorRes
import android.support.annotation.StringRes
import android.support.design.widget.TextInputLayout
import android.support.v7.widget.AppCompatDrawableManager
import android.view.View
import android.widget.EditText
import android.widget.TextSwitcher
import android.widget.TextView
import com.softdesign.goodshop.R

/**
 * @author elena
 * @date 23/12/16
 */

@BindingAdapter("showError")
fun showError(til: TextInputLayout, error: String) {
    when (error) {
        "" -> til.error = null
        else -> til.error = error
    }
    til.isErrorEnabled = error.isNotEmpty()
}

@BindingAdapter("hint")
fun setHint(textInputLayout: TextInputLayout, s: String) {
    textInputLayout.hint = s
}

@BindingAdapter("switchText")
fun switchText(switcher: TextSwitcher, text: String?) {
    if (text == null) return

    val tag = switcher.getTag(R.id.animText_tag) as? String

    if (tag == null) {
        switcher.setTag(R.id.animText_tag, text)
    } else if (tag != text) {
        switcher.setText(text)
        switcher.setTag(R.id.animText_tag, text)
    }
}

@BindingAdapter("font")
fun setFont(textView: TextView, fontName: String) {
    textView.typeface = Typeface.createFromAsset(textView.context.assets, fontName)
}

@BindingAdapter("customizeBackgroundTint")
fun setBackgroundTint(editText: EditText, @ColorRes color: Int) {
    tintView(editText, color)
}

private fun tintView(view: View, color: Int) {
    val nd = view.background?.constantState?.newDrawable() ?: return
    nd.colorFilter = AppCompatDrawableManager.getPorterDuffColorFilter(color, PorterDuff.Mode.SRC_IN)
    view.background = nd
}