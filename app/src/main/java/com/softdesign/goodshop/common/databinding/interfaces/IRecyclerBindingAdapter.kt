package com.softdesign.goodshop.common.databinding.interfaces

import com.softdesign.goodshop.common.databinding.interfaces.IRecyclerBindingHelper
import com.softdesign.goodshop.common.databinding.interfaces.IRecyclerItemViewModel

/**
 * @author Space
 * @date 06.04.2017
 */

interface IRecyclerBindingAdapter<I : IRecyclerItemViewModel> {

    val bindingHelper: IRecyclerBindingHelper<I>

    val items: List<I>

    fun setItems(newItems: List<I>)
}