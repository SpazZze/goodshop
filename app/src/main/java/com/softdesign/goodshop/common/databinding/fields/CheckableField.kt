package com.softdesign.goodshop.common.databinding.fields

import android.content.Context
import android.databinding.ObservableField
import android.support.annotation.StringRes
import com.softdesign.goodshop.R

/**
 * @author Space
 * @date 22.01.2017
 */


/**
 *  Sets value, checks if it is correct by passed check function and changes corresponding
 *  observableError
 */
open class CheckableField<T : Any>(private val context: Context,
                                   t: T,
                                   private val observableError: ObservableField<String>,
                                   @StringRes private val stringRes: Int = R.string.error_field_is_mandatory,
                                   private val check: (T) -> Boolean) : ObservableField<T>(t), com.github.spazzze.exto.databinding.interfaces.IValidable<T> {

    @Volatile
    override var fieldValue = t
        @Synchronized set(value) {
            if (field != value) {
                field = value
                validateField(value)
                notifyChange()
            }
        }

    override val isValid: Boolean
        get() = check(fieldValue)

    override fun get() = fieldValue

    override fun set(value: T?) {
        fieldValue = value ?: return
    }

    override fun validateField(value: T) = when (check(value)) {
        true -> {
            observableError.set(""); true
        }
        false -> {
            observableError.set(context.getString(stringRes)); false
        }
    }
}