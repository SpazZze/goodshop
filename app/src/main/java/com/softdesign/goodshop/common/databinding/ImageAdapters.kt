package com.softdesign.goodshop.common.databinding

import android.databinding.BindingAdapter
import android.graphics.drawable.Drawable
import android.support.annotation.DrawableRes
import android.widget.ImageView
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.softdesign.goodshop.utils.extentions.load
import com.softdesign.goodshop.utils.extentions.roundedImage

/**
 * @author elena
 * @date 23/12/16
 */

@BindingAdapter("src")
fun setSrc(view: ImageView, @DrawableRes drawable: Int?) {
    view.setImageResource(drawable ?: return)
}

@BindingAdapter("placeholder")
fun loadImage(view: ImageView, placeholder: Drawable?) {
    view.load("", placeholder ?: return)
}

@BindingAdapter("imageUrl", "placeholder")
fun loadImage(view: ImageView, url: String?, placeholder: Drawable?) {
    view.load(url, placeholder ?: return)
}

@BindingAdapter("roundedImage", "placeholder")
fun setRoundedImage(view: ImageView, url: String?, placeholder: Drawable?) {
    view.roundedImage(url, placeholder ?: return)
}

@BindingAdapter("roundedImage", "placeholder", "borderWidth", "borderColor")
fun setRoundedImage(view: ImageView, url: String?, placeholder: Drawable?, borderWidth: Float, borderColor: Int) {
    view.roundedImage(url, placeholder ?: return, borderWidth, borderColor)
}

@BindingAdapter("roundedImage", "placeholder", "borderWidth", "borderColor", "cacheStrategy")
fun setRoundedImage(view: ImageView, url: String?, placeholder: Drawable?, borderWidth: Float,
                    borderColor: Int, cacheStrategy: Int?) {
    val diskCacheStrategy = diskCacheStrategy(cacheStrategy ?: 0)
    view.roundedImage(url, placeholder ?: return, borderWidth, borderColor, diskCacheStrategy)
}

@BindingAdapter("srcCompat")
fun loadImageDrawable(view: ImageView, drawable: Drawable?) {
    view.setImageDrawable(drawable ?: return)
}

@BindingAdapter("srcCompat")
fun loadImageDrawable(view: ImageView, drawableRes: Int?) {
    view.setImageResource(drawableRes ?: return)
}

private fun diskCacheStrategy(cacheStrategy: Int): DiskCacheStrategy = when (cacheStrategy) {
    0 -> DiskCacheStrategy.NONE
    1 -> DiskCacheStrategy.SOURCE
    2 -> DiskCacheStrategy.RESULT
    else -> DiskCacheStrategy.ALL
}