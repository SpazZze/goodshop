package com.softdesign.goodshop.common.databinding.abs

import android.support.annotation.IdRes
import android.support.annotation.LayoutRes
import com.softdesign.goodshop.common.databinding.interfaces.IRecyclerBindingHelper
import com.softdesign.goodshop.common.databinding.interfaces.IRecyclerItemViewModel

/**
 * @author Space
 * @date 05.04.2017
 */

data class RecyclerBindingHelper<out I : IRecyclerItemViewModel>(@LayoutRes override val layoutRes: Int,
                                                                 @IdRes override val variableId: Int) : IRecyclerBindingHelper<I>
