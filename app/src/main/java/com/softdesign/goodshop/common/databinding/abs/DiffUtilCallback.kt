package com.softdesign.goodshop.common.databinding.abs

import android.support.v7.util.DiffUtil
import com.softdesign.goodshop.common.databinding.interfaces.IRecyclerItemViewModel

/**
 * @author Space
 * @date 03.04.2017
 */

class DiffUtilCallback<T : IRecyclerItemViewModel> : DiffUtil.Callback() {
    val oldItems: MutableList<T> = ArrayList()
    val newItems: MutableList<T> = ArrayList()

    fun update(new: List<T>) = apply {
        oldItems.clear()
        oldItems.addAll(newItems)
        newItems.clear()
        newItems.addAll(new)
    }

    override fun getOldListSize(): Int = oldItems.size

    override fun getNewListSize(): Int = newItems.size

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int) =
            oldItems[oldItemPosition] == newItems[newItemPosition]

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int) =
            oldItems[oldItemPosition].id == newItems[newItemPosition].id
}