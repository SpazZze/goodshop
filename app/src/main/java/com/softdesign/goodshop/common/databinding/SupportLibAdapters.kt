package com.softdesign.goodshop.common.databinding

import android.databinding.BindingAdapter
import android.support.design.widget.FloatingActionButton
import android.support.v4.widget.SwipeRefreshLayout
import com.softdesign.goodshop.R
import com.softdesign.goodshop.utils.extentions.refreshColorState

/**
 * @author Space
 * @date 18.02.2017
 */

@BindingAdapter("setCustomColors")
fun setColorSchemeResources(swipeRefreshLayout: SwipeRefreshLayout, set: Boolean) {
    if (!set) return
    swipeRefreshLayout.setColorSchemeResources(
            R.color.colorRefreshProgress_1,
            R.color.colorRefreshProgress_2,
            R.color.colorRefreshProgress_3)
}

@BindingAdapter("enabled")
fun setEnabled(fab: FloatingActionButton, enabled: Boolean) {
    fab.isEnabled = enabled
    fab.refreshColorState(enabled)
}