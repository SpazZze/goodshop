package com.softdesign.goodshop.common.databinding

import android.databinding.BindingAdapter
import android.support.v4.view.ViewPager
import com.softdesign.goodshop.common.databinding.abs.BindingPagerAdapter

/**
 * @author elena
 * @date 23/12/16
 */

@Suppress("UNCHECKED_CAST")
@BindingAdapter("pagerItems", "currentPage")
fun <T : Any> setPagerItems(pager: ViewPager, items: List<T>?, position: Int) {
    val adapter = pager.adapter as? BindingPagerAdapter<T> ?: return
    items?.let { adapter.setItems(it) }
    if (adapter.count > position) pager.currentItem = position
}