package com.softdesign.goodshop.di.components

import com.softdesign.goodshop.data.managers.DataManager
import com.softdesign.goodshop.di.modules.*
import dagger.Component
import javax.inject.Singleton

/**
 * @author Space
 * @date 08.12.2016
 */

@Component(modules = arrayOf(
        NetworkModule::class,
        UserSettingsModule::class,
        CartModule::class,
        ProductsModule::class,
        CommentModule::class,
        AddressesModule::class))
@Singleton
interface DataManagerComponent {
    fun inject(dataManager: DataManager)
}
