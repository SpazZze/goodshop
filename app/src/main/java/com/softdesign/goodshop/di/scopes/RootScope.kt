package com.softdesign.goodshop.di.scopes

import javax.inject.Scope
import kotlin.reflect.KClass

/**
 * @author Space
 * @date 14.01.2017
 */

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class RootScope(val value: KClass<*>)