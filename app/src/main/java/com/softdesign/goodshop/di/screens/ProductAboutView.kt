package com.softdesign.goodshop.di.screens

import com.softdesign.goodshop.data.storage.dto.Product
import com.softdesign.goodshop.di.scopes.RootScope
import com.softdesign.goodshop.mvp.models.ProductAboutModel
import com.softdesign.goodshop.mvp.presenters.ProductAboutPresenter
import com.softdesign.goodshop.mvp.presenters.interfaces.IProductAboutPresenter
import com.softdesign.goodshop.mvp.views.screens.IProductInfoView
import com.softdesign.goodshop.ui.fragments.ProductAboutFragment
import dagger.Component
import dagger.Module
import dagger.Provides
import rx.subscriptions.CompositeSubscription

/**
 * @author Space
 * @date 30.01.2017
 */

//region :::::::::::::::::::::::::::::::::: ProductAboutPresenter
@Module
class ProductAboutPresenterModule(private val product: Product,
                                  private val subscription: CompositeSubscription) {

    @Provides
    @RootScope(IProductInfoView::class)
    fun provideProductAboutModel() = ProductAboutModel(subscription, product)
}

@Component(dependencies = arrayOf(RootScreenComponent::class),
        modules = arrayOf(ProductAboutPresenterModule::class))
@RootScope(IProductInfoView::class)
interface ProductAboutPresenterComponent {
    fun inject(presenter: ProductAboutPresenter)
}
//endregion :::::::::::::::::::::::::::::::::: ProductAboutPresenter

//region :::::::::::::::::::::::::::::::::: ProductAboutView
@Module
class ProductAboutViewModule(private val product: Product) {

    @Provides
    @RootScope(IProductInfoView::class)
    fun provideProductAboutPresenter(): IProductAboutPresenter = ProductAboutPresenter(product)
}

@Component(modules = arrayOf(ProductAboutViewModule::class))
@RootScope(IProductInfoView::class)
interface ProductAboutViewComponent {
    fun inject(fragment: ProductAboutFragment)
}
//endregion :::::::::::::::::::::::::::::::::: ProductAboutView