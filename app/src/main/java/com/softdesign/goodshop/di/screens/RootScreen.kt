package com.softdesign.goodshop.di.screens

import com.softdesign.goodshop.di.modules.FragmentManagerModule
import com.softdesign.goodshop.di.scopes.RootScope
import com.softdesign.goodshop.mvp.models.MenuCartModel
import com.softdesign.goodshop.mvp.models.NavHeaderModel
import com.softdesign.goodshop.mvp.presenters.RootPresenter
import com.softdesign.goodshop.mvp.presenters.interfaces.IRootPresenter
import com.softdesign.goodshop.ui.activities.RootActivity
import dagger.Component
import dagger.Module
import dagger.Provides
import rx.subscriptions.CompositeSubscription

/**
 * @author Space
 * @date 30.01.2017
 */


//region :::::::::::::::::::::::::::::::::: RootPresenter
@Module
class RootPresenterModule(private val subscription: CompositeSubscription,
                          private val presenter: IRootPresenter) {

    @Provides
    @RootScope(RootScope::class)
    fun provideNavHeaderModel() = NavHeaderModel(subscription, presenter)

    @Provides
    @RootScope(RootScope::class)
    fun provideRootModel() = MenuCartModel(subscription, presenter)
}

@Component(modules = arrayOf(RootPresenterModule::class))
@RootScope(RootScope::class)
interface RootPresenterComponent {
    fun inject(presenter: RootPresenter)
}
//endregion :::::::::::::::::::::::::::::::::: RootPresenter

//region :::::::::::::::::::::::::::::::::: RootView
@Module
class RootScreenModule {
    @Provides
    @RootScope(RootScope::class)
    fun provideRootPresenter(): IRootPresenter = RootPresenter()
}

@Component(modules = arrayOf(RootScreenModule::class, FragmentManagerModule::class))
@RootScope(RootScope::class)
interface RootScreenComponent {
    fun inject(activity: RootActivity)
    val presenter: IRootPresenter
}
//endregion :::::::::::::::::::::::::::::::::: RootView