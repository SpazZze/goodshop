package com.softdesign.goodshop.di.modules

import com.softdesign.goodshop.ui.view.interfaces.IFragmentFactory
import dagger.Module
import dagger.Provides

/**
 * @author Space
 * @date 31.01.2017
 */


@Module
class FragmentManagerModule(private val activity: IFragmentFactory) {

    @Provides
    fun provideFragmentManager(): IFragmentFactory = activity
}
