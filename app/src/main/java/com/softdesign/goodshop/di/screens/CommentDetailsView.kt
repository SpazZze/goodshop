package com.softdesign.goodshop.di.screens

import com.softdesign.goodshop.data.storage.dto.Comment
import com.softdesign.goodshop.di.scopes.RootScope
import com.softdesign.goodshop.mvp.models.CommentModel
import com.softdesign.goodshop.mvp.presenters.CommentPresenter
import com.softdesign.goodshop.mvp.presenters.interfaces.ICommentPresenter
import com.softdesign.goodshop.mvp.views.screens.ICommentView
import com.softdesign.goodshop.ui.fragments.CommentFragment
import dagger.Component
import dagger.Module
import dagger.Provides
import rx.subscriptions.CompositeSubscription

/**
 * @author Space
 * @date 30.01.2017
 */

//region :::::::::::::::::::::::::::::::::: CommentPresenter
@Module
class CommentPresenterModule(private val subscription: CompositeSubscription,
                             private val comment: Comment,
                             private val presenter: ICommentPresenter) {

    @Provides
    @RootScope(ICommentView::class)
    fun provideCommentModel() = CommentModel(subscription, comment, presenter)
}

@Component(dependencies = arrayOf(RootScreenComponent::class),
        modules = arrayOf(CommentPresenterModule::class))
@RootScope(ICommentView::class)
interface CommentPresenterComponent {
    fun inject(presenter: CommentPresenter)
}
//endregion :::::::::::::::::::::::::::::::::: CommentPresenter

//region :::::::::::::::::::::::::::::::::: ICommentView
@Module
class CommentDetailsModule(private val comment: Comment) {
    @Provides
    @RootScope(ICommentView::class)
    fun provideCommentPresenter(): ICommentPresenter = CommentPresenter(comment)
}

@Component(modules = arrayOf(CommentDetailsModule::class))
@RootScope(ICommentView::class)
interface CommentDetailsComponent {
    fun inject(fragment: CommentFragment)
}
//endregion :::::::::::::::::::::::::::::::::: ICommentView