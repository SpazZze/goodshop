package com.softdesign.goodshop.di.screens

import com.softdesign.goodshop.data.storage.dto.Product
import com.softdesign.goodshop.di.scopes.RootScope
import com.softdesign.goodshop.mvp.models.ProductInfoModel
import com.softdesign.goodshop.mvp.presenters.ProductInfoPresenter
import com.softdesign.goodshop.mvp.presenters.interfaces.IProductInfoPresenter
import com.softdesign.goodshop.mvp.views.screens.IProductInfoView
import com.softdesign.goodshop.ui.fragments.ProductInfoFragment
import dagger.Component
import dagger.Module
import dagger.Provides
import rx.subscriptions.CompositeSubscription

/**
 * @author Space
 * @date 30.01.2017
 */


//region :::::::::::::::::::::::::::::::::: ProductInfoPresenter
@Module
class ProductInfoPresenterModule(private val subscription: CompositeSubscription,
                                 private val product: Product,
                                 private val presenter: IProductInfoPresenter) {

    @Provides
    @RootScope(IProductInfoView::class)
    fun provideProductInfoModel() = ProductInfoModel(subscription, product, presenter)
}

@Component(dependencies = arrayOf(RootScreenComponent::class),
        modules = arrayOf(ProductInfoPresenterModule::class))
@RootScope(IProductInfoView::class)
interface ProductInfoPresenterComponent {
    fun inject(presenter: ProductInfoPresenter)
}
//endregion :::::::::::::::::::::::::::::::::: ProductInfoPresenter

//region :::::::::::::::::::::::::::::::::: IProductInfoView
@Module
class ProductInfoViewModule(private val product: Product) {
    @Provides
    @RootScope(IProductInfoView::class)
    fun provideProductInfoPresenter(): IProductInfoPresenter = ProductInfoPresenter(product)
}

@Component(modules = arrayOf(ProductInfoViewModule::class))
@RootScope(IProductInfoView::class)
interface ProductInfoViewComponent {
    fun inject(fragment: ProductInfoFragment)
}
//endregion :::::::::::::::::::::::::::::::::: IProductInfoView