package com.softdesign.goodshop.di.modules

import com.softdesign.goodshop.data.converters.CommentConverter
import com.softdesign.goodshop.data.converters.ProductConverter
import com.softdesign.goodshop.data.network.RestService
import com.softdesign.goodshop.data.providers.ProductProvider
import com.softdesign.goodshop.data.repositories.ProductRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * @author Space
 * @date 08.12.2016
 */


@Module
class ProductsModule {

    @Provides
    @Singleton
    fun provideProductProvider(service: RestService,
                               converter: ProductConverter,
                               repository: ProductRepository) = ProductProvider(service, converter, repository)

    @Provides
    @Singleton
    fun provideProductConverter(commentConverter: CommentConverter) = ProductConverter(commentConverter)

    @Provides
    @Singleton
    fun provideProductRepository() = ProductRepository()
}