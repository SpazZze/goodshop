package com.softdesign.goodshop.di.modules

import android.content.Context
import com.softdesign.goodshop.data.managers.DataManager
import dagger.Module
import dagger.Provides

/**
 * @author Space
 * @date 08.12.2016
 */

@Module
class AppModule(private val context: Context) {

    val dataManager = DataManager()

    @Provides
    fun provideDataManager() = dataManager

    @Provides
    fun provideContext() = context
}
