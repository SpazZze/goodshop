package com.softdesign.goodshop.di.components

import android.content.Context
import com.softdesign.goodshop.data.managers.DataManager
import com.softdesign.goodshop.di.modules.AppModule
import dagger.Component

/**
 * @author Space
 * @date 08.12.2016
 */

@Component(modules = arrayOf(AppModule::class))
interface AppComponent {
    val appContext: Context
    val dataManager: DataManager
}
