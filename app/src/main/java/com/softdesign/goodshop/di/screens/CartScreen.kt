package com.softdesign.goodshop.di.screens

import com.softdesign.goodshop.di.scopes.RootScope
import com.softdesign.goodshop.mvp.models.CartModel
import com.softdesign.goodshop.mvp.presenters.CartPresenter
import com.softdesign.goodshop.mvp.presenters.interfaces.ICartPresenter
import com.softdesign.goodshop.mvp.views.screens.ICartView
import com.softdesign.goodshop.ui.fragments.CartFragment
import dagger.Component
import dagger.Module
import dagger.Provides
import rx.subscriptions.CompositeSubscription

/**
 * @author Space
 * @date 16.04.2017
 */

//region :::::::::::::::::::::::::::::::::: CartPresenter
@Module
class CartPresenterModule(private val subscription: CompositeSubscription,
                          private val presenter: ICartPresenter) {

    @Provides
    @RootScope(ICartView::class)
    fun provideCartModel() = CartModel(subscription, presenter)
}

@Component(dependencies = arrayOf(RootScreenComponent::class),
        modules = arrayOf(CartPresenterModule::class))
@RootScope(ICartView::class)
interface CartPresenterComponent {
    fun inject(presenter: CartPresenter)
}
//endregion :::::::::::::::::::::::::::::::::: CartPresenter

//region :::::::::::::::::::::::::::::::::: ICartView
@Module
class CartViewModule {
    @Provides
    @RootScope(ICartView::class)
    fun provideCartPresenter(): ICartPresenter = CartPresenter()
}

@Component(modules = arrayOf(CartViewModule::class))
@RootScope(ICartView::class)
interface CartViewComponent {
    fun inject(fragment: CartFragment)
}
//endregion :::::::::::::::::::::::::::::::::: ICartView