package com.softdesign.goodshop.di

import com.softdesign.goodshop.data.storage.dto.Product
import com.softdesign.goodshop.di.scopes.DaggerScope
import com.softdesign.goodshop.di.scopes.RootScope
import com.softdesign.goodshop.mvp.views.screens.ICatalogView
import java.util.*
import kotlin.reflect.KClass

/**
 * @author Space
 * @date 14.01.2017
 */

@Suppress("UNCHECKED_CAST")
object DaggerService {

    @JvmStatic
    private val componentMap = HashMap<Class<*>, Any>()

    @JvmStatic
    private val productComponentMap = HashMap<String, Any>()

    @JvmStatic
    fun <T : Any> getComponent(clazz: Class<T>, dependencies: Array<Any>, vararg moduleClasses: Class<*>): T =
            componentMap[clazz] as? T ?:
                    createComponent(clazz, dependencies, *moduleClasses).apply { componentMap.put(clazz, this) }

    @JvmStatic
    fun <T : Any> getComponent(clazz: Class<T>, vararg moduleClasses: Class<*>): T =
            getComponent(clazz, arrayOf<Any>(), *moduleClasses)

    @JvmStatic
    fun <T : Any, C : Any> getComponent(clazz: Class<T>, dependencyComponentClass: Class<C>,
                                        dependencies: Array<Any>, vararg moduleClasses: Class<*>): T {
        val dependencyComponent = componentMap[dependencyComponentClass] ?: throw Exception("No such dependency component found")
        return getComponent(clazz, arrayOf(dependencyComponent, *dependencies), *moduleClasses)
    }

    @JvmStatic
    fun <T : Any> getProductComponent(clazz: Class<T>, product: Product,
                                      dependencies: Array<Any>, vararg moduleClasses: Class<*>): T =
            productComponentMap[clazz.name + product.id] as? T
                    ?: createComponent(clazz, dependencies, *moduleClasses)
                    .apply { productComponentMap.put(clazz.name + product.id, this) }


    @JvmStatic
    fun <T : Any, C : Any> getProductComponent(clazz: Class<T>, dependencyComponentClass: Class<C>,
                                               product: Product, dependencies: Array<Any>, vararg moduleClasses: Class<*>): T {
        val dependencyComponent = componentMap[dependencyComponentClass] ?: throw Exception("No such dependency component found")
        return productComponentMap[clazz.name + product.id] as? T ?:
                getProductComponent(clazz, product, arrayOf(dependencyComponent, *dependencies), *moduleClasses)
    }

    @JvmStatic
    fun unregisterScope(annotations: Array<Annotation>) = annotations.forEach {
        when (it) {
            is RootScope -> unregisterRootScope(it.value)
            is DaggerScope -> unregisterDaggerScopeDependency(it.value)
        }
    }

    @JvmStatic
    fun unregisterProduct(productId: String) = with(productComponentMap.entries.iterator()) {
        while (this.hasNext()) {
            if (this.next().key.contains(productId)) this.remove()
        }
    }

    //region :::::::::::::::::::::::::::::::::: Internal
    @JvmStatic
    private fun <T : Any> createComponent(componentClass: Class<T>, dependencies: Array<Any>, vararg moduleClasses: Class<*>): T = try {
        val fqn = componentClass.name
        val packageName = componentClass.`package`.name
        val simpleName = fqn.substring(packageName.length + 1)
        val generatedName = (packageName + ".Dagger" + simpleName).replace('$', '_')
        val generatedClass = Class.forName(generatedName)
        val builder = generatedClass.getMethod("builder").invoke(null)

        for (method in builder.javaClass.declaredMethods) {
            val params = method.parameterTypes
            if (params.size == 1) {
                val dependencyClass = params[0]
                for (dependency in dependencies) {
                    if (dependencyClass.isAssignableFrom(dependency.javaClass)) {
                        method.invoke(builder, dependency)
                        break
                    }
                }
                for (moduleClazz in moduleClasses) {
                    if (dependencyClass.isAssignableFrom(moduleClazz)) {
                        method.invoke(builder, moduleClazz.newInstance())
                        break
                    }
                }
            }
        }
        builder.javaClass.getMethod("build").invoke(builder) as T
    } catch (e: Exception) {
        throw RuntimeException(e)
    }

    @JvmStatic
    private fun unregisterRootScope(scopeClass: KClass<*>) {
        when (scopeClass) {
            ICatalogView::class -> productComponentMap.clear()
            RootScope::class -> unregisterFullScope(RootScope::class)
            else -> unregisterRootScopeDependency(scopeClass)
        }
    }

    @JvmStatic
    private fun unregisterRootScopeDependency(scopeClass: KClass<*>) = with(componentMap.entries.iterator()) {
        while (this.hasNext()) {
            val rootScope = this.next().key.getAnnotation(RootScope::class.java)
            if (rootScope?.value == scopeClass) this.remove()
        }
    }

    @JvmStatic
    private fun unregisterDaggerScopeDependency(scopeClass: KClass<*>) = with(componentMap.entries.iterator()) {
        while (this.hasNext()) {
            val daggerScope = this.next().key.getAnnotation(DaggerScope::class.java)
            if (daggerScope?.value == scopeClass) this.remove()
        }
    }

    @JvmStatic
    private fun unregisterFullScope(scopeAnnotation: KClass<out Annotation>) = with(componentMap.entries.iterator()) {
        while (this.hasNext()) {
            if (this.next().key.isAnnotationPresent(scopeAnnotation.java)) this.remove()
        }
    }
    //endregion :::::::::::::::::::::::::::::::::: Internal
}
