package com.softdesign.goodshop.di.modules

import com.softdesign.goodshop.data.converters.UserSettingsConverter
import com.softdesign.goodshop.data.network.RestService
import com.softdesign.goodshop.data.providers.UserSettingsProvider
import com.softdesign.goodshop.data.repositories.UserSettingsRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * @author Space
 * @date 10.12.2016
 */

@Module
class UserSettingsModule {

    @Provides
    @Singleton
    fun provideUserSettingsProvider(restService: RestService,
                                    converter: UserSettingsConverter,
                                    repository: UserSettingsRepository) =
            UserSettingsProvider(restService, converter, repository)

    @Provides
    @Singleton
    fun provideUserSettingsConverter() = UserSettingsConverter()

    @Provides
    @Singleton
    fun provideUserSettingsRepository() = UserSettingsRepository()
}