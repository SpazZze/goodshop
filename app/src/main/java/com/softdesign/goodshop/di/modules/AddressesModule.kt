package com.softdesign.goodshop.di.modules

import com.softdesign.goodshop.data.converters.AddressConverter
import com.softdesign.goodshop.data.providers.AddressProvider
import com.softdesign.goodshop.data.repositories.AddressRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * @author Space
 * @date 08.12.2016
 */

@Module
class AddressesModule {

    @Provides
    @Singleton
    fun provideAddressProvider(converter: AddressConverter,
                               repository: AddressRepository) = AddressProvider(converter, repository)

    @Provides
    @Singleton
    fun provideAddressConverter() = AddressConverter()

    @Provides
    @Singleton
    fun provideAddressRepository() = AddressRepository()

}