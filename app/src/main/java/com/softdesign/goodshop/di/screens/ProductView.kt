package com.softdesign.goodshop.di.screens

import com.softdesign.goodshop.data.storage.dto.Product
import com.softdesign.goodshop.di.scopes.RootScope
import com.softdesign.goodshop.mvp.models.ProductModel
import com.softdesign.goodshop.mvp.presenters.ProductPresenter
import com.softdesign.goodshop.mvp.presenters.interfaces.IProductPresenter
import com.softdesign.goodshop.mvp.views.screens.IProductView
import com.softdesign.goodshop.ui.fragments.ProductFragment
import dagger.Component
import dagger.Module
import dagger.Provides
import rx.subscriptions.CompositeSubscription

/**
 * @author Space
 * @date 30.01.2017
 */


//region :::::::::::::::::::::::::::::::::: ProductPresenter
@Module
class ProductPresenterModule(private val product: Product,
                             private val subscription: CompositeSubscription,
                             private val presenter: IProductPresenter) {

    @Provides
    @RootScope(IProductView::class)
    fun provideProductModel() = ProductModel(subscription, product, presenter)
}

@Component(dependencies = arrayOf(RootScreenComponent::class),
        modules = arrayOf(ProductPresenterModule::class))
@RootScope(IProductView::class)
interface ProductPresenterComponent {
    fun inject(presenter: ProductPresenter)
}
//endregion :::::::::::::::::::::::::::::::::: ProductPresenter

//region :::::::::::::::::::::::::::::::::: IProductView
@Module
class ProductViewModule(private val product: Product) {

    @Provides
    @RootScope(IProductView::class)
    fun provideProductPresenter(): IProductPresenter = ProductPresenter(product)
}

@Component(modules = arrayOf(ProductViewModule::class))
@RootScope(IProductView::class)
interface ProductViewComponent {
    fun inject(fragment: ProductFragment)
}
//endregion :::::::::::::::::::::::::::::::::: IProductView