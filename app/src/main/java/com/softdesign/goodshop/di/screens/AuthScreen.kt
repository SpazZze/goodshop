package com.softdesign.goodshop.di.screens

import com.softdesign.goodshop.di.scopes.DaggerScope
import com.softdesign.goodshop.mvp.models.AuthModel
import com.softdesign.goodshop.mvp.presenters.AuthPresenter
import com.softdesign.goodshop.mvp.presenters.interfaces.IAuthPresenter
import com.softdesign.goodshop.mvp.views.screens.IAuthView
import com.softdesign.goodshop.ui.activities.AuthActivity
import dagger.Component
import dagger.Module
import dagger.Provides
import rx.subscriptions.CompositeSubscription

/**
 * @author Space
 * @date 30.01.2017
 */

//region :::::::::::::::::::::::::::::::::: AuthPresenter
@Module
class AuthPresenterModule(private val subscription: CompositeSubscription,
                          private val presenter: IAuthPresenter) {
    @Provides
    @DaggerScope(IAuthView::class)
    fun provideAuthModel() = AuthModel(subscription, presenter)
}

@Component(modules = arrayOf(AuthPresenterModule::class))
@DaggerScope(IAuthView::class)
interface AuthPresenterComponent {
    fun inject(presenter: AuthPresenter)
}
//endregion :::::::::::::::::::::::::::::::::: AuthPresenter

//region :::::::::::::::::::::::::::::::::: IAuthView
@Module
class AuthViewModule {
    @Provides
    @DaggerScope(IAuthView::class)
    fun provideAuthPresenter(): IAuthPresenter = AuthPresenter()
}

@Component(modules = arrayOf(AuthViewModule::class))
@DaggerScope(IAuthView::class)
interface AuthViewComponent {
    fun inject(activity: AuthActivity)
}
//endregion :::::::::::::::::::::::::::::::::: IAuthView