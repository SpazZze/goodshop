package com.softdesign.goodshop.di.screens

import com.softdesign.goodshop.data.storage.dto.Address
import com.softdesign.goodshop.di.scopes.RootScope
import com.softdesign.goodshop.mvp.models.AddressModel
import com.softdesign.goodshop.mvp.presenters.AddressPresenter
import com.softdesign.goodshop.mvp.presenters.interfaces.IAddressPresenter
import com.softdesign.goodshop.mvp.views.screens.IAddressView
import com.softdesign.goodshop.ui.fragments.AddressFragment
import dagger.Component
import dagger.Module
import dagger.Provides
import rx.subscriptions.CompositeSubscription

/**
 * @author Space
 * @date 30.01.2017
 */

//region :::::::::::::::::::::::::::::::::: AddressPresenter
@Module
class AddressPresenterModule(private val subscription: CompositeSubscription,
                             private val address: Address,
                             private val presenter: IAddressPresenter) {
    @Provides
    @RootScope(IAddressView::class)
    fun provideAddressModel() = AddressModel(subscription, address, presenter)
}

@Component(dependencies = arrayOf(RootScreenComponent::class),
        modules = arrayOf(AddressPresenterModule::class))
@RootScope(IAddressView::class)
interface AddressPresenterComponent {
    fun inject(presenter: AddressPresenter)
}
//endregion :::::::::::::::::::::::::::::::::: AddressPresenter

//region :::::::::::::::::::::::::::::::::: IAddressView
@Module
class AddressViewModule(private val address: Address) {
    @Provides
    @RootScope(IAddressView::class)
    fun provideAddressPresenter(): IAddressPresenter = AddressPresenter(address)
}

@Component(modules = arrayOf(AddressViewModule::class))
@RootScope(IAddressView::class)
interface AddressViewComponent {
    fun inject(fragment: AddressFragment)
}
//endregion :::::::::::::::::::::::::::::::::: IAddressView