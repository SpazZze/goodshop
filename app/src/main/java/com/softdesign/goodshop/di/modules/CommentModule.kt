package com.softdesign.goodshop.di.modules

import com.softdesign.goodshop.data.converters.CommentConverter
import com.softdesign.goodshop.data.network.RestService
import com.softdesign.goodshop.data.providers.CommentProvider
import com.softdesign.goodshop.data.repositories.CommentRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * @author Space
 * @date 17.12.2016
 */


@Module
class CommentModule {

    @Provides
    @Singleton
    fun provideCommentProvider(service: RestService,
                               converter: CommentConverter,
                               repository: CommentRepository) = CommentProvider(service, converter, repository)

    @Provides
    @Singleton
    fun provideCommentConverter() = CommentConverter()

    @Provides
    @Singleton
    fun provideCommentRepository() = CommentRepository()
}