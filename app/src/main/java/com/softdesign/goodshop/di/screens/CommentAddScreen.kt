package com.softdesign.goodshop.di.screens

import com.softdesign.goodshop.di.scopes.RootScope
import com.softdesign.goodshop.mvp.models.CommentAddModel
import com.softdesign.goodshop.mvp.presenters.CommentAddPresenter
import com.softdesign.goodshop.mvp.presenters.interfaces.ICommentAddPresenter
import com.softdesign.goodshop.mvp.views.screens.ICommentView
import com.softdesign.goodshop.ui.fragments.CommentAddFragment
import dagger.Component
import dagger.Module
import dagger.Provides
import rx.subscriptions.CompositeSubscription

/**
 * @author Space
 * @date 30.01.2017
 */

//region :::::::::::::::::::::::::::::::::: CommentAddPresenter
@Module
class CommentAddPresenterModule(private val subscription: CompositeSubscription,
                                private val productId: String,
                                private val presenter: ICommentAddPresenter) {
    @Provides
    @RootScope(ICommentView::class)
    fun provideCommentModel() = CommentAddModel(subscription, productId, presenter)
}

@Component(dependencies = arrayOf(RootScreenComponent::class),
        modules = arrayOf(CommentAddPresenterModule::class))
@RootScope(ICommentView::class)
interface CommentAddPresenterComponent {
    fun inject(presenter: CommentAddPresenter)
}
//endregion :::::::::::::::::::::::::::::::::: CommentAddPresenter

//region :::::::::::::::::::::::::::::::::: ICommentView
@Module
class CommentAddViewModule(private val productId: String) {
    @Provides
    @RootScope(ICommentView::class)
    fun provideCommentPresenter(): ICommentAddPresenter = CommentAddPresenter(productId)
}

@Component(modules = arrayOf(CommentAddViewModule::class))
@RootScope(ICommentView::class)
interface CommentAddViewComponent {
    fun inject(fragment: CommentAddFragment)
}
//endregion :::::::::::::::::::::::::::::::::: ICommentView