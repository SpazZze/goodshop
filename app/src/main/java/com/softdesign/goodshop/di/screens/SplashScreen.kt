package com.softdesign.goodshop.di.screens

import com.softdesign.goodshop.di.scopes.DaggerScope
import com.softdesign.goodshop.mvp.models.SplashModel
import com.softdesign.goodshop.mvp.presenters.SplashPresenter
import com.softdesign.goodshop.mvp.presenters.interfaces.ISplashPresenter
import com.softdesign.goodshop.mvp.views.screens.ISplashView
import com.softdesign.goodshop.ui.activities.SplashActivity
import dagger.Component
import dagger.Module
import dagger.Provides
import rx.subscriptions.CompositeSubscription

/**
 * @author Space
 * @date 30.01.2017
 */


//region :::::::::::::::::::::::::::::::::: SplashPresenter
@Module
class SplashPresenterModule(private val subscription: CompositeSubscription,
                            private val presenter: ISplashPresenter) {
    @Provides
    @DaggerScope(ISplashView::class)
    fun provideSplashModel() = SplashModel(subscription, presenter)
}

@Component(modules = arrayOf(SplashPresenterModule::class))
@DaggerScope(ISplashView::class)
interface SplashPresenterComponent {
    fun inject(presenter: SplashPresenter)
}
//endregion :::::::::::::::::::::::::::::::::: SplashPresenter

//region :::::::::::::::::::::::::::::::::: SplashView
@Module
class SplashViewModule {
    @Provides
    @DaggerScope(ISplashView::class)
    fun provideSplashPresenter(): ISplashPresenter = SplashPresenter()
}

@Component(modules = arrayOf(SplashViewModule::class))
@DaggerScope(ISplashView::class)
interface SplashViewComponent {
    fun inject(activity: SplashActivity)
}
//endregion :::::::::::::::::::::::::::::::::: SplashView