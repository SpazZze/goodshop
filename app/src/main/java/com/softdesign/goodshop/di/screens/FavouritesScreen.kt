package com.softdesign.goodshop.di.screens

import com.softdesign.goodshop.di.scopes.RootScope
import com.softdesign.goodshop.mvp.models.FavouritesModel
import com.softdesign.goodshop.mvp.presenters.FavouritesPresenter
import com.softdesign.goodshop.mvp.presenters.interfaces.IFavouritesPresenter
import com.softdesign.goodshop.mvp.views.screens.IFavouritesView
import com.softdesign.goodshop.ui.fragments.FavouritesFragment
import dagger.Component
import dagger.Module
import dagger.Provides
import rx.subscriptions.CompositeSubscription

/**
 * @author Space
 * @date 03.04.2017
 */

//region :::::::::::::::::::::::::::::::::: FavouritesPresenter
@Module
class FavouritesPresenterModule(private val subscription: CompositeSubscription,
                                private val defaultNoContentStringId: Int,
                                private val presenter: IFavouritesPresenter) {

    @Provides
    @RootScope(IFavouritesView::class)
    fun provideFavouritesModel() = FavouritesModel(subscription, defaultNoContentStringId, presenter)
}

@Component(dependencies = arrayOf(RootScreenComponent::class),
        modules = arrayOf(FavouritesPresenterModule::class))
@RootScope(IFavouritesView::class)
interface FavouritesPresenterComponent {
    fun inject(presenter: FavouritesPresenter)
}
//endregion :::::::::::::::::::::::::::::::::: FavouritesPresenter

//region :::::::::::::::::::::::::::::::::: IFavouritesView
@Module
class FavouritesViewModule {
    @Provides
    @RootScope(IFavouritesView::class)
    fun provideFavouritesPresenter(): IFavouritesPresenter = FavouritesPresenter()
}

@Component(modules = arrayOf(FavouritesViewModule::class))
@RootScope(IFavouritesView::class)
interface FavouritesViewComponent {
    fun inject(fragment: FavouritesFragment)
}
//endregion :::::::::::::::::::::::::::::::::: IFavouritesView