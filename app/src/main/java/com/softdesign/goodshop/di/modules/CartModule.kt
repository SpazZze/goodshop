package com.softdesign.goodshop.di.modules

import com.softdesign.goodshop.data.converters.CartItemConverter
import com.softdesign.goodshop.data.converters.ProductConverter
import com.softdesign.goodshop.data.network.RestService
import com.softdesign.goodshop.data.providers.CartItemProvider
import com.softdesign.goodshop.data.repositories.CartItemRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * @author Space
 * @date 10.12.2016
 */

@Module
class CartModule {

    @Provides
    @Singleton
    fun provideCartItemProvider(converter: CartItemConverter,
                                repository: CartItemRepository,
                                service: RestService) = CartItemProvider(converter, repository, service)

    @Provides
    @Singleton
    fun provideCartItemConverter(productConverter: ProductConverter) = CartItemConverter(productConverter)

    @Provides
    @Singleton
    fun provideCartItemRepository() = CartItemRepository()
}