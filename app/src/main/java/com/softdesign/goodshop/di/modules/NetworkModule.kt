package com.softdesign.goodshop.di.modules

import com.facebook.stetho.okhttp3.StethoInterceptor
import com.softdesign.goodshop.data.network.RestService
import com.softdesign.goodshop.data.network.RxErrorHandlingCallAdapterFactory
import com.softdesign.goodshop.data.network.interceptors.HeaderInterceptor
import com.softdesign.goodshop.utils.Config
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

/**
 * @author Space
 * @date 11.12.2016
 */

@Module
class NetworkModule {

    @Provides
    @Singleton
    fun provideOkHttpClient(): OkHttpClient = OkHttpClient().newBuilder()
            .addInterceptor(HeaderInterceptor())
            .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
            .addNetworkInterceptor(StethoInterceptor())
            .connectTimeout(Config.MAX_CONNECTION_TIMEOUT, TimeUnit.MILLISECONDS)
            .readTimeout(Config.MAX_READ_TIMEOUT, TimeUnit.MILLISECONDS)
            .writeTimeout(Config.MAX_WRITE_TIMEOUT, TimeUnit.MILLISECONDS)
            .build()

    @Provides
    @Singleton
    fun provideRetrofit(client: OkHttpClient): Retrofit = Retrofit.Builder()
            .baseUrl(Config.API_URL)
            .addCallAdapterFactory(RxErrorHandlingCallAdapterFactory())
            .addConverterFactory(GsonConverterFactory.create())
            .client(client)
            .build()

    @Provides
    @Singleton
    fun provideRestService(retrofit: Retrofit): RestService = retrofit.create(RestService::class.java)
}
