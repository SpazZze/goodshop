package com.softdesign.goodshop.di.screens

import com.softdesign.goodshop.di.scopes.RootScope
import com.softdesign.goodshop.mvp.models.AccountModel
import com.softdesign.goodshop.mvp.presenters.AccountPresenter
import com.softdesign.goodshop.mvp.presenters.interfaces.IAccountPresenter
import com.softdesign.goodshop.mvp.views.screens.IAccountView
import com.softdesign.goodshop.ui.fragments.AccountFragment
import dagger.Component
import dagger.Module
import dagger.Provides
import rx.subscriptions.CompositeSubscription

/**
 * @author Space
 * @date 30.01.2017
 */


//region :::::::::::::::::::::::::::::::::: AccountPresenter
@Module
class AccountPresenterModule(private val subscription: CompositeSubscription,
                             private val presenter: IAccountPresenter) {
    @Provides
    @RootScope(IAccountView::class)
    fun provideAccountModel() = AccountModel(subscription, presenter)
}

@Component(dependencies = arrayOf(RootScreenComponent::class),
        modules = arrayOf(AccountPresenterModule::class))
@RootScope(IAccountView::class)
interface AccountPresenterComponent {
    fun inject(presenter: AccountPresenter)
}
//endregion :::::::::::::::::::::::::::::::::: AccountPresenter

//region :::::::::::::::::::::::::::::::::: IAccountView
@Module
class AccountViewModule {
    @Provides
    @RootScope(IAccountView::class)
    fun provideAccountPresenter(): IAccountPresenter = AccountPresenter()
}

@Component(modules = arrayOf(AccountViewModule::class))
@RootScope(IAccountView::class)
interface AccountViewComponent {
    fun inject(fragment: AccountFragment)
}
//endregion :::::::::::::::::::::::::::::::::: IAccountView