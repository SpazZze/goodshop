package com.softdesign.goodshop.di.screens

import com.softdesign.goodshop.di.scopes.RootScope
import com.softdesign.goodshop.mvp.models.CatalogModel
import com.softdesign.goodshop.mvp.presenters.CatalogPresenter
import com.softdesign.goodshop.mvp.presenters.interfaces.ICatalogPresenter
import com.softdesign.goodshop.mvp.views.screens.ICatalogView
import com.softdesign.goodshop.ui.fragments.CatalogFragment
import dagger.Component
import dagger.Module
import dagger.Provides
import rx.subscriptions.CompositeSubscription

/**
 * @author Space
 * @date 30.01.2017
 */

//region :::::::::::::::::::::::::::::::::: CatalogPresenter
@Module
class CatalogPresenterModule(private val subscription: CompositeSubscription,
                             private val defaultNoContentStringId: Int,
                             private val presenter: ICatalogPresenter) {

    @Provides
    @RootScope(ICatalogView::class)
    fun provideCatalogModel() = CatalogModel(subscription, defaultNoContentStringId, presenter)
}

@Component(dependencies = arrayOf(RootScreenComponent::class),
        modules = arrayOf(CatalogPresenterModule::class))
@RootScope(ICatalogView::class)
interface CatalogPresenterComponent {
    fun inject(presenter: CatalogPresenter)
}
//endregion :::::::::::::::::::::::::::::::::: CatalogPresenter

//region :::::::::::::::::::::::::::::::::: ICatalogView
@Module
class CatalogViewModule {
    @Provides
    @RootScope(ICatalogView::class)
    fun provideCatalogPresenter(): ICatalogPresenter = CatalogPresenter()
}

@Component(modules = arrayOf(CatalogViewModule::class))
@RootScope(ICatalogView::class)
interface CatalogViewComponent {
    fun inject(fragment: CatalogFragment)
}
//endregion :::::::::::::::::::::::::::::::::: ICatalogView