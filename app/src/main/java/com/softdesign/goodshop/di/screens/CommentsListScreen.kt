package com.softdesign.goodshop.di.screens

import com.softdesign.goodshop.di.scopes.RootScope
import com.softdesign.goodshop.mvp.models.CommentsListModel
import com.softdesign.goodshop.mvp.presenters.CommentsListPresenter
import com.softdesign.goodshop.mvp.presenters.interfaces.ICommentsListPresenter
import com.softdesign.goodshop.mvp.views.screens.IProductInfoView
import com.softdesign.goodshop.ui.fragments.CommentsListFragment
import dagger.Component
import dagger.Module
import dagger.Provides
import rx.subscriptions.CompositeSubscription

/**
 * @author Space
 * @date 30.01.2017
 */


//region :::::::::::::::::::::::::::::::::: CommentsListPresenter
@Module
class CommentsListPresenterModule(private val productId: String,
                                  private val subscription: CompositeSubscription,
                                  private val defaultNoContentStringId: Int,
                                  private val presenter: ICommentsListPresenter) {

    @Provides
    @RootScope(IProductInfoView::class)
    fun provideCommentsListModel() = CommentsListModel(productId, subscription, defaultNoContentStringId, presenter)
}

@Component(dependencies = arrayOf(RootScreenComponent::class),
        modules = arrayOf(CommentsListPresenterModule::class))
@RootScope(IProductInfoView::class)
interface CommentsListPresenterComponent {
    fun inject(presenter: CommentsListPresenter)
}
//endregion :::::::::::::::::::::::::::::::::: CommentsListPresenter

//region :::::::::::::::::::::::::::::::::: CommentsListView
@Module
class CommentsListViewModule(private val productId: String) {

    @Provides
    @RootScope(IProductInfoView::class)
    fun provideCommentListPresenter(): ICommentsListPresenter = CommentsListPresenter(productId)
}

@Component(modules = arrayOf(CommentsListViewModule::class))
@RootScope(IProductInfoView::class)
interface CommentsListViewComponent {
    fun inject(fragment: CommentsListFragment)
}
//endregion :::::::::::::::::::::::::::::::::: CommentsListView