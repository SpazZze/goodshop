package com.softdesign.goodshop.utils

import com.softdesign.goodshop.data.network.api.res.AuthRes
import com.softdesign.goodshop.data.storage.dto.UserSettings
import rx.Single

/**
 * @author Space
 * @date 29.01.2017
 */


fun mockUserSettings() = UserSettings(
        "Иннокентий Григорьевич Шаурмадзе-Шампурыч",
        "http://images.akamai.steamusercontent.com/ugc/3263170909113198096/DDE44BE3C6ED8C1FAF34F2FD96F3BC7C059BE1C8/",
        "79484848484848",
        "test@test.",
        true,
        false)

fun mockAuthReq(): Single<AuthRes> =
        Single.just(AuthRes("DDE44BE3C6ED8C1FAF34F2FD96F3BC7C059BE1C8", mockUserSettings()))