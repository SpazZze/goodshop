package com.softdesign.goodshop.utils.extentions

import android.content.Context
import com.softdesign.goodshop.common.App
import com.softdesign.goodshop.data.managers.PreferencesManager
import kotlin.properties.Delegates

@Suppress("UNUSED")
fun <T> Delegates.preference(key: String,
                             default: T,
                             prefsName: String = PreferencesManager.prefsName,
                             context: Context = App.ctx) = Preference(key, default, prefsName, context)