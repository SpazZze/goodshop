package com.softdesign.goodshop.utils.extentions

import com.softdesign.goodshop.utils.Config
import timber.log.Timber
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

/**
 * @author Space
 * @date 17.12.2016
 */

fun Date.convertDate(format: String = Config.LAST_MODIFIED_DATE_FORMAT) =
        SimpleDateFormat(format, Locale.US).format(this)!!

fun provideInitDate(format: String = Config.LAST_MODIFIED_DATE_FORMAT) =
        GregorianCalendar(1900, Calendar.JANUARY, 1).time.convertDate(format)

fun parseDate(input: String?, format: String = Config.COMMENT_DATE_FORMAT): Date = try {
    SimpleDateFormat(format, Locale.US).parse(input ?: "")
} catch (e: ParseException) {
    Timber.e(e, "DateConverter parseDate error: "); Date()
}