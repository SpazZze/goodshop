package com.softdesign.goodshop.utils.extentions

import java.util.*

/**
 * @author Space
 * @date 24.01.2017
 */


@Synchronized
fun <T> MutableCollection<T>.replaceAllBy(list: List<T>) =
        if (list != this) {
            clear()
            if (list.isNotEmpty()) addAll(list) else true
        } else false

inline fun <T, R, C : MutableCollection<in R>> Iterable<T>.mapToWithReplace(destination: C, transform: (T) -> R) =
        destination.replaceAllBy(this.mapTo(ArrayList(), { transform(it) }))

@Synchronized
fun <T, V> MutableMap<T, V>.replaceAllBy(list: List<V>, key: (V) -> T) {
    if (list != this.values) {
        clear()
        list.forEach { this.put(key(it), it) }
    }
}