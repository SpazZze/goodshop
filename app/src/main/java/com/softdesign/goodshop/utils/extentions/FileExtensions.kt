package com.softdesign.goodshop.utils.extentions

import android.graphics.Bitmap
import android.graphics.Bitmap.CompressFormat
import java.io.File
import java.io.FileOutputStream
import java.io.IOException


/**
 * @author Space
 * @date 05.03.2017
 */

@Throws(IOException::class)
fun File.create(): File = apply {
    File(parent).mkdirs()
    if (exists()) delete()
    createNewFile()
}

fun Bitmap.resize(size: Int) = (width.toFloat() / height.toFloat()).let {
    when (it > 1f) {
        true -> Bitmap.createScaledBitmap(this, size, (size / it).toInt(), true)
        else -> Bitmap.createScaledBitmap(this, (size * it).toInt(), size, true)
    }
}!!

fun Bitmap.compressInto(format: CompressFormat, quality: Int, file: File) {
    FileOutputStream(file).use { compress(format, quality, it) }
}