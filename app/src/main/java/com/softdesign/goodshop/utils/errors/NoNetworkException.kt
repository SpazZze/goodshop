package com.softdesign.goodshop.utils.errors

import java.io.IOException

/**
 * @author Space
 * @date 18.02.2017
 */

class NoNetworkException : IOException()