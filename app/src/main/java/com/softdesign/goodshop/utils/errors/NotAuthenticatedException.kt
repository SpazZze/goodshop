package com.softdesign.goodshop.utils.errors

/**
 * @author Space
 * @date 03.04.2017
 */

class NotAuthenticatedException : Exception()