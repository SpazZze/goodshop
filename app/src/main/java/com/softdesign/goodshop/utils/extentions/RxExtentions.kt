package com.softdesign.goodshop.utils.extentions

import com.softdesign.goodshop.common.App
import com.softdesign.goodshop.utils.errors.NoNetworkException
import com.softdesign.goodshop.utils.errors.NotAuthenticatedException
import rx.Observable
import rx.Observer
import rx.Single
import rx.Subscription
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import rx.subscriptions.CompositeSubscription

/**
 * @author Space
 * @date 28.01.2017
 */

fun <T> Single<T>.withConnectionStatusCheck(): Single<T> = Single.just(isNetworkAvailable())
        .flatMap { if (it) this else Single.error(NoNetworkException()) }

fun <T> Single<T>.withAuthCheck(): Single<T> = Single.just(App.userAccessToken)
        .flatMap { if (it.isNotBlank()) this else Single.error(NotAuthenticatedException()) }

fun <T> Single<T>.runOnIoObsOnMain(): Single<T> = this
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())

fun <T> Single<T>.runOnIo(): Single<T> = this
        .subscribeOn(Schedulers.io())

fun <T> Observable<T>.runOnIoObsOnMain(): Observable<T> = this
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())

fun <T> Observable<T>.runOnIo(): Observable<T> = this
        .subscribeOn(Schedulers.io())

inline fun <T, reified A : Any> A.silentObserver(crossinline onNextAction: (T) -> Unit) = object : Observer<T> {

    override fun onNext(t: T) = onNextAction(t)

    override fun onCompleted() = Unit

    override fun onError(e: Throwable) = e.reportToDeveloper("$javaClass")
}

inline fun <T, reified A : Any> A.reportingObserver(crossinline onErrorAction: (Throwable) -> Unit) = object : Observer<T> {

    override fun onNext(t: T) = Unit

    override fun onCompleted() = Unit

    override fun onError(e: Throwable) {
        e.reportToDeveloper("$javaClass")
        onErrorAction(e)
    }
}

inline fun <T, reified A : Any> A.reportingObserver(crossinline onNextAction: (T) -> Unit,
                                                    crossinline onErrorAction: (Throwable) -> Unit) = object : Observer<T> {
    override fun onNext(t: T) = onNextAction(t)

    override fun onCompleted() = Unit

    override fun onError(e: Throwable) {
        e.reportToDeveloper("$javaClass")
        onErrorAction(e)
    }
}

fun Subscription.addAsSingleInstanceTo(compositeSubscription: CompositeSubscription) = apply {
    compositeSubscription.clear()
    compositeSubscription.add(this)
}