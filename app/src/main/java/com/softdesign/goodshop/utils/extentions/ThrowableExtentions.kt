package com.softdesign.goodshop.utils.extentions

import android.content.Context
import android.util.Log
import com.softdesign.goodshop.R
import com.softdesign.goodshop.common.App
import com.softdesign.goodshop.data.network.RetrofitException
import com.softdesign.goodshop.data.network.RetrofitException.Kind
import com.softdesign.goodshop.utils.errors.NoNetworkException
import com.softdesign.goodshop.utils.errors.NotAuthenticatedException
import timber.log.Timber
import java.net.SocketTimeoutException

/**
 * @author Space
 * @date 29.01.2017
 */

fun Throwable.networkErrorMsg(context: Context = App.ctx) = when {
    this is NotAuthenticatedException -> context.getString(R.string.error_authentication_required)
    this is NoNetworkException -> context.getString(R.string.error_no_network_connection)
    this is RetrofitException && kind == Kind.HTTP && response!!.code() < 400 -> ""
    this is RetrofitException && exception is SocketTimeoutException -> context.getString(R.string.error_timeout)
    else -> context.getString(R.string.error_server_error)
}!!

fun Throwable.reportToDeveloper(clazz: String) {
    val msg = "$clazz failure in subscription: "
    when {
        this is NotAuthenticatedException -> Log.e("DEV", "$msg NotAuthenticatedException")
        this is NoNetworkException -> Log.e("DEV", "$msg NoNetworkException")
        this is RetrofitException && kind == RetrofitException.Kind.HTTP && response!!.code() < 400 -> Unit
        this is RetrofitException && exception is SocketTimeoutException -> Log.e("DEV", "$msg SocketTimeoutException")
        else -> Timber.e(this, msg)
    }
}