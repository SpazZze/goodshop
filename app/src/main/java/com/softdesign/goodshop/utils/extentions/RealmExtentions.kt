package com.softdesign.goodshop.utils.extentions

import io.realm.Realm

fun Realm.auto(function: Realm.() -> Unit) {
    use { function(this) }
}

fun Realm.autoExecuteTransaction(function: Realm.() -> Unit) {
    use { executeTransaction { function(this) } }
}


