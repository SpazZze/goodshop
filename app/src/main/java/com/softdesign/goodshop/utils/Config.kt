package com.softdesign.goodshop.utils

/**
 * @author Space
 * @date 11.12.2016
 */

object Config {

    // Rest API
    val API_URL = "https://skba1.mgbeta.ru/api/v1/"
    val COMMENT_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
    val LAST_MODIFIED_DATE_FORMAT = "EEE, dd MMM yyyy HH:mm:ss z"

    // Network timeouts
    val MAX_CONNECTION_TIMEOUT: Long = 10000
    val MAX_READ_TIMEOUT: Long = 10000
    val MAX_WRITE_TIMEOUT: Long = 10000

    // Picture cache
    val MAX_GLIDE_CACHE_SIZE = 1024 * 1024 * 500   //500 Mb

    // Splash screen fade-in delay
    val SPLASH_FADE_DELAY: Long = 1000

    // Largest screen dpi multiplier
    val XXXHDPI_MULTIPLIER = 4

    // Should be get from API request, but for now it is constant
    val SUM_FOR_DISCOUNT = 3000
}