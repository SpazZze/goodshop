package com.softdesign.goodshop.utils.extentions

/**
 * @author Space
 * @date 28.01.2017
 */

inline fun <T> T.applyIf(condition: Boolean, block: T.() -> Unit): T = when {
    condition -> apply { block() }
    else -> this
}

inline fun runIf(condition: Boolean, block: () -> Unit) = condition.apply { if (this) block() }

inline fun <R> execute(block: () -> R): Unit {
    block()
}