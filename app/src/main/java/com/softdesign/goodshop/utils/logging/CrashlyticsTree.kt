package com.softdesign.goodshop.utils.logging

import com.crashlytics.android.Crashlytics
import timber.log.Timber


/**
 * @author Space
 * @date 15.01.2017
 */

class CrashlyticsTree : Timber.DebugTree() {
    override fun log(priority: Int, tag: String?, message: String?, t: Throwable?) {
        Crashlytics.log(message)
        Crashlytics.logException(t ?: Throwable(message))
        super.log(priority, "DEV", message, t)
    }
}