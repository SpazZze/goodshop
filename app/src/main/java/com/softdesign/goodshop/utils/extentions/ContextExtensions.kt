package com.softdesign.goodshop.utils.extentions

import android.content.ContentResolver
import android.content.Context
import android.content.Intent
import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.ConnectivityManager
import android.net.Uri
import android.provider.MediaStore
import android.provider.Settings
import android.support.annotation.DimenRes
import android.support.annotation.StringRes
import android.support.v4.app.Fragment
import com.softdesign.goodshop.common.App
import timber.log.Timber
import java.io.File
import java.io.FileNotFoundException

/**
 * @author Space
 * @date 29.01.2017
 */

fun isNetworkAvailable(context: Context = App.ctx) =
        (context.getSystemService(Context.CONNECTIVITY_SERVICE) as? ConnectivityManager)
                ?.activeNetworkInfo?.isConnectedOrConnecting
                ?: false

fun Context.showAppSettings() {
    startActivity(Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.parse("package:$packageName")))
}

fun Fragment.takePicture(file: File, intentId: Int) = try {
    val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file))
    startActivityForResult(takePictureIntent, intentId)
} catch (e: Exception) {
    Timber.e(e, "takePhoto intent error: ")
}

fun Fragment.chooseFromGallery(@StringRes titleRes: Int, intentId: Int) = try {
    val takeFromGalleryIntent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
    takeFromGalleryIntent.type = "image/*"
    startActivityForResult(Intent.createChooser(takeFromGalleryIntent, getString(titleRes)), intentId)
} catch (e: Exception) {
    Timber.e(e, "chooseFromGallery intent error: ")
}

@Throws(Resources.NotFoundException::class)
fun Context.getDimensionValue(@DimenRes dimenRes: Int) = with(resources) { (getDimension(dimenRes) / displayMetrics.density).toInt() }

@Throws(FileNotFoundException::class)
fun ContentResolver.getResizedBitmap(imageUri: Uri, maxImagePixelSize: Int): Bitmap {
    val options = BitmapFactory.Options().apply { inJustDecodeBounds = true }
    openInputStream(imageUri).use { BitmapFactory.decodeStream(it, null, options) }
    options.apply {
        inJustDecodeBounds = false
        if (maxImagePixelSize > 0) inSampleSize = Math.max(outWidth, outHeight) / maxImagePixelSize
    }
    return openInputStream(imageUri).use { BitmapFactory.decodeStream(it, null, options) }
}