package com.softdesign.goodshop.utils.extentions

/**
 * @author Space
 * @date 21.02.2017
 */

fun CharSequence?.isNotNullOrBlank(): Boolean = this != null && this.isNotBlank()

fun CharSequence?.match(pattern: String) = this != null && this.trim().matches(pattern.toRegex())