package com.softdesign.goodshop.mvp.views.screens

import com.softdesign.goodshop.mvp.models.CommentModel
import com.softdesign.goodshop.mvp.views.IRootChildWithToolbar

/**
 * @author Space
 * @date 05.01.2017
 */

interface ICommentView : IRootChildWithToolbar<CommentModel>