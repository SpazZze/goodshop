package com.softdesign.goodshop.mvp.presenters.interfaces

import com.softdesign.goodshop.mvp.models.AddressModel
import com.softdesign.goodshop.mvp.presenters.abs.interfaces.IChildPresenterWithToolbar
import com.softdesign.goodshop.mvp.views.screens.IAddressView

/**
 * @author Space
 * @date 11.12.2016
 */

interface IAddressPresenter : IChildPresenterWithToolbar<IAddressView, AddressModel> {

    val addressId: Int
}
