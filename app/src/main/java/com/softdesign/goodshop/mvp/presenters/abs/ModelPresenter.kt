package com.softdesign.goodshop.mvp.presenters.abs

import android.support.annotation.CallSuper
import com.softdesign.goodshop.mvp.models.abs.BaseModel
import com.softdesign.goodshop.mvp.presenters.abs.interfaces.IModelPresenter
import com.softdesign.goodshop.mvp.views.IModelView
import rx.subscriptions.CompositeSubscription
import javax.inject.Inject

/**
 * @author Space
 * @date 05.01.2017
 */

abstract class ModelPresenter<V : IModelView<M>, M : BaseModel> : BasePresenter<V>(), IModelPresenter<V, M> {

    @Inject
    lateinit override var model: M

    var subscription = CompositeSubscription()

    @CallSuper
    override fun onDestroy() {
        subscription.clear()
    }

    @CallSuper
    override fun finishView() {
        onDestroy()
        super.finishView()
    }
}
