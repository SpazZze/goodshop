package com.softdesign.goodshop.mvp.presenters.interfaces

import com.softdesign.goodshop.mvp.models.SplashModel
import com.softdesign.goodshop.mvp.presenters.abs.interfaces.IModelPresenter
import com.softdesign.goodshop.mvp.views.screens.ISplashView

/**
 * @author Space
 * @date 05.01.2017
 */

interface ISplashPresenter : IModelPresenter<ISplashView, SplashModel>