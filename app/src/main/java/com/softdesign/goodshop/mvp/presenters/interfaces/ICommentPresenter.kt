package com.softdesign.goodshop.mvp.presenters.interfaces

import com.softdesign.goodshop.mvp.models.CommentModel
import com.softdesign.goodshop.mvp.presenters.abs.interfaces.IChildPresenterWithToolbar
import com.softdesign.goodshop.mvp.views.screens.ICommentView

/**
 * @author Space
 * @date 05.01.2017
 */

interface ICommentPresenter : IChildPresenterWithToolbar<ICommentView, CommentModel>
