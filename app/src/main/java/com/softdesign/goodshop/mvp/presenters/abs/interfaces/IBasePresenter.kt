package com.softdesign.goodshop.mvp.presenters.abs.interfaces

import com.softdesign.goodshop.mvp.views.IBaseView

/**
 * @author Space
 * @date 11.12.2016
 */

interface IBasePresenter<V : IBaseView> {

    fun initView()

    fun dropView()

    fun takeView(view: V)

    fun getView(): V?

    fun finishView()

    fun onDestroy()
}
