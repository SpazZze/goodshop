package com.softdesign.goodshop.mvp.presenters.interfaces

import com.softdesign.goodshop.mvp.models.CartModel
import com.softdesign.goodshop.mvp.presenters.abs.interfaces.IChildPresenterWithToolbar
import com.softdesign.goodshop.mvp.views.screens.ICartView

/**
 * @author Space
 * @date 15.04.2017
 */

interface ICartPresenter : IChildPresenterWithToolbar<ICartView, CartModel> {

    fun removeItemFromCart(cartItemId: String, itemName: String)
}