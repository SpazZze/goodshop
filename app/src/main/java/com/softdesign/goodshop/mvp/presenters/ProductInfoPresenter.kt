package com.softdesign.goodshop.mvp.presenters

import com.softdesign.goodshop.data.storage.dto.Product
import com.softdesign.goodshop.di.screens.ProductInfoPresenterComponent
import com.softdesign.goodshop.di.screens.ProductInfoPresenterModule
import com.softdesign.goodshop.mvp.models.ProductInfoModel
import com.softdesign.goodshop.mvp.presenters.abs.ChildPresenterWithToolbar
import com.softdesign.goodshop.mvp.presenters.interfaces.IProductInfoPresenter
import com.softdesign.goodshop.mvp.views.screens.IProductInfoView

/**
 * @author Space
 * @date 18.12.2016
 */


class ProductInfoPresenter(product: Product) :
        ChildPresenterWithToolbar<IProductInfoView, ProductInfoModel>(), IProductInfoPresenter {

    override val product: Product
        get() = model.thisProduct

    init {
        injectRootChild(ProductInfoPresenterComponent::class.java,
                arrayOf<Any>(ProductInfoPresenterModule(subscription, product, this))).inject(this)
    }

    override fun initView() {
        super<ChildPresenterWithToolbar>.initView()
        getView()?.setupViewPager()
    }
}
