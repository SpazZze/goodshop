package com.softdesign.goodshop.mvp.presenters.abs

import android.support.annotation.CallSuper
import com.softdesign.goodshop.mvp.models.abs.BaseModel
import com.softdesign.goodshop.mvp.presenters.abs.interfaces.IBindingPresenter
import com.softdesign.goodshop.mvp.views.IBindingView

/**
 * @author elena
 * @date 19/12/16
 */

abstract class BindingPresenter<V : IBindingView<M>, M : BaseModel> : ModelPresenter<V, M>(), IBindingPresenter<V, M> {

    @CallSuper
    override fun initView() {
        getView()?.setViewModel(model)
    }
}
