package com.softdesign.goodshop.mvp.presenters.interfaces

import com.softdesign.goodshop.data.storage.dto.Product
import com.softdesign.goodshop.mvp.models.CatalogModel
import com.softdesign.goodshop.mvp.presenters.abs.interfaces.IChildPresenterWithToolbar
import com.softdesign.goodshop.mvp.views.screens.ICatalogView

/**
 * @author Space
 * @date 11.12.2016
 */

interface ICatalogPresenter : IChildPresenterWithToolbar<ICatalogView, CatalogModel> {

    fun addItemToCart(product: Product, count: Int)
}
