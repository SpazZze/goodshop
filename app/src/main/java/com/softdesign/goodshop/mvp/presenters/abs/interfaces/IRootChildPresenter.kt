package com.softdesign.goodshop.mvp.presenters.abs.interfaces

import com.softdesign.goodshop.data.storage.dto.Product
import com.softdesign.goodshop.di.DaggerService
import com.softdesign.goodshop.di.screens.RootScreenComponent
import com.softdesign.goodshop.mvp.models.abs.BaseModel
import com.softdesign.goodshop.mvp.presenters.interfaces.IRootPresenter
import com.softdesign.goodshop.mvp.views.IRootChildView
import com.softdesign.goodshop.ui.view.interfaces.IFragmentFactory

/**
 * @author Space
 * @date 31.01.2017
 */

interface IRootChildPresenter<V : IRootChildView<M>, M : BaseModel> : IBindingPresenter<V, M> {

    var rootPresenter: IRootPresenter

    val fragmentFactory: IFragmentFactory?

    fun <C : Any> injectRootChild(childComponentClass: Class<C>, dependencies: Array<Any>, vararg moduleClasses: Class<*>): C =
            DaggerService.getComponent(childComponentClass, RootScreenComponent::class.java, dependencies, *moduleClasses)

    fun <C : Any> injectRootChildProduct(childComponentClass: Class<C>, product: Product,
                                         dependencies: Array<Any>, vararg moduleClasses: Class<*>): C =
            DaggerService.getProductComponent(childComponentClass, RootScreenComponent::class.java,
                    product, dependencies, *moduleClasses)
}