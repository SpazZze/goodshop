package com.softdesign.goodshop.mvp.presenters

import com.softdesign.goodshop.R
import com.softdesign.goodshop.di.screens.CommentsListPresenterComponent
import com.softdesign.goodshop.di.screens.CommentsListPresenterModule
import com.softdesign.goodshop.mvp.models.CommentsListModel
import com.softdesign.goodshop.mvp.presenters.abs.RootChildPresenter
import com.softdesign.goodshop.mvp.presenters.interfaces.ICommentsListPresenter
import com.softdesign.goodshop.mvp.views.screens.ICommentsListView

/**
 * @author elena
 * @date 23/12/16
 */

class CommentsListPresenter(productId: String) : RootChildPresenter<ICommentsListView, CommentsListModel>(), ICommentsListPresenter {

    init {
        injectRootChild(CommentsListPresenterComponent::class.java,
                arrayOf<Any>(CommentsListPresenterModule(productId, subscription, R.string.no_content_comment_list, this))).inject(this)
    }

    override fun initView() {
        super<RootChildPresenter>.initView()
        model.loadItems()
    }
}
