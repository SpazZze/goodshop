package com.softdesign.goodshop.mvp.views.screens

import com.softdesign.goodshop.mvp.models.NavHeaderModel
import com.softdesign.goodshop.mvp.views.IBindingView
import com.softdesign.goodshop.ui.view.interfaces.IFragmentFactory
import com.softdesign.goodshop.ui.view.interfaces.INavigationManager

/**
 * @author Space
 * @date 11.12.2016
 */

interface IRootView : IBindingView<NavHeaderModel>, IFragmentFactory, INavigationManager {

    fun updateCartProductCounter(count: Int)

    fun setupToolbar(title: String = "", isDrawerLocked: Boolean = true)
}
