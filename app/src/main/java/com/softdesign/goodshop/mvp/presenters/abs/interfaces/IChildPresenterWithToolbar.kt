package com.softdesign.goodshop.mvp.presenters.abs.interfaces

import com.softdesign.goodshop.mvp.models.abs.BaseModel
import com.softdesign.goodshop.mvp.views.IRootChildWithToolbar

/**
 * @author elena
 * @date 19/12/16
 */

interface IChildPresenterWithToolbar<V : IRootChildWithToolbar<M>, M : BaseModel> : IRootChildPresenter<V, M> {

    fun setupToolbar(s: String = "", isDrawerLocked: Boolean = true) {
        rootPresenter.getView()?.setupToolbar(s, isDrawerLocked)
    }
}