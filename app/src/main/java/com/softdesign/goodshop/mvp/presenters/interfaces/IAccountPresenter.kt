package com.softdesign.goodshop.mvp.presenters.interfaces

import android.net.Uri
import android.view.View
import com.softdesign.goodshop.data.storage.dto.UserSettings
import com.softdesign.goodshop.mvp.models.AccountModel
import com.softdesign.goodshop.mvp.presenters.abs.interfaces.IChildPresenterWithToolbar
import com.softdesign.goodshop.mvp.views.screens.IAccountView

/**
 * @author Space
 * @date 05.01.2017
 */

interface IAccountPresenter : IChildPresenterWithToolbar<IAccountView, AccountModel> {

    fun updateNavView(settings: UserSettings)

    fun cancelEditMode(v: View): Boolean

    fun deleteAddress(addressId: Int)

    fun onChangePhotoClick()

    fun saveAvatar(imageUri: Uri)
}
