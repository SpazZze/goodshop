package com.softdesign.goodshop.mvp.models

import com.softdesign.goodshop.BR
import com.softdesign.goodshop.R
import com.softdesign.goodshop.common.databinding.abs.RecyclerBindingHelper
import com.softdesign.goodshop.data.storage.dto.Comment
import com.softdesign.goodshop.mvp.models.abs.RecyclerViewModel
import com.softdesign.goodshop.mvp.presenters.interfaces.ICommentsListPresenter
import com.softdesign.goodshop.mvp.views.screens.ICommentsListView
import com.softdesign.goodshop.utils.extentions.mapToWithReplace
import com.softdesign.goodshop.utils.extentions.reportingObserver
import com.softdesign.goodshop.utils.extentions.runOnIoObsOnMain
import rx.subscriptions.CompositeSubscription


/**
 * @author elena
 * @date 23/12/16
 */

class CommentsListModel(val productId: String,
                        subscription: CompositeSubscription,
                        defaultNoContentStringId: Int,
                        presenter: ICommentsListPresenter) :
        RecyclerViewModel<CommentItemModel, ICommentsListPresenter, ICommentsListView>(subscription, defaultNoContentStringId, presenter) {

    override val bindingHelper = RecyclerBindingHelper<CommentItemModel>(R.layout.item_comment, BR.viewModel)

    override fun loadItems() = subscription.add(commentProvider.getList(productId)
            .map { it.sortedWith(compareByDescending<Comment> { it.commentDate }.thenBy { it.userName }) }
            .map { it.mapToWithReplace(items, { CommentItemModel(it, presenter) }) }
            .withUiUpdate()
            .withProgress()
            .runOnIoObsOnMain()
            .subscribe(reportingObserver { handleNetworkException(it, false) }))

    override fun refreshItems() = subscription.add(commentProvider.getList(productId)
            .map { it.sortedWith(compareByDescending<Comment> { it.commentDate }.thenBy { it.userName }) }
            .map { it.mapToWithReplace(items, { CommentItemModel(it, presenter) }) }
            .withUiUpdate()
            .withRefresh()
            .runOnIoObsOnMain()
            .subscribe(reportingObserver { handleNetworkException(it, true) }))
}