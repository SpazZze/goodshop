package com.softdesign.goodshop.mvp.models

import com.softdesign.goodshop.common.databinding.interfaces.IRecyclerItemViewModel
import com.softdesign.goodshop.data.storage.dto.Comment
import com.softdesign.goodshop.mvp.presenters.interfaces.ICommentsListPresenter
import com.softdesign.goodshop.utils.extentions.execute
import org.ocpsoft.prettytime.PrettyTime

/**
 * @author Space
 * @date 18.12.2016
 */

class CommentItemModel(private val comment: Comment,
                       private val presenter: ICommentsListPresenter) : IRecyclerItemViewModel {

    override val id = comment.id
    val name = comment.userName
    val avatar = comment.avatar
    val rating = comment.rating
    val date = PrettyTime().format(comment.commentDate)!!
    val commentText = comment.comment

    fun onShowMoreClick() = execute { presenter.fragmentFactory?.showCommentDetailsScreen(comment) }

    override fun equals(other: Any?) = comment == (other as? CommentItemModel)?.comment

    override fun hashCode(): Int = comment.hashCode()
}