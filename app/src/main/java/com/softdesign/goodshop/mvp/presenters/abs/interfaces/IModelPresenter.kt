package com.softdesign.goodshop.mvp.presenters.abs.interfaces

import com.softdesign.goodshop.mvp.models.abs.BaseModel
import com.softdesign.goodshop.mvp.views.IModelView

/**
 * @author Space
 * @date 05.01.2017
 */

interface IModelPresenter<V : IModelView<M>, M : BaseModel> : IBasePresenter<V> {

    val model: M
}