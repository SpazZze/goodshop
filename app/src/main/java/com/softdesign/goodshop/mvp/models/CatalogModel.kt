package com.softdesign.goodshop.mvp.models

import android.databinding.ObservableInt
import com.softdesign.goodshop.common.databinding.abs.PagerPositionListener
import com.softdesign.goodshop.common.databinding.interfaces.IRecyclerBindingHelper
import com.softdesign.goodshop.data.storage.dto.Product
import com.softdesign.goodshop.mvp.models.abs.RecyclerViewModel
import com.softdesign.goodshop.mvp.presenters.interfaces.ICatalogPresenter
import com.softdesign.goodshop.mvp.views.screens.ICatalogView
import com.softdesign.goodshop.utils.extentions.replaceAllBy
import com.softdesign.goodshop.utils.extentions.reportingObserver
import com.softdesign.goodshop.utils.extentions.runOnIo
import com.softdesign.goodshop.utils.extentions.runOnIoObsOnMain
import rx.subscriptions.CompositeSubscription

/**
 * @author Space
 * @date 11.12.2016
 */

class CatalogModel(subscription: CompositeSubscription,
                   defaultNoContentStringId: Int,
                   presenter: ICatalogPresenter) :
        RecyclerViewModel<Product, ICatalogPresenter, ICatalogView>(subscription, defaultNoContentStringId, presenter) {

    override val bindingHelper: IRecyclerBindingHelper<Product> get() = TODO("ignore")

    val currentPage = ObservableInt(0)
    val pageChangeListener = PagerPositionListener(currentPage)

    init {
        loadItems()
    }

    override fun loadItems() = subscription.add(productProvider.getList()
            .map { items.replaceAllBy(it) }
            .withUiUpdate()
            .withProgress()
            .runOnIo()
            .subscribe(reportingObserver { handleNetworkException(it, false) }))

    override fun refreshItems() = subscription.add(productProvider.getList()
            .map { items.replaceAllBy(it) }
            .withUiUpdate()
            .withRefresh()
            .runOnIoObsOnMain()
            .subscribe(reportingObserver { handleNetworkException(it, true) }))
}
