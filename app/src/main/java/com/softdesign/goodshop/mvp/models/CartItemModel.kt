package com.softdesign.goodshop.mvp.models

import com.softdesign.goodshop.common.databinding.interfaces.IRecyclerItemViewModel
import com.softdesign.goodshop.data.storage.dto.CartItem
import com.softdesign.goodshop.mvp.presenters.interfaces.ICartPresenter
import com.softdesign.goodshop.utils.extentions.execute

/**
 * @author Space
 * @date 15.04.2017
 */

class CartItemModel(val cartItem: CartItem,
                    private val presenter: ICartPresenter) : IRecyclerItemViewModel {

    override val id = cartItem.product.id

    val cost = cartItem.product.price * cartItem.count

    fun onRemoveClick() = presenter.removeItemFromCart(id, cartItem.product.productName)

    fun showProductInfo() = execute { presenter.fragmentFactory?.showProductInfoScreen(cartItem.product) }

    override fun equals(other: Any?) = cartItem == (other as? CartItemModel)?.cartItem

    override fun hashCode(): Int = cartItem.hashCode()
}