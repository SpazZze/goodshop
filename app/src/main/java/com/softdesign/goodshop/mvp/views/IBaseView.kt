package com.softdesign.goodshop.mvp.views

import android.content.Context
import android.support.annotation.StringRes
import com.softdesign.goodshop.ui.view.interfaces.IActivityFactory

/**
 * @author Space
 * @date 11.12.2016
 */

interface IBaseView : IActivityFactory {

    override val ctx: Context

    fun showMessage(message: String)

    fun showMessage(@StringRes messageId: Int)

    fun showNotification(message: String)

    fun showNotification(@StringRes messageId: Int)

    fun showError(@StringRes messageId: Int, e: Throwable)

    fun showNetworkError(t: Throwable)

    fun showNetworkError(@StringRes messageId: Int, t: Throwable)

    fun showDialog(@StringRes messageId: Int, onPositive: () -> Unit, onCancel: () -> Unit = {})

    fun finish()

    fun handleBadIntent(key: String)
}
