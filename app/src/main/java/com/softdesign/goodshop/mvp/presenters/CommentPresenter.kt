package com.softdesign.goodshop.mvp.presenters

import com.softdesign.goodshop.data.storage.dto.Comment
import com.softdesign.goodshop.di.screens.CommentPresenterComponent
import com.softdesign.goodshop.di.screens.CommentPresenterModule
import com.softdesign.goodshop.mvp.models.CommentModel
import com.softdesign.goodshop.mvp.presenters.abs.ChildPresenterWithToolbar
import com.softdesign.goodshop.mvp.presenters.interfaces.ICommentPresenter
import com.softdesign.goodshop.mvp.views.screens.ICommentView

/**
 * @author Space
 * @date 05.01.2017
 */


class CommentPresenter(comment: Comment) : ChildPresenterWithToolbar<ICommentView, CommentModel>(), ICommentPresenter {

    init {
        injectRootChild(CommentPresenterComponent::class.java,
                arrayOf<Any>(CommentPresenterModule(subscription, comment, this))).inject(this)
    }
}