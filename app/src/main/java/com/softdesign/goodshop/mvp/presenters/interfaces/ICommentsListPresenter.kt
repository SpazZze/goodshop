package com.softdesign.goodshop.mvp.presenters.interfaces

import com.softdesign.goodshop.mvp.models.CommentsListModel
import com.softdesign.goodshop.mvp.presenters.abs.interfaces.IRootChildPresenter
import com.softdesign.goodshop.mvp.views.screens.ICommentsListView

/**
 * @author Space
 * @date 05.01.2017
 */

interface ICommentsListPresenter : IRootChildPresenter<ICommentsListView, CommentsListModel>