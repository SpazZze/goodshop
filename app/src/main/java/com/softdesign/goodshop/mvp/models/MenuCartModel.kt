package com.softdesign.goodshop.mvp.models

import android.databinding.ObservableArrayMap
import android.databinding.ObservableInt
import com.softdesign.goodshop.data.storage.dto.CartItem
import com.softdesign.goodshop.data.storage.dto.Product
import com.softdesign.goodshop.mvp.models.abs.BaseModel
import com.softdesign.goodshop.mvp.presenters.interfaces.IRootPresenter
import com.softdesign.goodshop.utils.extentions.*
import rx.lang.kotlin.addTo
import rx.subscriptions.CompositeSubscription

/**
 * @author Space
 * @date 10.12.2016
 */

class MenuCartModel(subscription: CompositeSubscription,
                    val presenter: IRootPresenter) : BaseModel(subscription) {

    val cartItems: ObservableArrayMap<String, CartItem> = ObservableArrayMap<String, CartItem>()
            .onChange { itemsCount.set(values.sumBy { it.count }) }

    val itemsCount = ObservableInt(0).onChange { presenter.getView()?.updateCartProductCounter(get()) }

    init {
        loadItems()
    }

    fun addItemToCart(product: Product, count: Int) {
        cartItems.put(product.id, CartItem(product, count.plus(cartItems[product.id]?.count ?: 0)))
        saveItems()
    }

    fun removeItemFromCart(cartItemId: String) = cartItems.remove(cartItemId) ?: Unit

    private fun loadItems() = cartItemProvider.getList()
            .map { cartItems.replaceAllBy(it, { it.product.id }) }
            .runOnIo()
            .subscribe(silentObserver {})
            .addTo(subscription)

    private fun saveItems() = cartItemProvider.saveListIntoDB(cartItems.values.toList())
            .runOnIo()
            .subscribe(silentObserver {})
            .addAsSingleInstanceTo(subscription)
}