package com.softdesign.goodshop.mvp.views

import com.softdesign.goodshop.mvp.models.abs.Model

/**
 * @author elena
 * @date 19/12/16
 */

interface IRootChildWithToolbar<M : Model> : IRootChildView<M> {

    fun setupToolbar()
}
