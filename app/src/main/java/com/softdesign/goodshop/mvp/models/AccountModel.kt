package com.softdesign.goodshop.mvp.models

import android.databinding.ObservableArrayList
import android.databinding.ObservableBoolean
import android.databinding.ObservableField
import android.net.Uri
import android.util.Patterns
import android.view.View
import com.softdesign.goodshop.BR
import com.softdesign.goodshop.R
import com.softdesign.goodshop.common.databinding.abs.RecyclerBindingHelper
import com.softdesign.goodshop.common.databinding.fields.CheckableField
import com.softdesign.goodshop.common.databinding.fields.RussianPhoneField
import com.softdesign.goodshop.common.databinding.fields.RussianPhoneField.Companion.reformatPhone
import com.softdesign.goodshop.data.managers.FileManager
import com.softdesign.goodshop.data.storage.dto.UserSettings
import com.softdesign.goodshop.mvp.models.abs.BaseModel
import com.softdesign.goodshop.mvp.presenters.interfaces.IAccountPresenter
import com.softdesign.goodshop.utils.Config.XXXHDPI_MULTIPLIER
import com.softdesign.goodshop.utils.extentions.*
import rx.Single
import rx.subscriptions.CompositeSubscription
import java.io.IOException

/**
 * @author Space
 * @date 11.12.2016
 */
@Suppress("DEPRECATION")
class AccountModel(subscription: CompositeSubscription,
                   val presenter: IAccountPresenter) : BaseModel(subscription) {

    val bindingHelper = RecyclerBindingHelper<AddressItemModel>(R.layout.item_address, BR.viewModel)

    lateinit var savedCorrectSettings: UserSettings

    val items = ObservableArrayList<AddressItemModel>()

    val emailError = ObservableField<String>("")
    val phoneError = ObservableField<String>("")

    val fullName: ObservableField<String> = ObservableField("")
    val avatarUrl: ObservableField<String> = ObservableField("")
    val phone = RussianPhoneField(context, "", phoneError)
    val email = CheckableField(context, "", emailError, R.string.error_wrong_email) { Patterns.EMAIL_ADDRESS.matcher(it).matches() }
    val isStatusNotifyEnabled = ObservableBoolean(true)
    val isSpecialNotifyEnabled = ObservableBoolean(true)

    val isEditMode = ObservableBoolean(false)
    val fabIconRes = ObservableField(chooseFabRes)

    val chooseFabRes: Int
        get() = if (isEditMode.get()) R.drawable.ic_account_confirm else R.drawable.ic_account_edit

    private val currentSettings: UserSettings
        get() = UserSettings(fullName.get()!!, avatarUrl.get()!!, phone.get(), email.get(),
                isStatusNotifyEnabled.get(), isSpecialNotifyEnabled.get())

    init {
        loadSettings()
    }

    //region :::::::::::::::::::::::::::::::::: onClick
    fun onChangePhotoClick() = presenter.onChangePhotoClick()

    fun onAddAddressClick() = execute { presenter.fragmentFactory?.showAddAddressScreen() }

    fun deleteAddress(addressId: Int) = subscription.add(addressProvider.removeFromDB(addressId)
            .map { items.remove(items.find { it.id == addressId.toString() }) }
            .withProgress()
            .runOnIoObsOnMain()
            .subscribe(silentObserver {}))

    fun onFabClick(v: View) {
        if (isEditMode.get()) saveChanges()
        isEditMode.invert()
        v.animateViewChanges(R.anim.disappearance, R.anim.appearance) { fabIconRes.set(chooseFabRes) }
    }

    fun onSwitchClick() = userSettingsProvider.saveIntoDB(savedCorrectSettings.copy(
            isStatusNotifyEnabled = this.isStatusNotifyEnabled.get(),
            isSpecialNotifyEnabled = this.isSpecialNotifyEnabled.get()))
            .map { savedCorrectSettings = it }
            .withProgress()
            .runOnIo()
            .subscribe(silentObserver {})
            .addAsSingleInstanceTo(subscription)

    fun cancelEditMode(v: View): Boolean = runIf(isEditMode.get()) {
        if (currentSettings != savedCorrectSettings) {
            rollbackChangeableFields()
            presenter.getView()?.showNotification(R.string.notify_changes_cancelled)
        }
        isEditMode.invert()
        v.animateViewChanges(R.anim.disappearance, R.anim.appearance) { fabIconRes.set(chooseFabRes) }
    }
    //endregion :::::::::::::::::::::::::::::::::: onClick

    //region :::::::::::::::::::::::::::::::::: Data Load/Save
    internal fun loadAddresses() = subscription.add(addressProvider.getListFromDB()
            .map { it.sortedBy { it.id }.mapToWithReplace(items, { AddressItemModel(it, presenter) }) }
            .withProgress()
            .runOnIo()
            .subscribe(silentObserver {}))

    private fun loadSettings() = subscription.add(userSettingsProvider.getFromDB()
            .flatMap { if (it.isDefined()) Single.just(it.get()) else Single.error(IOException("AccountModel: no settings in DB")) }
            .map { updateFields(it) }
            .withProgress()
            .runOnIo()
            .subscribe(reportingObserver {
                presenter.getView()?.run {
                    showNotification(R.string.notify_authorization_required)
                    showAuthScreen()
                    finish()
                }
            }))

    private fun saveSettings(settings: UserSettings) = userSettingsProvider.saveIntoDB(settings)
            .map { updateFields(it) }
            .withProgress()
            .runOnIo()
            .subscribe(silentObserver {})
            .addAsSingleInstanceTo(subscription)

    fun saveAvatar(uri: Uri) = try {
        val avatarMaxSize by lazy { context.getDimensionValue(R.dimen.account_avatar_size) * XXXHDPI_MULTIPLIER }
        val selectedImage = context.contentResolver.getResizedBitmap(uri, avatarMaxSize)
        FileManager.cacheAvatarImage(selectedImage)?.apply { avatarUrl.set(toURI().toString()) }
                ?: presenter.getView()?.showMessage(R.string.error_cache_file_not_created)
    } catch (e: Exception) {
        presenter.getView()?.showError(R.string.error_file_not_found, e)
    }
    //endregion :::::::::::::::::::::::::::::::::: DataLoading

    //region :::::::::::::::::::::::::::::::::: Internal
    private fun rollbackChangeableFields() {
        avatarUrl.set(savedCorrectSettings.avatarUrl)
        rollbackValidableFields()
    }

    private fun rollbackValidableFields() {
        phone.set(savedCorrectSettings.phone)
        email.set(savedCorrectSettings.email)
    }

    private fun updateFields(settings: UserSettings) {
        fullName.set(settings.fullName)
        avatarUrl.set(settings.avatarUrl)
        phone.set(reformatPhone(settings.phone))
        email.set(settings.email)
        isSpecialNotifyEnabled.set(settings.isSpecialNotifyEnabled)
        isStatusNotifyEnabled.set(settings.isStatusNotifyEnabled)
        savedCorrectSettings = settings.copy(phone = reformatPhone(settings.phone))
        presenter.updateNavView(settings)
    }

    private fun composeValidSettings(): UserSettings = savedCorrectSettings.copy(
            avatarUrl = this.avatarUrl.get()!!,
            phone = if (this.phone.isValid) this.phone.applyFormatting() else savedCorrectSettings.phone,
            email = if (this.email.isValid) this.email.get() else savedCorrectSettings.email,
            isStatusNotifyEnabled = this.isStatusNotifyEnabled.get(),
            isSpecialNotifyEnabled = this.isSpecialNotifyEnabled.get())

    private fun saveChanges() = with(composeValidSettings()) {
        when (savedCorrectSettings != this) {
            true -> saveSettings(this)
            else -> rollbackValidableFields()
        }
    }
    //endregion :::::::::::::::::::::::::::::::::: Internal
}
