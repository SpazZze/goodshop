package com.softdesign.goodshop.mvp.views.screens

import com.softdesign.goodshop.data.storage.dto.Product
import com.softdesign.goodshop.mvp.models.FavouritesModel
import com.softdesign.goodshop.mvp.views.IRootChildWithToolbar

/**
 * @author Space
 * @date 30.03.2017
 */

interface IFavouritesView : IRootChildWithToolbar<FavouritesModel> {

    fun showAddedToCartMessage(productName: String)

    fun showRemovedFromFavouritesMessage(product: Product)
}