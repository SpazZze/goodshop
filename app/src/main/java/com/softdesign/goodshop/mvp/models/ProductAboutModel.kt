package com.softdesign.goodshop.mvp.models

import com.softdesign.goodshop.data.storage.dto.Product
import com.softdesign.goodshop.mvp.models.abs.BaseModel
import rx.subscriptions.CompositeSubscription

/**
 * @author Space
 * @date 01.02.2017
 */

class ProductAboutModel(subscription: CompositeSubscription,
                        product: Product) : BaseModel(subscription) {

    val imageUrl = product.imageUrl
    val description = product.description
    val rating = product.rating
}
