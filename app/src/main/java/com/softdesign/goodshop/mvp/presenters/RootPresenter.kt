package com.softdesign.goodshop.mvp.presenters

import com.softdesign.goodshop.R
import com.softdesign.goodshop.data.storage.dto.Product
import com.softdesign.goodshop.data.storage.dto.UserSettings
import com.softdesign.goodshop.di.DaggerService
import com.softdesign.goodshop.di.screens.RootPresenterComponent
import com.softdesign.goodshop.di.screens.RootPresenterModule
import com.softdesign.goodshop.mvp.models.MenuCartModel
import com.softdesign.goodshop.mvp.models.NavHeaderModel
import com.softdesign.goodshop.mvp.presenters.abs.BindingPresenter
import com.softdesign.goodshop.mvp.presenters.interfaces.IRootPresenter
import com.softdesign.goodshop.mvp.views.screens.IRootView
import javax.inject.Inject

/**
 * @author Space
 * @date 10.12.2016
 */


class RootPresenter : BindingPresenter<IRootView, NavHeaderModel>(), IRootPresenter {

    @Inject
    lateinit var menuCartModel: MenuCartModel

    init {
        DaggerService.getComponent(RootPresenterComponent::class.java,
                arrayOf<Any>(RootPresenterModule(subscription, this))).inject(this)
    }

    override fun initView() {
        super<BindingPresenter>.initView()
        with(checkUserAuth()) { model.update(this); updateNavView(this) }
    }

    override fun getCartItemsCount(): Int = menuCartModel.itemsCount.get()

    override fun addItemToCart(product: Product, count: Int) = menuCartModel.addItemToCart(product, count)

    override fun removeItemFromCart(itemId: String) = menuCartModel.removeItemFromCart(itemId)

    override fun checkUserAuth() = model.isUserAuthenticated

    override fun logout() = model.logout()

    override fun updateFields(settings: UserSettings) = model.updateFields(settings)

    private fun updateNavView(authenticated: Boolean) {
        getView()?.run {
            changeNavItemTitle(R.id.nav_account, if (authenticated) R.string.header_personal_cabinet else R.string.header_enter)
            changeNavItemVisibility(R.id.nav_logout, authenticated)
        }
    }

}
