package com.softdesign.goodshop.mvp.views.screens

import com.softdesign.goodshop.mvp.models.ProductAboutModel
import com.softdesign.goodshop.mvp.views.IRootChildView

/**
 * @author Space
 * @date 05.01.2017
 */

interface IProductAboutView : IRootChildView<ProductAboutModel>