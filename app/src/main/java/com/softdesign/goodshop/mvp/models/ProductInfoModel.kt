package com.softdesign.goodshop.mvp.models

import android.databinding.ObservableBoolean
import android.databinding.ObservableField
import android.databinding.ObservableInt
import android.view.View
import com.softdesign.goodshop.R
import com.softdesign.goodshop.common.databinding.abs.PagerPositionListener
import com.softdesign.goodshop.data.storage.dto.Product
import com.softdesign.goodshop.mvp.models.abs.BaseModel
import com.softdesign.goodshop.mvp.presenters.interfaces.IProductInfoPresenter
import com.softdesign.goodshop.utils.extentions.*
import rx.subscriptions.CompositeSubscription

/**
 * @author Space
 * @date 18.12.2016
 */

@Suppress("DEPRECATION")
class ProductInfoModel(subscription: CompositeSubscription,
                       private val product: Product,
                       private val presenter: IProductInfoPresenter) : BaseModel(subscription) {

    val isFavourite = ObservableBoolean(product.isFavourite).onChange { saveProductIntoDB() }
    val currentPage = ObservableInt(0).onChange { changeFabIcon(get()) }
    val pageChangeListener = PagerPositionListener(currentPage)
    val fabIconRes = ObservableField(getLikeImage())

    val thisProduct: Product
        get() = product.copy(isFavourite = this.isFavourite.get())

    fun onFabClick(v: View) {
        when (currentPage.get()) {
            0 -> onLikeButtonClick(v)
            else -> presenter.fragmentFactory?.showCommentAddScreen(product.id)
        }
    }

    private fun onLikeButtonClick(v: View) {
        v.animateView(R.anim.heartbeat)
        isFavourite.invert()
        fabIconRes.set(getLikeImage())
    }

    private fun saveProductIntoDB() = productProvider.saveIntoDB(thisProduct)
            .runOnIo()
            .subscribe(silentObserver {})
            .addAsSingleInstanceTo(subscription)

    private fun changeFabIcon(position: Int) = when (position) {
        0 -> fabIconRes.set(getLikeImage())
        else -> fabIconRes.set(R.drawable.ic_plus_white)
    }

    fun getLikeImage(): Int = when (isFavourite.get()) {
        true -> R.drawable.ic_heart_white
        else -> R.drawable.ic_heart_outline_white
    }
}