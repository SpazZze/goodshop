package com.softdesign.goodshop.mvp.presenters

import android.net.Uri
import android.view.View
import com.softdesign.goodshop.data.storage.dto.UserSettings
import com.softdesign.goodshop.di.screens.AccountPresenterComponent
import com.softdesign.goodshop.di.screens.AccountPresenterModule
import com.softdesign.goodshop.mvp.models.AccountModel
import com.softdesign.goodshop.mvp.presenters.abs.ChildPresenterWithToolbar
import com.softdesign.goodshop.mvp.presenters.interfaces.IAccountPresenter
import com.softdesign.goodshop.mvp.views.screens.IAccountView
import com.softdesign.goodshop.utils.extentions.execute

/**
 * @author Space
 * @date 11.12.2016
 */

class AccountPresenter : ChildPresenterWithToolbar<IAccountView, AccountModel>(), IAccountPresenter {

    init {
        injectRootChild(AccountPresenterComponent::class.java,
                arrayOf<Any>(AccountPresenterModule(subscription, this))).inject(this)
    }

    override fun initView() {
        super<ChildPresenterWithToolbar>.initView()
        model.loadAddresses()
    }

    override fun updateNavView(settings: UserSettings) = rootPresenter.updateFields(settings)

    override fun cancelEditMode(v: View) = model.cancelEditMode(v)

    override fun deleteAddress(addressId: Int) = model.deleteAddress(addressId)

    override fun onChangePhotoClick() = execute { getView()?.showChangePhotoDialog() }

    override fun saveAvatar(imageUri: Uri) = execute { model.saveAvatar(imageUri) }
}
