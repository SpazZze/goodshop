package com.softdesign.goodshop.mvp.views.screens

import com.softdesign.goodshop.mvp.models.CommentsListModel
import com.softdesign.goodshop.mvp.views.IRootChildView

/**
 * @author elena
 * @date 23/12/16
 */

interface ICommentsListView : IRootChildView<CommentsListModel>