package com.softdesign.goodshop.mvp.views

import com.softdesign.goodshop.mvp.models.abs.Model

/**
 * @author Space
 * @date 05.01.2017
 */

interface IModelView<M : Model> : IBaseView