package com.softdesign.goodshop.mvp.views.screens

import com.softdesign.goodshop.mvp.models.ProductModel
import com.softdesign.goodshop.mvp.views.IRootChildView

/**
 * @author Space
 * @date 05.01.2017
 */

interface IProductView : IRootChildView<ProductModel>