package com.softdesign.goodshop.mvp.presenters

import com.softdesign.goodshop.di.screens.CartPresenterComponent
import com.softdesign.goodshop.di.screens.CartPresenterModule
import com.softdesign.goodshop.mvp.models.CartModel
import com.softdesign.goodshop.mvp.presenters.abs.ChildPresenterWithToolbar
import com.softdesign.goodshop.mvp.presenters.interfaces.ICartPresenter
import com.softdesign.goodshop.mvp.views.screens.ICartView

/**
 * @author Space
 * @date 16.04.2017
 */

class CartPresenter : ChildPresenterWithToolbar<ICartView, CartModel>(), ICartPresenter {

    init {
        injectRootChild(CartPresenterComponent::class.java,
                arrayOf<Any>(CartPresenterModule(subscription, this))).inject(this)
    }

    override fun removeItemFromCart(cartItemId: String, itemName: String) {
        model.removeItemFromCart(cartItemId, itemName)
        rootPresenter.removeItemFromCart(cartItemId)
    }
}