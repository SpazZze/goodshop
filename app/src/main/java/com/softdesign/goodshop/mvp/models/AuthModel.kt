package com.softdesign.goodshop.mvp.models

import android.databinding.ObservableBoolean
import android.databinding.ObservableField
import android.util.Patterns
import android.view.View
import com.softdesign.goodshop.R
import com.softdesign.goodshop.common.databinding.fields.CheckableField
import com.softdesign.goodshop.data.network.api.req.AuthReq
import com.softdesign.goodshop.mvp.models.abs.BaseModel
import com.softdesign.goodshop.mvp.presenters.interfaces.IAuthPresenter
import com.softdesign.goodshop.utils.extentions.*
import rx.subscriptions.CompositeSubscription
import java.util.concurrent.TimeUnit

/**
 * @author Space
 * @date 11.12.2016
 */

class AuthModel(subscription: CompositeSubscription,
                private val presenter: IAuthPresenter) : BaseModel(subscription) {

    val isLoginBtnClicked = ObservableBoolean(false)

    val emailError = ObservableField<String>("")
    val passwordError = ObservableField<String>("")

    val email = CheckableField(context, "123@123.ru", emailError, R.string.error_wrong_email,
            { it.isNotNullOrBlank() && Patterns.EMAIL_ADDRESS.matcher(it).matches() })

    val password = CheckableField(context, "12345678", passwordError, R.string.error_short_password,
            { it.trim().length >= 8 })

    fun loginUser() = subscription.add(userSettingsProvider.authUser(AuthReq(email.get(), password.get()))
            .map { isLoginBtnClicked.set(false); password.set("") }
            .delay(1000, TimeUnit.MILLISECONDS)
            .withProgress()
            .runOnIoObsOnMain()
            .subscribe(reportingObserver({ onShowCatalogClick() }, { presenter.getView()?.showNetworkError(it) })))

    fun onLoginClick() {
        when (isLoginBtnClicked.get()) {
            true -> if (email.validateField() && password.validateField()) loginUser()
            else -> isLoginBtnClicked.set(!isLoginBtnClicked.get())
        }
    }

    fun onShowCatalogClick() = execute { presenter.getView()?.showRootScreen() }

    fun onVkClick(v: View) = v.animateView(R.anim.milkshake)

    fun onFbClick(v: View) = v.animateView(R.anim.milkshake)

    fun onTwitterClick(v: View) = v.animateView(R.anim.milkshake)
}
