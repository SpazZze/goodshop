package com.softdesign.goodshop.mvp.presenters

import com.softdesign.goodshop.R
import com.softdesign.goodshop.data.storage.dto.Product
import com.softdesign.goodshop.di.screens.CatalogPresenterComponent
import com.softdesign.goodshop.di.screens.CatalogPresenterModule
import com.softdesign.goodshop.mvp.models.CatalogModel
import com.softdesign.goodshop.mvp.presenters.abs.ChildPresenterWithToolbar
import com.softdesign.goodshop.mvp.presenters.interfaces.ICatalogPresenter
import com.softdesign.goodshop.mvp.views.screens.ICatalogView

/**
 * @author Space
 * @date 11.12.2016
 */

class CatalogPresenter : ChildPresenterWithToolbar<ICatalogView, CatalogModel>(), ICatalogPresenter {

    init {
        injectRootChild(CatalogPresenterComponent::class.java,
                arrayOf<Any>(CatalogPresenterModule(subscription, R.string.no_content_product_list, this))).inject(this)
    }

    override fun initView() {
        super<ChildPresenterWithToolbar>.initView()
        getView()?.setupViewPager()
    }

    override fun addItemToCart(product: Product, count: Int) = rootPresenter.addItemToCart(product, count)
}
