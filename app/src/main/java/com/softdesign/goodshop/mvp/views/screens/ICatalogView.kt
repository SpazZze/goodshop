package com.softdesign.goodshop.mvp.views.screens

import com.softdesign.goodshop.mvp.models.CatalogModel
import com.softdesign.goodshop.mvp.views.IRootChildWithToolbar

/**
 * @author Space
 * @date 11.12.2016
 */


interface ICatalogView : IRootChildWithToolbar<CatalogModel> {

    fun showAddedToCartMessage(productName: String)

    fun setupViewPager()

    fun onClickAtBuyButton()
}
