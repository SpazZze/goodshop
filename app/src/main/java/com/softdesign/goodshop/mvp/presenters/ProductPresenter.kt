package com.softdesign.goodshop.mvp.presenters

import com.softdesign.goodshop.data.storage.dto.Product
import com.softdesign.goodshop.di.screens.ProductPresenterComponent
import com.softdesign.goodshop.di.screens.ProductPresenterModule
import com.softdesign.goodshop.mvp.models.ProductModel
import com.softdesign.goodshop.mvp.presenters.abs.RootChildPresenter
import com.softdesign.goodshop.mvp.presenters.interfaces.IProductPresenter
import com.softdesign.goodshop.mvp.views.screens.IProductView

/**
 * @author Space
 * @date 11.12.2016
 */


class ProductPresenter(override val product: Product) : RootChildPresenter<IProductView, ProductModel>(), IProductPresenter {

    override val itemsCount: Int get() = model.count.get()

    init {
        injectRootChildProduct(ProductPresenterComponent::class.java, product,
                arrayOf<Any>(ProductPresenterModule(product, subscription, this))).inject(this)
    }

    override fun update() = model.update()
}
