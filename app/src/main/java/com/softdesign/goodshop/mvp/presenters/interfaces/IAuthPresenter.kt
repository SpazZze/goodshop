package com.softdesign.goodshop.mvp.presenters.interfaces

import com.softdesign.goodshop.mvp.models.AuthModel
import com.softdesign.goodshop.mvp.presenters.abs.interfaces.IBindingPresenter
import com.softdesign.goodshop.mvp.views.screens.IAuthView

/**
 * @author Space
 * @date 11.12.2016
 */

interface IAuthPresenter : IBindingPresenter<IAuthView, AuthModel> {

    fun hideLoginCard(): Boolean
}
