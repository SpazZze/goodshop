package com.softdesign.goodshop.mvp.presenters.interfaces

import com.softdesign.goodshop.mvp.models.ProductAboutModel
import com.softdesign.goodshop.mvp.presenters.abs.interfaces.IRootChildPresenter
import com.softdesign.goodshop.mvp.views.screens.IProductAboutView

/**
 * @author Space
 * @date 05.01.2017
 */

interface IProductAboutPresenter : IRootChildPresenter<IProductAboutView, ProductAboutModel>