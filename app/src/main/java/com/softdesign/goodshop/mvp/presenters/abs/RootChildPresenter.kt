package com.softdesign.goodshop.mvp.presenters.abs

import android.support.annotation.CallSuper
import com.softdesign.goodshop.mvp.models.abs.BaseModel
import com.softdesign.goodshop.mvp.presenters.abs.interfaces.IRootChildPresenter
import com.softdesign.goodshop.mvp.presenters.interfaces.IRootPresenter
import com.softdesign.goodshop.mvp.views.IRootChildView
import com.softdesign.goodshop.ui.view.interfaces.IFragmentFactory
import javax.inject.Inject

/**
 * @author Space
 * @date 31.01.2017
 */


abstract class RootChildPresenter<V : IRootChildView<M>, M : BaseModel> : BindingPresenter<V, M>(),
        IRootChildPresenter<V, M> {

    @Inject
    lateinit override var rootPresenter: IRootPresenter

    override val fragmentFactory: IFragmentFactory? get() = rootPresenter.getView()

    @CallSuper
    override fun initView() {
        super<BindingPresenter>.initView()
    }
}