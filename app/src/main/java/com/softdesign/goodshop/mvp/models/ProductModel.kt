package com.softdesign.goodshop.mvp.models

import android.databinding.ObservableBoolean
import android.databinding.ObservableInt
import android.view.View
import com.softdesign.goodshop.R
import com.softdesign.goodshop.data.storage.dto.Product
import com.softdesign.goodshop.mvp.models.abs.BaseModel
import com.softdesign.goodshop.mvp.presenters.interfaces.IProductPresenter
import com.softdesign.goodshop.utils.extentions.*
import rx.subscriptions.CompositeSubscription

/**
 * @author Space
 * @date 10.12.2016
 */


class ProductModel(subscription: CompositeSubscription,
                   val product: Product,
                   private val presenter: IProductPresenter) : BaseModel(subscription) {

    val count = ObservableInt(product.count).onChange { saveProductIntoDB() }
    val isFavourite = ObservableBoolean(product.isFavourite).onChange { saveProductIntoDB() }

    val fullPrice: String
        get() = "${product.price * count.get()}.-"

    private val thisProduct: Product
        get() = product.copy(isFavourite = this.isFavourite.get(), count = this.count.get())

    fun onMinusClick() {
        if (count.get() > 1) count.dec()
    }

    fun onPlusClick() {
        count.inc()
    }

    fun onLikeClick(v: View) {
        v.animateView(R.anim.heartbeat)
        isFavourite.invert()
    }

    fun onShowMoreClick() {
        presenter.fragmentFactory?.showProductInfoScreen(thisProduct)
    }

    fun update() = subscription.add(productProvider.get(product.id)
            .map { it.map { isFavourite.set(it.isFavourite); count.set(it.count) } }
            .runOnIo()
            .subscribe(silentObserver {}))

    private fun saveProductIntoDB() = subscription.add(productProvider.saveIntoDB(thisProduct)
            .runOnIo()
            .subscribe(silentObserver {}))
}
