package com.softdesign.goodshop.mvp.models.abs

import android.databinding.ObservableArrayList
import android.databinding.ObservableBoolean
import android.databinding.ObservableField
import android.support.annotation.StringRes
import com.softdesign.goodshop.common.databinding.interfaces.IRecyclerItemViewModel
import com.softdesign.goodshop.common.databinding.interfaces.IRecyclerViewModel
import com.softdesign.goodshop.mvp.presenters.abs.interfaces.IBasePresenter
import com.softdesign.goodshop.mvp.views.IBaseView
import com.softdesign.goodshop.utils.errors.NotAuthenticatedException
import com.softdesign.goodshop.utils.extentions.networkErrorMsg
import rx.Observable
import rx.Single
import rx.subscriptions.CompositeSubscription

/**
 * @author Space
 * @date 18.02.2017
 */

abstract class RecyclerViewModel<I : IRecyclerItemViewModel, out P : IBasePresenter<V>, V : IBaseView>(
        subscription: CompositeSubscription,
        @StringRes val defaultNoContentStringId: Int,
        val presenter: P) : BaseModel(subscription), IRecyclerViewModel<I> {

    val defaultNoContentText = context.getString(defaultNoContentStringId)!!

    override val items = ObservableArrayList<I>()
    val noContentTextVisibility = ObservableBoolean(false)
    val isRefreshing = ObservableBoolean(false)
    val noContentText = ObservableField(context.getString(defaultNoContentStringId))

    abstract fun loadItems()

    abstract fun refreshItems()

    fun handleNetworkException(t: Throwable, isAlertNeeded: Boolean) = with(t.networkErrorMsg()) {
        if (this.isNullOrBlank()) return
        if (isAlertNeeded) presenter.getView()?.showMessage(this)
        if (items.isEmpty() && t !is NotAuthenticatedException) noContentText.set(this)
    }

    fun <T> Observable<T>.withUiUpdate(): Observable<T> = this
            .doOnCompleted { noContentText.set(defaultNoContentText) }
            .doOnTerminate { updateUI() }

    fun <T> Observable<T>.withRefresh(): Observable<T> = this
            .doOnSubscribe { isRefreshing.set(true) }
            .doAfterTerminate { isRefreshing.set(false) }

    fun <T> Single<T>.withUiUpdate(): Single<T> = this
            .doOnSuccess { noContentText.set(defaultNoContentText) }
            .doAfterTerminate { updateUI() }

    fun <T> Single<T>.withRefresh(): Single<T> = this
            .doOnSubscribe { isRefreshing.set(true) }
            .doAfterTerminate { isRefreshing.set(false) }

    private fun updateUI() = noContentTextVisibility.set(items.isEmpty())
}