package com.softdesign.goodshop.mvp.presenters

import com.softdesign.goodshop.di.screens.CommentAddPresenterComponent
import com.softdesign.goodshop.di.screens.CommentAddPresenterModule
import com.softdesign.goodshop.mvp.models.CommentAddModel
import com.softdesign.goodshop.mvp.presenters.abs.ChildPresenterWithToolbar
import com.softdesign.goodshop.mvp.presenters.interfaces.ICommentAddPresenter
import com.softdesign.goodshop.mvp.views.screens.ICommentAddView

/**
 * @author Space
 * @date 05.01.2017
 */


class CommentAddPresenter(productId: String) : ChildPresenterWithToolbar<ICommentAddView, CommentAddModel>(), ICommentAddPresenter {

    init {
        injectRootChild(CommentAddPresenterComponent::class.java,
                arrayOf<Any>(CommentAddPresenterModule(subscription, productId, this))).inject(this)
    }
}