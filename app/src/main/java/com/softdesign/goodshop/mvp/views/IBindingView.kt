package com.softdesign.goodshop.mvp.views

import com.softdesign.goodshop.mvp.models.abs.Model

/**
 * @author elena
 * @date 19/12/16
 */

interface IBindingView<M : Model> : IModelView<M> {

    fun setViewModel(model: M)
}
