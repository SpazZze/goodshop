package com.softdesign.goodshop.mvp.presenters.abs

import android.support.annotation.CallSuper
import com.softdesign.goodshop.mvp.presenters.abs.interfaces.IBasePresenter
import com.softdesign.goodshop.mvp.views.IBaseView

/**
 * @author Space
 * @date 11.12.2016
 */


abstract class BasePresenter<V : IBaseView> : IBasePresenter<V> {

    private var view: V? = null

    @CallSuper
    override fun dropView() {
        view = null
    }

    @CallSuper
    override fun takeView(view: V) {
        this.view = view
    }

    @CallSuper
    override fun getView(): V? {
        return view
    }

    @CallSuper
    override fun finishView() {
        view?.finish()
        view = null
    }

    abstract override fun initView()
}
