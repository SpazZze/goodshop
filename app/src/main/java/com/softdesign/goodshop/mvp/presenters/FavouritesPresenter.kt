package com.softdesign.goodshop.mvp.presenters

import com.softdesign.goodshop.R
import com.softdesign.goodshop.data.storage.dto.Product
import com.softdesign.goodshop.di.screens.FavouritesPresenterComponent
import com.softdesign.goodshop.di.screens.FavouritesPresenterModule
import com.softdesign.goodshop.mvp.models.FavouritesModel
import com.softdesign.goodshop.mvp.presenters.abs.ChildPresenterWithToolbar
import com.softdesign.goodshop.mvp.presenters.interfaces.IFavouritesPresenter
import com.softdesign.goodshop.mvp.views.screens.IFavouritesView

/**
 * @author Space
 * @date 03.04.2017
 */

class FavouritesPresenter :
        ChildPresenterWithToolbar<IFavouritesView, FavouritesModel>(), IFavouritesPresenter {

    init {
        injectRootChild(FavouritesPresenterComponent::class.java,
                arrayOf<Any>(FavouritesPresenterModule(subscription, R.string.no_content_favourites_list, this))).inject(this)
    }

    override fun removeFromFavourites(product: Product) = model.removeItem(product)

    override fun addItemToCart(product: Product) {
        getView()?.showAddedToCartMessage(product.productName)
        rootPresenter.addItemToCart(product, 1)
    }
}