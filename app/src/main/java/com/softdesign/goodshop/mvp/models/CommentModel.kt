package com.softdesign.goodshop.mvp.models

import android.databinding.ObservableField
import android.databinding.ObservableInt
import com.softdesign.goodshop.data.storage.dto.Comment
import com.softdesign.goodshop.mvp.models.abs.BaseModel
import com.softdesign.goodshop.mvp.presenters.interfaces.ICommentPresenter
import org.ocpsoft.prettytime.PrettyTime
import rx.subscriptions.CompositeSubscription

/**
 * @author Space
 * @date 05.01.2017
 */

class CommentModel(subscription: CompositeSubscription,
                   comment: Comment,
                   private val presenter: ICommentPresenter) : BaseModel(subscription) {

    val name = ObservableField(comment.userName)
    val avatar = ObservableField(comment.avatar)
    val rating = ObservableInt(comment.rating)
    val date = ObservableField(PrettyTime().format(comment.commentDate))
    val commentText = ObservableField(comment.comment)
}