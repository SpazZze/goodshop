package com.softdesign.goodshop.mvp.models

import com.softdesign.goodshop.R
import com.softdesign.goodshop.common.databinding.interfaces.IRecyclerItemViewModel
import com.softdesign.goodshop.data.storage.dto.Address
import com.softdesign.goodshop.mvp.models.abs.Model
import com.softdesign.goodshop.mvp.presenters.interfaces.IAccountPresenter
import com.softdesign.goodshop.utils.extentions.execute

/**
 * @author Space
 * @date 12.12.2016
 */

class AddressItemModel(val address: Address,
                       val presenter: IAccountPresenter) : Model(), IRecyclerItemViewModel {

    override val id = address.id.toString()
    val fullAddress = composeAddressString(address)
    val hint = address.hint
    val comment = address.comment

    fun showEditAddressScreen() = execute { presenter.fragmentFactory?.showEditAddressScreen(address) }

    fun deleteAddress() = execute { presenter.deleteAddress(address.id) }

    private fun composeAddressString(address: Address): String {
        val sb = StringBuilder(address.street).append(" ").append(address.house)
        if (!address.flat.isNullOrBlank()) sb.append("-").append(address.flat)
        if (!address.floor.isNullOrBlank()) sb.append(", ").append(address.floor).append(" ").append(context.getString(R.string.floor))
        return sb.toString().trim()
    }

    override fun equals(other: Any?) = address == (other as? AddressItemModel)?.address

    override fun hashCode(): Int = address.hashCode()
}