package com.softdesign.goodshop.mvp.views.screens

import com.softdesign.goodshop.mvp.models.SplashModel
import com.softdesign.goodshop.mvp.views.IModelView

/**
 * @author Space
 * @date 21.12.2016
 */

interface ISplashView : IModelView<SplashModel>