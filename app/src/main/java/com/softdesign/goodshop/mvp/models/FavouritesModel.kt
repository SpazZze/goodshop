package com.softdesign.goodshop.mvp.models

import com.softdesign.goodshop.BR
import com.softdesign.goodshop.R
import com.softdesign.goodshop.common.databinding.abs.RecyclerBindingHelper
import com.softdesign.goodshop.data.storage.dto.Product
import com.softdesign.goodshop.mvp.models.abs.RecyclerViewModel
import com.softdesign.goodshop.mvp.presenters.interfaces.IFavouritesPresenter
import com.softdesign.goodshop.mvp.views.screens.IFavouritesView
import com.softdesign.goodshop.utils.extentions.mapToWithReplace
import com.softdesign.goodshop.utils.extentions.reportingObserver
import com.softdesign.goodshop.utils.extentions.runOnIoObsOnMain
import rx.subscriptions.CompositeSubscription

/**
 * @author Space
 * @date 30.03.2017
 */

class FavouritesModel(subscription: CompositeSubscription,
                      defaultNoContentStringId: Int,
                      presenter: IFavouritesPresenter) :
        RecyclerViewModel<FavouritesItemModel, IFavouritesPresenter, IFavouritesView>(subscription, defaultNoContentStringId, presenter) {

    override val bindingHelper = RecyclerBindingHelper<FavouritesItemModel>(R.layout.item_favourites, BR.viewModel)

    init {
        loadItems()
    }

    override fun loadItems() = subscription.add(productProvider.getFavourites()
            .map { it.mapToWithReplace(items, { FavouritesItemModel(it, presenter) }) }
            .withUiUpdate()
            .withProgress()
            .runOnIoObsOnMain()
            .subscribe(reportingObserver { handleNetworkException(it, false) }))

    override fun refreshItems() = subscription.add(productProvider.getFavourites()
            .map { it.mapToWithReplace(items, { FavouritesItemModel(it, presenter) }) }
            .withUiUpdate()
            .withRefresh()
            .runOnIoObsOnMain()
            .subscribe(reportingObserver { handleNetworkException(it, true) }))

    fun removeItem(product: Product) = subscription.add(productProvider.removeFavourite(product) //todo rework with network
            .map { it.mapToWithReplace(items, { FavouritesItemModel(it, presenter) }) }
            .withUiUpdate()
            .withProgress()
            .runOnIoObsOnMain()
            .subscribe(reportingObserver({ presenter.getView()?.showRemovedFromFavouritesMessage(product) },
                    { handleNetworkException(it, true) })))
}