package com.softdesign.goodshop.mvp.models

import android.databinding.ObservableField
import com.softdesign.goodshop.R
import com.softdesign.goodshop.data.storage.dto.UserSettings
import com.softdesign.goodshop.mvp.models.abs.BaseModel
import com.softdesign.goodshop.mvp.presenters.interfaces.IRootPresenter
import com.softdesign.goodshop.utils.extentions.runOnIo
import com.softdesign.goodshop.utils.extentions.runOnIoObsOnMain
import com.softdesign.goodshop.utils.extentions.silentObserver
import rx.subscriptions.CompositeSubscription

/**
 * @author Space
 * @date 10.12.2016
 */

class NavHeaderModel(subscription: CompositeSubscription,
                     val presenter: IRootPresenter) : BaseModel(subscription) {

    val userName = ObservableField("")
    val userAvatarUri = ObservableField("")

    fun update(userAuthenticated: Boolean) = with(userName.get().isNullOrBlank()) {
        when {
            this && userAuthenticated -> loadSettings()
            !this && !userAuthenticated -> clearFields()
        }
    }

    fun updateFields(it: UserSettings) {
        userName.set(it.fullName)
        userAvatarUri.set(it.avatarUrl)
        notifyChange()
    }

    fun logout() = subscription.add(userSettingsProvider.logoutUser()
            .runOnIoObsOnMain()
            .subscribe(silentObserver {
                presenter.getView()?.run { showNotification(R.string.notify_logged_out); showAuthScreen(); finish() }
            }))

    private fun loadSettings() = subscription.add(userSettingsProvider.getFromDB()
            .map { it.map { updateFields(it) } }
            .runOnIo()
            .subscribe(silentObserver {}))

    private fun clearFields() {
        userName.set(""); userAvatarUri.set("")
    }
}