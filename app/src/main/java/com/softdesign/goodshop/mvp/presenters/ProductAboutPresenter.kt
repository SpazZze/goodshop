package com.softdesign.goodshop.mvp.presenters

import com.softdesign.goodshop.data.storage.dto.Product
import com.softdesign.goodshop.di.screens.ProductAboutPresenterComponent
import com.softdesign.goodshop.di.screens.ProductAboutPresenterModule
import com.softdesign.goodshop.mvp.models.ProductAboutModel
import com.softdesign.goodshop.mvp.presenters.abs.RootChildPresenter
import com.softdesign.goodshop.mvp.presenters.interfaces.IProductAboutPresenter
import com.softdesign.goodshop.mvp.views.screens.IProductAboutView

/**
 * @author Space
 * @date 19.12.2016
 */

class ProductAboutPresenter(product: Product) : RootChildPresenter<IProductAboutView, ProductAboutModel>(), IProductAboutPresenter {

    init {
        injectRootChild(ProductAboutPresenterComponent::class.java,
                arrayOf<Any>(ProductAboutPresenterModule(product, subscription))).inject(this)
    }
}
