package com.softdesign.goodshop.mvp.views.screens

import com.softdesign.goodshop.mvp.models.AuthModel
import com.softdesign.goodshop.mvp.views.IBindingView

/**
 * @author Space
 * @date 11.12.2016
 */

interface IAuthView : IBindingView<AuthModel>
