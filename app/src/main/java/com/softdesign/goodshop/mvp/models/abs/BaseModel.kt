package com.softdesign.goodshop.mvp.models.abs

import android.databinding.ObservableBoolean
import com.softdesign.goodshop.common.App
import com.softdesign.goodshop.data.managers.DataManager
import com.softdesign.goodshop.data.providers.*
import rx.Observable
import rx.Single
import rx.subscriptions.CompositeSubscription

/**
 * @author Space
 * @date 11.12.2016
 */

open class BaseModel(var subscription: CompositeSubscription) : Model() {

    val dataManager: DataManager by lazy { App.appComponent.dataManager }

    val cartItemProvider: CartItemProvider by lazy { dataManager.cartItemProvider }

    val productProvider: ProductProvider  by lazy { dataManager.productProvider }

    val addressProvider: AddressProvider  by lazy { dataManager.addressProvider }

    val commentProvider: CommentProvider by lazy { dataManager.commentProvider }

    val userSettingsProvider: UserSettingsProvider by lazy { dataManager.userSettingsProvider }

    val isUserAuthenticated: Boolean
        get() = App.userAccessToken.isNotBlank()

    val isProgressBarVisible = ObservableBoolean(false)

    open fun hideProgress() = isProgressBarVisible.set(false)

    open fun showProgress() = isProgressBarVisible.set(true)

    fun <T> Single<T>.withProgress(): Single<T> = this
            .doOnSubscribe { showProgress() }
            .doAfterTerminate { hideProgress() }

    fun <T> Observable<T>.withProgress(): Observable<T> = this
            .doOnSubscribe { showProgress() }
            .doAfterTerminate { hideProgress() }
}