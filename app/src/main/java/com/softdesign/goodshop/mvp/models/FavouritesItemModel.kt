package com.softdesign.goodshop.mvp.models

import android.view.View
import com.softdesign.goodshop.R
import com.softdesign.goodshop.common.databinding.interfaces.IRecyclerItemViewModel
import com.softdesign.goodshop.data.storage.dto.Product
import com.softdesign.goodshop.mvp.presenters.interfaces.IFavouritesPresenter
import com.softdesign.goodshop.utils.extentions.animateView

/**
 * @author Space
 * @date 30.03.2017
 */

class FavouritesItemModel(private val product: Product,
                          private val presenter: IFavouritesPresenter) : IRecyclerItemViewModel {

    override val id = product.id
    val name = product.productName
    val imageUrl = product.imageUrl
    val description = product.description
    val price = product.price

    fun onLikeClick(v: View) {
        v.animateView(R.anim.heartbeat)
        presenter.removeFromFavourites(product)
    }

    fun onAddToCartClick() = presenter.addItemToCart(product)

    fun showProductInfo() {
        presenter.fragmentFactory?.showProductInfoScreen(product)
    }

    override fun equals(other: Any?) = product == (other as? FavouritesItemModel)?.product

    override fun hashCode(): Int = product.hashCode()
}