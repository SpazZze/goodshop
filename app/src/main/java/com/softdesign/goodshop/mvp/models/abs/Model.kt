package com.softdesign.goodshop.mvp.models.abs

import android.databinding.BaseObservable
import com.softdesign.goodshop.common.App

/**
 * @author Space
 * @date 03.01.2017
 */

open class Model : BaseObservable() {

    val context by lazy { App.ctx }
}