package com.softdesign.goodshop.mvp.presenters.interfaces

import com.softdesign.goodshop.data.storage.dto.Product
import com.softdesign.goodshop.mvp.models.ProductModel
import com.softdesign.goodshop.mvp.presenters.abs.interfaces.IRootChildPresenter
import com.softdesign.goodshop.mvp.views.screens.IProductView

/**
 * @author Space
 * @date 11.12.2016
 */


interface IProductPresenter : IRootChildPresenter<IProductView, ProductModel> {

    val itemsCount: Int

    val product: Product

    fun update()
}
