package com.softdesign.goodshop.mvp.presenters

import com.softdesign.goodshop.di.DaggerService
import com.softdesign.goodshop.di.screens.AuthPresenterComponent
import com.softdesign.goodshop.di.screens.AuthPresenterModule
import com.softdesign.goodshop.mvp.models.AuthModel
import com.softdesign.goodshop.mvp.presenters.abs.BindingPresenter
import com.softdesign.goodshop.mvp.presenters.interfaces.IAuthPresenter
import com.softdesign.goodshop.mvp.views.screens.IAuthView

/**
 * @author Space
 * @date 11.12.2016
 */

class AuthPresenter : BindingPresenter<IAuthView, AuthModel>(), IAuthPresenter {

    init {
        DaggerService.getComponent(AuthPresenterComponent::class.java,
                arrayOf<Any>(AuthPresenterModule(subscription, this))).inject(this)
    }

    override fun hideLoginCard(): Boolean {
        when (model.isLoginBtnClicked.get()) {
            false -> return false
            true -> {
                model.isLoginBtnClicked.set(false)
                return true
            }
        }
    }
}