package com.softdesign.goodshop.mvp.models

import android.databinding.ObservableField
import com.softdesign.goodshop.common.databinding.fields.CheckableField
import com.softdesign.goodshop.data.storage.dto.Address
import com.softdesign.goodshop.mvp.models.abs.BaseModel
import com.softdesign.goodshop.mvp.presenters.interfaces.IAddressPresenter
import com.softdesign.goodshop.utils.extentions.runOnIoObsOnMain
import com.softdesign.goodshop.utils.extentions.silentObserver
import rx.subscriptions.CompositeSubscription
import java.util.concurrent.TimeUnit

/**
 * @author Space
 * @date 11.12.2016
 */

class AddressModel(subscription: CompositeSubscription,
                   val address: Address,
                   private val presenter: IAddressPresenter) : BaseModel(subscription) {

    val hintError = ObservableField<String>("")
    val streetError = ObservableField<String>("")

    val hint = CheckableField(context, address.hint, hintError) { !it.isBlank() }
    val street = CheckableField(context, address.street, streetError) { !it.isBlank() }

    val house = ObservableField(address.house)
    val flat = ObservableField(address.flat)
    val floor = ObservableField(address.floor)
    val comment = ObservableField(address.comment)

    val thisAddress: Address
        get() = address.copy(hint.get(), street.get(), comment.get()!!, house.get()!!, flat.get()!!, floor.get()!!)

    fun addAddress() = with(hint.validateField() && street.validateField()) {
        if (this) subscription.add(addressProvider.saveIntoDB(thisAddress)
                .delay(500, TimeUnit.MILLISECONDS)
                .withProgress()
                .runOnIoObsOnMain()
                .subscribe(silentObserver { presenter.finishView() }))
    }

    fun onPointAtMapClick() {}
}
