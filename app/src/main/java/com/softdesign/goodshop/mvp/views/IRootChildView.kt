package com.softdesign.goodshop.mvp.views

import com.softdesign.goodshop.mvp.models.abs.Model

/**
 * @author Space
 * @date 31.01.2017
 */

interface IRootChildView<M : Model> : IBindingView<M>