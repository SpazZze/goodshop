package com.softdesign.goodshop.mvp.models

import com.softdesign.goodshop.mvp.models.abs.BaseModel
import com.softdesign.goodshop.mvp.presenters.interfaces.ISplashPresenter
import com.softdesign.goodshop.utils.Config
import com.softdesign.goodshop.utils.extentions.runOnIoObsOnMain
import com.softdesign.goodshop.utils.extentions.silentObserver
import rx.Single
import rx.subscriptions.CompositeSubscription
import java.util.concurrent.TimeUnit

/**
 * @author Space
 * @date 21.12.2016
 */

class SplashModel(subscription: CompositeSubscription,
                  private val presenter: ISplashPresenter) : BaseModel(subscription) {

    fun loadFirstScreen() = subscription.add(Single.just(isUserAuthenticated)
            .delay(Config.SPLASH_FADE_DELAY, TimeUnit.MILLISECONDS)
            .runOnIoObsOnMain()
            .subscribe(silentObserver { if (it) presenter.getView()?.showRootScreen() else presenter.getView()?.showAuthScreen() }))
}
