package com.softdesign.goodshop.mvp.presenters.interfaces

import com.softdesign.goodshop.mvp.models.CommentAddModel
import com.softdesign.goodshop.mvp.presenters.abs.interfaces.IChildPresenterWithToolbar
import com.softdesign.goodshop.mvp.views.screens.ICommentAddView

/**
 * @author Space
 * @date 05.01.2017
 */

interface ICommentAddPresenter : IChildPresenterWithToolbar<ICommentAddView, CommentAddModel>
