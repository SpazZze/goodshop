package com.softdesign.goodshop.mvp.presenters

import com.softdesign.goodshop.data.storage.dto.Address
import com.softdesign.goodshop.di.screens.AddressPresenterComponent
import com.softdesign.goodshop.di.screens.AddressPresenterModule
import com.softdesign.goodshop.mvp.models.AddressModel
import com.softdesign.goodshop.mvp.presenters.abs.ChildPresenterWithToolbar
import com.softdesign.goodshop.mvp.presenters.interfaces.IAddressPresenter
import com.softdesign.goodshop.mvp.views.screens.IAddressView

/**
 * @author Space
 * @date 11.12.2016
 */

class AddressPresenter(address: Address) : ChildPresenterWithToolbar<IAddressView, AddressModel>(), IAddressPresenter {

    override val addressId: Int = address.id

    init {
        injectRootChild(AddressPresenterComponent::class.java,
                arrayOf<Any>(AddressPresenterModule(subscription, address, this))).inject(this)
    }
}
