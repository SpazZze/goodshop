package com.softdesign.goodshop.mvp.presenters.abs.interfaces

import com.softdesign.goodshop.mvp.models.abs.BaseModel
import com.softdesign.goodshop.mvp.views.IBindingView

/**
 * @author Space
 * @date 05.01.2017
 */

interface IBindingPresenter<V : IBindingView<M>, M : BaseModel> : IModelPresenter<V, M> {

    override fun initView() {
        getView()?.setViewModel(model)
    }
}
