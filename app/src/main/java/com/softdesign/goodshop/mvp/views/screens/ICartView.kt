package com.softdesign.goodshop.mvp.views.screens

import com.softdesign.goodshop.mvp.models.CartModel
import com.softdesign.goodshop.mvp.views.IRootChildWithToolbar

/**
 * @author Space
 * @date 15.04.2017
 */

interface ICartView : IRootChildWithToolbar<CartModel> {

    fun showRemovedFromCartMessage(productName: String)
}