package com.softdesign.goodshop.mvp.views.screens

import com.softdesign.goodshop.mvp.models.ProductInfoModel
import com.softdesign.goodshop.mvp.views.IRootChildWithToolbar

/**
 * @author Space
 * @date 18.12.2016
 */

interface IProductInfoView : IRootChildWithToolbar<ProductInfoModel> {

    fun setupViewPager()
}
