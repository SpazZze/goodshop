package com.softdesign.goodshop.mvp.presenters.interfaces

import com.softdesign.goodshop.data.storage.dto.Product
import com.softdesign.goodshop.mvp.models.ProductInfoModel
import com.softdesign.goodshop.mvp.presenters.abs.interfaces.IChildPresenterWithToolbar
import com.softdesign.goodshop.mvp.views.screens.IProductInfoView

/**
 * @author Space
 * @date 05.01.2017
 */


interface IProductInfoPresenter : IChildPresenterWithToolbar<IProductInfoView, ProductInfoModel> {
    val product: Product
}