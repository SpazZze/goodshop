package com.softdesign.goodshop.mvp.presenters.interfaces

import com.softdesign.goodshop.data.storage.dto.Product
import com.softdesign.goodshop.mvp.models.FavouritesModel
import com.softdesign.goodshop.mvp.presenters.abs.interfaces.IChildPresenterWithToolbar
import com.softdesign.goodshop.mvp.views.screens.IFavouritesView

/**
 * @author Space
 * @date 30.03.2017
 */

interface IFavouritesPresenter : IChildPresenterWithToolbar<IFavouritesView, FavouritesModel> {

    fun removeFromFavourites(product: Product)

    fun addItemToCart(product: Product)
}