package com.softdesign.goodshop.mvp.presenters

import com.softdesign.goodshop.di.DaggerService
import com.softdesign.goodshop.di.screens.SplashPresenterComponent
import com.softdesign.goodshop.di.screens.SplashPresenterModule
import com.softdesign.goodshop.mvp.models.SplashModel
import com.softdesign.goodshop.mvp.presenters.abs.ModelPresenter
import com.softdesign.goodshop.mvp.presenters.interfaces.ISplashPresenter
import com.softdesign.goodshop.mvp.views.screens.ISplashView

/**
 * @author Space
 * @date 21.12.2016
 */


class SplashPresenter : ModelPresenter<ISplashView, SplashModel>(), ISplashPresenter {

    init {
        DaggerService.getComponent(SplashPresenterComponent::class.java,
                arrayOf<Any>(SplashPresenterModule(subscription, this))).inject(this)
    }

    override fun initView() = model.loadFirstScreen()
}