package com.softdesign.goodshop.mvp.models

import android.databinding.ObservableBoolean
import android.databinding.ObservableField
import com.softdesign.goodshop.R
import com.softdesign.goodshop.common.databinding.fields.CheckableField
import com.softdesign.goodshop.data.network.api.req.AddCommentReq
import com.softdesign.goodshop.data.storage.dto.UserSettings
import com.softdesign.goodshop.mvp.models.abs.BaseModel
import com.softdesign.goodshop.mvp.presenters.interfaces.ICommentAddPresenter
import com.softdesign.goodshop.utils.extentions.*
import rx.subscriptions.CompositeSubscription
import java.util.concurrent.TimeUnit

/**
 * @author Space
 * @date 04.01.2017
 */

class CommentAddModel(subscription: CompositeSubscription,
                      val productId: String,
                      val presenter: ICommentAddPresenter) : BaseModel(subscription) {

    val isSubmitEnabled = ObservableBoolean(false)

    val ratingError = ObservableField<String>("")
    val commentError = ObservableField<String>("")

    val rating = CheckableField(context, 0f, ratingError, R.string.hint_set_rating) { it > 0f }.onChange { setSubmitEnable() }
    val commentText = CheckableField(context, "", commentError) { !it.isBlank() }.onChange { setSubmitEnable() }

    private var userSettings = UserSettings("Guest")

    init {
        loadSettings()
    }

    fun onAddClick() = subscription.add(commentProvider.post(productId, addCommentReq)
            .delay(1000, TimeUnit.MILLISECONDS)
            .withProgress()
            .runOnIoObsOnMain()
            .subscribe(reportingObserver({ presenter.finishView() }, { presenter.getView()?.showNetworkError(it) })))

    private fun loadSettings() = subscription.add(userSettingsProvider.getFromDB()
            .map { it.map { userSettings = it } }
            .runOnIo()
            .subscribe(silentObserver {}))

    private val addCommentReq: AddCommentReq
        get() = AddCommentReq(rating.get(), commentText.get(), userSettings.fullName, userSettings.avatarUrl)

    private fun setSubmitEnable(): Unit = isSubmitEnabled.set(rating.isValid && commentText.isValid)
}
