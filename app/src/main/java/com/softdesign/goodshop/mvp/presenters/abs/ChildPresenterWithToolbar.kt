package com.softdesign.goodshop.mvp.presenters.abs

import android.support.annotation.CallSuper
import com.softdesign.goodshop.mvp.models.abs.BaseModel
import com.softdesign.goodshop.mvp.presenters.abs.interfaces.IChildPresenterWithToolbar
import com.softdesign.goodshop.mvp.views.IRootChildWithToolbar

/**
 * @author elena
 * @date 19/12/16
 */

abstract class ChildPresenterWithToolbar<T : IRootChildWithToolbar<M>, M : BaseModel> : RootChildPresenter<T, M>(),
        IChildPresenterWithToolbar<T, M> {

    @CallSuper
    override fun initView() {
        super<RootChildPresenter>.initView()
        getView()?.setupToolbar()
    }
}
