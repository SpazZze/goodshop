package com.softdesign.goodshop.mvp.presenters.interfaces

import com.softdesign.goodshop.data.storage.dto.Product
import com.softdesign.goodshop.data.storage.dto.UserSettings
import com.softdesign.goodshop.mvp.models.MenuCartModel
import com.softdesign.goodshop.mvp.models.NavHeaderModel
import com.softdesign.goodshop.mvp.presenters.abs.interfaces.IBindingPresenter
import com.softdesign.goodshop.mvp.views.screens.IRootView

/**
 * @author Space
 * @date 11.12.2016
 */

interface IRootPresenter : IBindingPresenter<IRootView, NavHeaderModel> {

    fun getCartItemsCount(): Int

    fun checkUserAuth(): Boolean

    fun addItemToCart(product: Product, count: Int)

    fun removeItemFromCart(itemId: String): Any

    fun logout()

    fun updateFields(settings: UserSettings)
}
