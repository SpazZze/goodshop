package com.softdesign.goodshop.mvp.models

import android.databinding.ObservableArrayList
import android.databinding.ObservableBoolean
import android.databinding.ObservableInt
import android.util.Log
import com.softdesign.goodshop.BR
import com.softdesign.goodshop.R
import com.softdesign.goodshop.common.databinding.abs.RecyclerBindingHelper
import com.softdesign.goodshop.mvp.models.abs.RecyclerViewModel
import com.softdesign.goodshop.mvp.presenters.interfaces.ICartPresenter
import com.softdesign.goodshop.mvp.views.screens.ICartView
import com.softdesign.goodshop.utils.Config.SUM_FOR_DISCOUNT
import com.softdesign.goodshop.utils.extentions.mapToWithReplace
import com.softdesign.goodshop.utils.extentions.onChange
import com.softdesign.goodshop.utils.extentions.reportingObserver
import com.softdesign.goodshop.utils.extentions.runOnIoObsOnMain
import rx.subscriptions.CompositeSubscription

/**
 * @author Space
 * @date 15.04.2017
 */

class CartModel(subscription: CompositeSubscription,
                presenter: ICartPresenter) :
        RecyclerViewModel<CartItemModel, ICartPresenter, ICartView>(subscription, R.string.no_content_cart_list, presenter) {

    override val bindingHelper = RecyclerBindingHelper<CartItemModel>(R.layout.item_cart, BR.viewModel)

    override val items = ObservableArrayList<CartItemModel>().onChange { updateViews() }

    val totalCount = ObservableInt(0)
    val totalDiscount = ObservableInt(0)
    val totalCost = ObservableInt(0)

    val isFooterVisible = ObservableBoolean(false)

    init {
        loadItems()
    }

    fun onOrderClick() {
        Log.e("DEV", "${javaClass.simpleName} onOrderClick: ")
    }

    override fun loadItems() = subscription.add(cartItemProvider.getList()
            .map { it.sortedBy { it.product.id } }
            .map { it.mapToWithReplace(items, { CartItemModel(it, presenter) }) }
            .withUiUpdate()
            .withProgress()
            .runOnIoObsOnMain()
            .subscribe(reportingObserver { handleNetworkException(it, false) }))

    override fun refreshItems() = subscription.add(cartItemProvider.getList()
            .map { it.sortedBy { it.product.id } }
            .map { it.mapToWithReplace(items, { CartItemModel(it, presenter) }) }
            .withUiUpdate()
            .withRefresh()
            .runOnIoObsOnMain()
            .subscribe(reportingObserver { handleNetworkException(it, true) }))

    fun removeItemFromCart(cartItemId: String, itemName: String) = subscription.add(cartItemProvider.delete(cartItemId)
            .map { items.firstOrNull { it.id == cartItemId }?.let { items.remove(it) } }
            .withUiUpdate()
            .withProgress()
            .runOnIoObsOnMain()
            .subscribe(reportingObserver({ presenter.getView()?.showRemovedFromCartMessage(itemName) },
                    { handleNetworkException(it, true) })))

    private fun updateViews() {
        isFooterVisible.set(items.isNotEmpty())
        if (items.isEmpty()) return
        totalCount.set(items.sumBy { it.cartItem.count })
        val cost = items.sumBy { it.cost }
        totalDiscount.set(if (cost > SUM_FOR_DISCOUNT) (cost * 0.15).toInt() else 0)
        totalCost.set(cost - totalDiscount.get())
    }
}