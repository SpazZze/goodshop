package com.softdesign.goodshop.data.managers

/**
 * @author Space
 * @date 11.12.2016
 */


object PreferencesManager {
    val prefsName = "com.softdesign.goodshop.space"
    val USER_LOGIN = "USER_LOGIN"
    val ACCESS_TOKEN = "ACCESS_TOKEN"
    val USER_ID = "USER_ID"
    val LAST_PRODUCTS_UPDATE = "LAST_PRODUCTS_UPDATE"
    val LAST_FAVOURITES_UPDATE = "LAST_PRODUCTS_UPDATE"
    val LAST_CART_UPDATE = "LAST_PRODUCTS_UPDATE"
}