package com.softdesign.goodshop.data.network.api.res

import com.softdesign.goodshop.data.storage.dto.UserSettings

/**
 * @author Space
 * @date 29.01.2017
 */

data class AuthRes(val token: String,
                   val settings: UserSettings)