package com.softdesign.goodshop.data.storage.realm

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import io.realm.annotations.RealmClass

/**
 * @author Space
 * @date 10.12.2016
 */

@RealmClass
open class RealmUserSettings : RealmObject() {
    @PrimaryKey
    open var fullName: String = ""
    open var avatarUrl: String = ""
    open var phone: String = ""
    open var email: String = ""
    open var isStatusNotifyEnabled: Boolean = true
    open var isSpecialNotifyEnabled: Boolean = true
}