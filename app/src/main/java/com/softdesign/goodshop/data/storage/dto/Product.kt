package com.softdesign.goodshop.data.storage.dto

import android.os.Parcel
import android.os.Parcelable
import com.softdesign.goodshop.common.databinding.interfaces.IRecyclerItemViewModel
import java.util.*

/**
 * @author Space
 * @date 08.12.2016
 */

data class Product(override val id: String,
                   val productName: String,
                   val price: Int,
                   val active: Boolean,
                   val rating: Float = 0.0f,
                   val description: String = "",
                   val imageUrl: String = "",
                   val comments: List<Comment> = ArrayList<Comment>(),
                   val lastUpdated: String = "",
                   val isFavourite: Boolean = true,
                   val count: Int = 0) : Parcelable, IRecyclerItemViewModel {
    companion object {
        @JvmField val CREATOR: Parcelable.Creator<Product> = object : Parcelable.Creator<Product> {
            override fun createFromParcel(source: Parcel): Product = Product(source)
            override fun newArray(size: Int): Array<Product?> = arrayOfNulls(size)
        }
    }

    constructor(source: Parcel) : this(source.readString(), source.readString(), source.readInt(), 1 == source.readInt(), source.readFloat(), source.readString(), source.readString(), source.createTypedArrayList(Comment.CREATOR), source.readString(), 1 == source.readInt(), source.readInt())

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel?, flags: Int) {
        dest?.writeString(id)
        dest?.writeString(productName)
        dest?.writeInt(price)
        dest?.writeInt((if (active) 1 else 0))
        dest?.writeFloat(rating)
        dest?.writeString(description)
        dest?.writeString(imageUrl)
        dest?.writeTypedList(comments)
        dest?.writeString(lastUpdated)
        dest?.writeInt((if (isFavourite) 1 else 0))
        dest?.writeInt(count)
    }
}