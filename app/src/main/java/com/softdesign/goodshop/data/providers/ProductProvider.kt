package com.softdesign.goodshop.data.providers

import com.softdesign.goodshop.common.App
import com.softdesign.goodshop.data.converters.ProductConverter
import com.softdesign.goodshop.data.managers.PreferencesManager.LAST_FAVOURITES_UPDATE
import com.softdesign.goodshop.data.managers.PreferencesManager.LAST_PRODUCTS_UPDATE
import com.softdesign.goodshop.data.network.RestService
import com.softdesign.goodshop.data.repositories.ProductRepository
import com.softdesign.goodshop.data.storage.dto.Product
import com.softdesign.goodshop.data.storage.realm.RealmProduct
import com.softdesign.goodshop.utils.extentions.*
import org.funktionale.option.Option
import rx.Observable
import rx.Single
import java.util.*
import kotlin.properties.Delegates

/**
 * @author Space
 * @date 08.12.2016
 */

class ProductProvider(private val service: RestService,
                      private val converter: ProductConverter,
                      private val repository: ProductRepository) {

    private var lastFavouritesUpdate: String by Delegates.preference(LAST_FAVOURITES_UPDATE, provideInitDate())

    private var lastProductsUpdate: String by Delegates.preference(LAST_PRODUCTS_UPDATE, provideInitDate())

    fun get(id: String): Single<Option<Product>> = repository.get(id)
            .map { it.map { converter.toObject(it) } }

    fun getList(): Observable<List<Product>> = getListFromDB()
            .concatWith(getListFromRest(lastProductsUpdate))

    fun getListFromDB(): Single<List<Product>> = repository.getList<RealmProduct>()
            .map { it.map { converter.toObject(it) } }

    fun getListFromRest(lastUpdateTime: String,
                        dateString: String = Date().convertDate()): Single<List<Product>> =
            service.getProducts(lastUpdateTime)
                    .withConnectionStatusCheck()
                    .doOnSuccess { lastProductsUpdate = dateString }
                    .flatMap { repository.storeItems(it.map { converter.toRealm(it, dateString) }) }
                    .map { it.map { converter.toObject(it) } }

    fun getFavourites(): Observable<List<Product>> = getFavouritesFromDB()
            .concatWith(getFavouritesFromRest(lastProductsUpdate))

    fun getFavouritesFromDB(): Single<List<Product>> = repository.getFavourites()
            .map { it.map { converter.toObject(it) } }

    fun getFavouritesFromRest(lastUpdateTime: String = provideInitDate(),
                              dateString: String = Date().convertDate()): Single<List<Product>> =
            service.getFavourites(App.userId, lastUpdateTime)
                    .withAuthCheck()
                    .withConnectionStatusCheck()
                    .doOnSuccess { lastFavouritesUpdate = dateString }
                    .flatMap { if (it.isNotEmpty()) repository.updateFavourites(it) else Single.just(emptyList()) }
                    .map { it.map { converter.toObject(it) } }

    fun removeFavourite(item: Product): Single<List<Product>> = Single.just(item)
            .map { converter.toRealm(it.copy(isFavourite = false)) }
            .flatMap { repository.removeFavourite(it) }
            .map { it.map { converter.toObject(it) } }

    fun saveIntoDB(item: Product): Single<Product> = Single.just(item)
            .map { converter.toRealm(it) }
            .flatMap { repository.storeItem(it) }
            .map { converter.toObject(it) }
}