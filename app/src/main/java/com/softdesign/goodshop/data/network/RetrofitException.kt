package com.softdesign.goodshop.data.network

import com.softdesign.goodshop.data.network.RetrofitException.Kind.*
import retrofit2.Response
import retrofit2.adapter.rxjava.HttpException
import java.io.IOException

/**
 * @author Space
 * @date 17.12.2016
 */


class RetrofitException private constructor(val exception: Throwable,
                                            val kind: RetrofitException.Kind = UNEXPECTED,
                                            val response: Response<*>? = null) : RuntimeException(exception) {
    enum class Kind {
        NETWORK,
        HTTP,
        UNEXPECTED
    }

    companion object {
        fun httpError(exception: Throwable, response: Response<*>) = RetrofitException(exception, HTTP, response)
        fun networkError(exception: IOException) = RetrofitException(exception, NETWORK)
        fun unexpectedError(exception: Throwable) = RetrofitException(exception)
    }
}

fun Throwable.asRetrofitException() = when (this) {
    is HttpException -> RetrofitException.httpError(this, this.response())
    is IOException -> RetrofitException.networkError(this)
    else -> RetrofitException.unexpectedError(this)
}