package com.softdesign.goodshop.data.providers

import com.softdesign.goodshop.data.converters.UserSettingsConverter
import com.softdesign.goodshop.data.managers.FileManager
import com.softdesign.goodshop.data.managers.PreferencesManager
import com.softdesign.goodshop.data.network.RestService
import com.softdesign.goodshop.data.network.api.req.AuthReq
import com.softdesign.goodshop.data.repositories.UserSettingsRepository
import com.softdesign.goodshop.data.storage.dto.UserSettings
import com.softdesign.goodshop.utils.extentions.preference
import com.softdesign.goodshop.utils.extentions.withConnectionStatusCheck
import com.softdesign.goodshop.utils.mockAuthReq
import org.funktionale.option.Option
import rx.Single
import kotlin.properties.Delegates

/**
 * @author Space
 * @date 10.12.2016
 */


class UserSettingsProvider(private val service: RestService,
                           private val converter: UserSettingsConverter,
                           private val repository: UserSettingsRepository) {

    private var userId: String by Delegates.preference(PreferencesManager.USER_LOGIN, "")
    private var userToken: String by Delegates.preference(PreferencesManager.ACCESS_TOKEN, "")

    fun authUser(req: AuthReq): Single<UserSettings> = mockAuthReq()
            .withConnectionStatusCheck()
            .doOnSuccess { userToken = it.token; userId = req.login }
            .map { converter.toRealm(it.settings) }
            .flatMap { repository.storeItem(it) }
            .map { converter.toObject(it) }

    fun getFromDB(): Single<Option<UserSettings>> = repository.get()
            .map { it.map { converter.toObject(it) } }

    fun saveIntoDB(userSettings: UserSettings): Single<UserSettings> = Single.just(userSettings)
            .map { converter.toRealm(it) }
            .flatMap { repository.storeItem(it) }
            .map { converter.toObject(it) }

    fun logoutUser(): Single<Boolean> = repository.clearUserData()
            .map { userId = ""; userToken = "" }
            .map { FileManager.clearCache() }
}
