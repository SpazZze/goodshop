package com.softdesign.goodshop.data.managers

import android.content.ContentValues
import android.graphics.Bitmap
import android.os.Environment
import android.provider.MediaStore
import com.softdesign.goodshop.common.App
import com.softdesign.goodshop.utils.extentions.compressInto
import com.softdesign.goodshop.utils.extentions.create
import timber.log.Timber
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*


/**
 * @author Space
 * @date 05.03.2017
 */

object FileManager {

    private val context by lazy { App.appComponent.appContext }
    private val timeStamp get() = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(Date())
    private val localAvatarFileName = "compressedUserAvatar.png"

    val cacheRootDir: String by lazy { context.cacheDir.absolutePath }
    val picturesDir: File by lazy { Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) }


    fun cacheAvatarImage(selectedImage: Bitmap) = createCacheFile(localAvatarFileName)
            ?.apply { selectedImage.compressInto(Bitmap.CompressFormat.PNG, 100, this) }

    fun createImageFile(): File? = try {
        File(picturesDir, "IMG_$timeStamp.png").create().apply {
            val values = ContentValues()
            values.put(MediaStore.Images.Media.DATE_TAKEN, System.currentTimeMillis())
            values.put(MediaStore.Images.Media.MIME_TYPE, "image/png")
            values.put(MediaStore.MediaColumns.DATA, absolutePath)
            context.contentResolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values)
        }
    } catch (e: IOException) {
        Timber.e(e, "$javaClass createImageFile error: "); null
    }

    fun createCacheFile(name: String): File? = try {
        File(cacheRootDir, name).create()
    } catch (e: IOException) {
        Timber.e(e, "$javaClass createCacheFile error: "); null
    }

    fun clearCache() = File(cacheRootDir).deleteRecursively()
}