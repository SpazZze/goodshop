package com.softdesign.goodshop.data.storage.dto

import android.os.Parcel
import android.os.Parcelable

/**
 * @author Space
 * @date 10.12.2016
 */

data class CartItem(val product: Product,
                    var count: Int = 0) : Parcelable {

    companion object {
        @JvmField val CREATOR: Parcelable.Creator<CartItem> = object : Parcelable.Creator<CartItem> {
            override fun createFromParcel(source: Parcel): CartItem = CartItem(source)
            override fun newArray(size: Int): Array<CartItem?> = arrayOfNulls(size)
        }
    }

    constructor(source: Parcel) : this(source.readParcelable<Product>(Product::class.java.classLoader), source.readInt())

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel?, flags: Int) {
        dest?.writeParcelable(product, 0)
        dest?.writeInt(count)
    }
}