package com.softdesign.goodshop.data.network.api.res

import com.google.gson.annotations.SerializedName

/**
 * @author Space
 * @date 17.12.2016
 */

data class BaseResponse(@SerializedName("statusCode") val statusCode: Int = 0,
                        @SerializedName("message") val message: String = "")