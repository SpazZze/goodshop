package com.softdesign.goodshop.data.network.api.req

/**
 * @author Space
 * @date 04.01.2017
 */

class AddCommentReq(val raiting: Float,
                    val comment: String,
                    val userName: String,
                    val avatar: String)