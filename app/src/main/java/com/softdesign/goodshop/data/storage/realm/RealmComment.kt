package com.softdesign.goodshop.data.storage.realm

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import io.realm.annotations.RealmClass
import java.util.*

/**
 * @author Space
 * @date 17.12.2016
 */

@RealmClass
open class RealmComment : RealmObject() {
    @PrimaryKey
    open var id: String = ""
    open var productId: String = ""
    open var userName: String = ""
    open var avatar: String = ""
    open var rating: Int = 0
    open var commentDate: Date = Date()
    open var comment: String = ""
    open var active: Boolean = true
}