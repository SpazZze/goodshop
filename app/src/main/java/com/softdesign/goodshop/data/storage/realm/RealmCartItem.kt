package com.softdesign.goodshop.data.storage.realm

import com.softdesign.goodshop.utils.extentions.applyIf
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import io.realm.annotations.RealmClass

/**
 * @author Space
 * @date 10.12.2016
 */

@RealmClass
open class RealmCartItem : RealmObject() {
    @PrimaryKey
    open var id: String = ""
    open var product: RealmProduct = RealmProduct()
    open var count: Int = 0

    fun update(product: RealmProduct?) = applyIf(product != null) { this.product = product!! }
}