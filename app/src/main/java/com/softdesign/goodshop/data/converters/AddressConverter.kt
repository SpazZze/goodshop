package com.softdesign.goodshop.data.converters

import com.softdesign.goodshop.data.storage.dto.Address
import com.softdesign.goodshop.data.storage.realm.RealmAddress

/**
 * @author Space
 * @date 08.12.2016
 */

class AddressConverter {

    fun toRealm(src: Address): RealmAddress {
        val out = RealmAddress()
        out.id = src.id
        out.hint = src.hint
        out.comment = src.comment
        out.street = src.street
        out.house = src.house
        out.flat = src.flat
        out.floor = src.floor
        return out
    }

    fun toObject(src: RealmAddress): Address {
        return Address(
                id = src.id,
                hint = src.hint,
                comment = src.comment,
                street = src.street,
                house = src.house,
                flat = src.flat,
                floor = src.floor)
    }
}