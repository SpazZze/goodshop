package com.softdesign.goodshop.data.repositories

import com.softdesign.goodshop.data.storage.realm.RealmProduct
import com.softdesign.goodshop.utils.extentions.auto
import com.softdesign.goodshop.utils.extentions.autoExecuteTransaction
import io.realm.Realm
import org.funktionale.option.Option
import org.funktionale.option.toOption
import rx.Single
import rx.lang.kotlin.single

/**
 * @author Space
 * @date 08.12.2016
 */


class ProductRepository : Repository<RealmProduct>() {
    fun get(id: String): Single<Option<RealmProduct>> = single {
        Realm.getDefaultInstance().auto {
            it.onSuccess(where(RealmProduct::class.java)
                    .equalTo("id", id)
                    .findFirst()
                    .toOption())
        }
    }

    fun storeItem(item: RealmProduct): Single<RealmProduct> = single {
        Realm.getDefaultInstance().autoExecuteTransaction {
            it.onSuccess(copyToRealmOrUpdate(item))
        }
    }

    override fun storeItems(items: List<RealmProduct>): Single<List<RealmProduct>> = single {
        Realm.getDefaultInstance().autoExecuteTransaction {
            val updated = items
                    .filter { it.active }
                    .map {
                        it.update(where(RealmProduct::class.java).equalTo("id", it.id).findFirst())
                                .apply { it.comments.removeAll { !it.active } }
                    }
            items.filter { !it.active }.forEach { it.deleteFromRealm() }
            copyToRealmOrUpdate(updated)
            it.onSuccess(where(RealmProduct::class.java)
                    .findAll()
                    .toList())
        }
    }

    @Throws(IllegalArgumentException::class)
    fun updateFavourites(items: List<String>): Single<List<RealmProduct>> = single {
        Realm.getDefaultInstance().autoExecuteTransaction {
            it.onSuccess(copyToRealmOrUpdate(where(RealmProduct::class.java)
                    .`in`("id", items.toTypedArray())
                    .findAll()
                    .toList()
                    .map { it.apply { isFavourite = true } }))
        }
    }

    fun getFavourites(): Single<List<RealmProduct>> = single {
        Realm.getDefaultInstance().auto {
            it.onSuccess(where(RealmProduct::class.java)
                    .equalTo("isFavourite", true)
                    .findAll()
                    .toList())
        }
    }

    fun removeFavourite(item: RealmProduct): Single<List<RealmProduct>> = single {
        Realm.getDefaultInstance().autoExecuteTransaction {
            copyToRealmOrUpdate(item)
            it.onSuccess(where(RealmProduct::class.java)
                    .equalTo("isFavourite", true)
                    .findAll()
                    .toList())
        }
    }
}