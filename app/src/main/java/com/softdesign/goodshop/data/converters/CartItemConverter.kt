package com.softdesign.goodshop.data.converters

import com.softdesign.goodshop.data.storage.dto.CartItem
import com.softdesign.goodshop.data.storage.realm.RealmCartItem
import com.softdesign.goodshop.data.storage.rest.RestCartItem

/**
 * @author Space
 * @date 10.12.2016
 */

class CartItemConverter(val productConverter: ProductConverter) {

    fun toRealm(src: RestCartItem) = RealmCartItem().apply {
        id = src.id
        count = src.count
    }

    fun toRealm(src: CartItem) = RealmCartItem().apply {
        id = src.product.id
        product = productConverter.toRealm(src.product)
        count = src.count
    }

    fun toObject(src: RealmCartItem) = CartItem(
            product = productConverter.toObject(src.product),
            count = src.count)
}