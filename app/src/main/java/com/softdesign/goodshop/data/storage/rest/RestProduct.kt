package com.softdesign.goodshop.data.storage.rest

import com.google.gson.annotations.SerializedName
import java.util.*


/**
 * @author Space
 * @date 17.12.2016
 */

data class RestProduct(@SerializedName("_id") val id: String = "",
                       @SerializedName("productName") val productName: String = "",
                       @SerializedName("imageUrl") val imageUrl: String = "",
                       @SerializedName("description") val description: String = "",
                       @SerializedName("price") val price: Int = 0,
                       @SerializedName("raiting") val rating: Float = 0.0f,
                       @SerializedName("active") val active: Boolean = false,
                       @SerializedName("comments") val comments: List<RestComment> = ArrayList<RestComment>())