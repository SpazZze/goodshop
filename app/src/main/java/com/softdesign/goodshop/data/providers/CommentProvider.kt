package com.softdesign.goodshop.data.providers

import com.softdesign.goodshop.data.converters.CommentConverter
import com.softdesign.goodshop.data.network.RestService
import com.softdesign.goodshop.data.network.api.req.AddCommentReq
import com.softdesign.goodshop.data.repositories.CommentRepository
import com.softdesign.goodshop.data.storage.dto.Comment
import com.softdesign.goodshop.utils.extentions.withConnectionStatusCheck
import rx.Observable
import rx.Single

/**
 * @author Space
 * @date 17.12.2016
 */

class CommentProvider(private val service: RestService,
                      private val converter: CommentConverter,
                      private val repository: CommentRepository) {

    fun post(productId: String, item: AddCommentReq): Single<Comment> = service.addComment(productId, item)
            .withConnectionStatusCheck()
            .map { converter.toRealm(productId, it) }
            .flatMap { repository.storeItem(productId, it) }
            .map { converter.toObject(it) }

    fun getList(productId: String): Observable<List<Comment>> = getListFromDB(productId)
            .concatWith(getListFromRest(productId))

    private fun getListFromDB(productId: String): Single<List<Comment>> = repository.getList(productId)
            .map { it.filter { it.active }.map { converter.toObject(it) } }

    private fun getListFromRest(productId: String): Single<List<Comment>> = service.getComments(productId)
            .withConnectionStatusCheck()
            .flatMap { repository.storeItems(productId, it.map { converter.toRealm(productId, it) }) }
            .map { it.filter { it.active }.map { converter.toObject(it) } }
}
