package com.softdesign.goodshop.data.network.api.req

/**
 * @author Space
 * @date 30.03.2017
 */

class SocialAuthReq(val firstName: String,
                    val lastName: String,
                    val avatarUrl: String,
                    val email: String,
                    val phone: String)