package com.softdesign.goodshop.data.repositories

import com.softdesign.goodshop.utils.extentions.auto
import com.softdesign.goodshop.utils.extentions.autoExecuteTransaction
import io.realm.Realm
import io.realm.RealmObject
import rx.Single
import rx.lang.kotlin.single

/**
 * @author Space
 * @date 28.01.2017
 */

abstract class Repository<T : RealmObject> {

    inline fun <reified T : RealmObject> getList(): Single<List<T>> = single {
        Realm.getDefaultInstance().auto {
            it.onSuccess(where(T::class.java)
                    .findAll()
                    .toList())
        }
    }

    open fun storeItems(items: List<T>): Single<List<T>> = single {
        Realm.getDefaultInstance().autoExecuteTransaction {
            it.onSuccess(copyToRealmOrUpdate(items))
        }
    }
}