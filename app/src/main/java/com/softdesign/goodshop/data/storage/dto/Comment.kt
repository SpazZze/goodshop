package com.softdesign.goodshop.data.storage.dto

import android.os.Parcel
import android.os.Parcelable
import java.util.*

/**
 * @author Space
 * @date 17.12.2016
 */

data class Comment(val id: String,
                   val userName: String,
                   val rating: Int,
                   val commentDate: Date,
                   val comment: String,
                   val active: Boolean,
                   val productId: String,
                   val avatar: String = "") : Parcelable {
    companion object {
        @JvmField val CREATOR: Parcelable.Creator<Comment> = object : Parcelable.Creator<Comment> {
            override fun createFromParcel(source: Parcel): Comment = Comment(source)
            override fun newArray(size: Int): Array<Comment?> = arrayOfNulls(size)
        }
    }

    constructor(source: Parcel) : this(source.readString(), source.readString(), source.readInt(), source.readSerializable() as Date, source.readString(), 1 == source.readInt(), source.readString(), source.readString())

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel?, flags: Int) {
        dest?.writeString(id)
        dest?.writeString(userName)
        dest?.writeInt(rating)
        dest?.writeSerializable(commentDate)
        dest?.writeString(comment)
        dest?.writeInt((if (active) 1 else 0))
        dest?.writeString(productId)
        dest?.writeString(avatar)
    }
}