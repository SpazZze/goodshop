package com.softdesign.goodshop.data.network.interceptors

import com.softdesign.goodshop.common.App
import okhttp3.Interceptor
import okhttp3.Response
import java.io.IOException

/**
 * @author Space
 * @date 11.12.2016
 */

class HeaderInterceptor : Interceptor {

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response = chain.proceed(
            chain.request().newBuilder()
                    .header("X-Access-Token", App.userId)
                    .header("Request-User-Id", App.userAccessToken)
                    .header("User-Agent", "GoodShopApp")
                    .header("Cache-Control", "max-age=" + 60 * 60 * 24).build())
}
