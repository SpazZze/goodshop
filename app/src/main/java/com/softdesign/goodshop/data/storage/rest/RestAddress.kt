package com.softdesign.goodshop.data.storage.rest

import com.google.gson.annotations.SerializedName

/**
 * @author Space
 * @date 30.03.2017
 */

data class RestAddress(@SerializedName("_id") val id: String = "",
                       @SerializedName("name") val name: String = "",
                       @SerializedName("street") val street: String = "",
                       @SerializedName("comment") val comment: String = "",
                       @SerializedName("house") val house: Int = -1,
                       @SerializedName("apartment") val apartment: Int = -1,
                       @SerializedName("floor") val floor: Int = -1)