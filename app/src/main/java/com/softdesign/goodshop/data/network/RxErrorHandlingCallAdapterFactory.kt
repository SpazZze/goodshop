package com.softdesign.goodshop.data.network

import retrofit2.Call
import retrofit2.CallAdapter
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import rx.Single
import java.lang.reflect.Type

/**
 * @author Space
 * @date 17.12.2016
 */


class RxErrorHandlingCallAdapterFactory(val original: RxJavaCallAdapterFactory = RxJavaCallAdapterFactory.create()) : CallAdapter.Factory() {

    override fun get(returnType: Type, annotations: Array<Annotation>, retrofit: Retrofit): CallAdapter<*> =
            RxCallAdapterWrapper(original.get(returnType, annotations, retrofit))

    private class RxCallAdapterWrapper(private val wrapped: CallAdapter<*>) : CallAdapter<Single<*>> {

        override fun responseType(): Type = wrapped.responseType()

        override fun <R> adapt(call: Call<R>): Single<*> = (wrapped.adapt(call) as Single<*>)
                .onErrorResumeNext { Single.error(it.asRetrofitException()) }
    }
}