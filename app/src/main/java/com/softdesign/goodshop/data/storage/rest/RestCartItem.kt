package com.softdesign.goodshop.data.storage.rest

import com.google.gson.annotations.SerializedName

/**
 * @author Space
 * @date 30.03.2017
 */

data class RestCartItem(@SerializedName("_id") val id: String = "",
                        @SerializedName("count") val count: Int = 1)