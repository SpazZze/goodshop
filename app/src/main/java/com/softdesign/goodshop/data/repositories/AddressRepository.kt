package com.softdesign.goodshop.data.repositories

import com.softdesign.goodshop.data.storage.realm.RealmAddress
import com.softdesign.goodshop.utils.extentions.auto
import com.softdesign.goodshop.utils.extentions.autoExecuteTransaction
import io.realm.Realm
import org.funktionale.option.Option
import org.funktionale.option.toOption
import rx.Single
import rx.lang.kotlin.single

/**
 * @author Space
 * @date 08.12.2016
 */


class AddressRepository : Repository<RealmAddress>() {

    fun get(id: Int): Single<Option<RealmAddress>> = single {
        Realm.getDefaultInstance().auto {
            it.onSuccess(where(RealmAddress::class.java)
                    .equalTo("id", id)
                    .findFirst()
                    .toOption())
        }
    }

    fun storeItem(item: RealmAddress): Single<RealmAddress> = single {
        Realm.getDefaultInstance().autoExecuteTransaction {
            it.onSuccess(with(item) {
                if (id == -1) id = where(RealmAddress::class.java).findAll()?.max("id")?.toInt()?.inc() ?: item.id.inc()
                copyToRealmOrUpdate(this)
            })
        }
    }

    fun removeItem(id: Int): Single<Boolean> = single {
        Realm.getDefaultInstance().autoExecuteTransaction {
            val existing = where(RealmAddress::class.java).equalTo("id", id).findFirst()
            it.onSuccess(existing?.run { deleteFromRealm(); true } ?: false)
        }
    }
}