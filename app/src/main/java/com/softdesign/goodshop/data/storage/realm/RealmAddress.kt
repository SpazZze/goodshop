package com.softdesign.goodshop.data.storage.realm

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import io.realm.annotations.RealmClass

/**
 * @author Space
 * @date 08.12.2016
 */

@RealmClass
open class RealmAddress : RealmObject() {
    @PrimaryKey
    open var id: Int = -1
    open var hint: String = ""
    open var comment: String = ""
    open var street: String = ""
    open var house: String = ""
    open var flat: String = ""
    open var floor: String = ""
}