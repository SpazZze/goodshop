package com.softdesign.goodshop.data.network.api.req

/**
 * @author Space
 * @date 11.12.2016
 */

class AuthReq(val login: String,
              val password: String)