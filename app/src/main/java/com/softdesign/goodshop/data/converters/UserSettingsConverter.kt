package com.softdesign.goodshop.data.converters

import com.softdesign.goodshop.data.storage.dto.UserSettings
import com.softdesign.goodshop.data.storage.realm.RealmUserSettings

/**
 * @author Space
 * @date 10.12.2016
 */

class UserSettingsConverter {

    fun toRealm(src: UserSettings): RealmUserSettings {
        val out = RealmUserSettings()
        out.fullName = src.fullName
        out.avatarUrl = src.avatarUrl
        out.phone = src.phone
        out.email = src.email
        out.isStatusNotifyEnabled = src.isStatusNotifyEnabled
        out.isSpecialNotifyEnabled = src.isSpecialNotifyEnabled
        return out
    }

    fun toObject(src: RealmUserSettings) = UserSettings(
            fullName = src.fullName,
            avatarUrl = src.avatarUrl,
            phone = src.phone,
            email = src.email,
            isStatusNotifyEnabled = src.isStatusNotifyEnabled,
            isSpecialNotifyEnabled = src.isSpecialNotifyEnabled)
}
