package com.softdesign.goodshop.data.converters

import com.softdesign.goodshop.data.storage.dto.Comment
import com.softdesign.goodshop.data.storage.dto.Product
import com.softdesign.goodshop.data.storage.realm.RealmProduct
import com.softdesign.goodshop.data.storage.rest.RestProduct
import java.util.*

/**
 * @author Space
 * @date 08.12.2016
 */

class ProductConverter(val commentConverter: CommentConverter) {

    fun toRealm(src: RestProduct, lastUpdated: String): RealmProduct {
        val out = RealmProduct()
        out.id = src.id
        out.productName = src.productName
        out.imageUrl = src.imageUrl
        out.description = src.description
        out.price = src.price
        out.rating = src.rating
        out.active = src.active
        out.comments.addAll(src.comments.map { commentConverter.toRealm(src.id, it) })
        out.lastUpdated = lastUpdated
        return out
    }

    fun toRealm(src: Product): RealmProduct {
        val out = RealmProduct()
        out.id = src.id
        out.productName = src.productName
        out.imageUrl = src.imageUrl
        out.description = src.description
        out.price = src.price
        out.rating = src.rating
        out.active = src.active
        out.comments.addAll(src.comments.map { commentConverter.toRealm(it) })
        out.lastUpdated = src.lastUpdated
        out.isFavourite = src.isFavourite
        out.count = src.count
        return out
    }

    fun toObject(src: RestProduct, lastUpdated: String) = Product(
            id = src.id,
            productName = src.productName,
            imageUrl = src.imageUrl,
            description = src.description,
            price = src.price,
            rating = src.rating,
            active = src.active,
            comments = ArrayList<Comment>(src.comments.map { commentConverter.toObject(src.id, it) }),
            lastUpdated = lastUpdated)

    fun toObject(src: RealmProduct) = Product(
            id = src.id,
            productName = src.productName,
            imageUrl = src.imageUrl,
            description = src.description,
            price = src.price,
            rating = src.rating,
            active = src.active,
            comments = ArrayList<Comment>(src.comments.map { commentConverter.toObject(it) }),
            lastUpdated = src.lastUpdated,
            isFavourite = src.isFavourite,
            count = src.count)
}