package com.softdesign.goodshop.data.storage.dto

import android.os.Parcel
import android.os.Parcelable

/**
 * @author Space
 * @date 08.12.2016
 */

data class UserSettings(val fullName: String = "Not Specified",
                        val avatarUrl: String = "",
                        val phone: String = "",
                        val email: String = "",
                        val isStatusNotifyEnabled: Boolean = true,
                        val isSpecialNotifyEnabled: Boolean = true) : Parcelable {

    companion object {
        @JvmField val CREATOR: Parcelable.Creator<UserSettings> = object : Parcelable.Creator<UserSettings> {
            override fun createFromParcel(source: Parcel): UserSettings = UserSettings(source)
            override fun newArray(size: Int): Array<UserSettings?> = arrayOfNulls(size)
        }
    }

    constructor(source: Parcel) : this(source.readString(), source.readString(), source.readString(), source.readString(), 1 == source.readInt(), 1 == source.readInt())

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel?, flags: Int) {
        dest?.writeString(fullName)
        dest?.writeString(avatarUrl)
        dest?.writeString(phone)
        dest?.writeString(email)
        dest?.writeInt((if (isStatusNotifyEnabled) 1 else 0))
        dest?.writeInt((if (isSpecialNotifyEnabled) 1 else 0))
    }
}