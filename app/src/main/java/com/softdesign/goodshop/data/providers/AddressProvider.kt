package com.softdesign.goodshop.data.providers

import com.softdesign.goodshop.data.converters.AddressConverter
import com.softdesign.goodshop.data.repositories.AddressRepository
import com.softdesign.goodshop.data.storage.dto.Address
import com.softdesign.goodshop.data.storage.realm.RealmAddress
import rx.Single

/**
 * @author Space
 * @date 08.12.2016
 */

class AddressProvider(private val converter: AddressConverter,
                      private val repository: AddressRepository) {

    fun getListFromDB(): Single<List<Address>> = repository.getList<RealmAddress>()
            .map { it.map { converter.toObject(it) } }

    fun saveIntoDB(item: Address): Single<Address> = Single.just(item)
            .map { converter.toRealm(it) }
            .flatMap { repository.storeItem(it) }
            .map { converter.toObject(it) }

    fun removeFromDB(id: Int): Single<Boolean> = repository.removeItem(id)
}
