package com.softdesign.goodshop.data.repositories

import com.softdesign.goodshop.data.storage.realm.RealmComment
import com.softdesign.goodshop.data.storage.realm.RealmProduct
import com.softdesign.goodshop.utils.extentions.applyIf
import com.softdesign.goodshop.utils.extentions.auto
import com.softdesign.goodshop.utils.extentions.autoExecuteTransaction
import io.realm.Realm
import org.funktionale.option.toOption
import rx.Single
import rx.lang.kotlin.single

/**
 * @author Space
 * @date 17.12.2016
 */


class CommentRepository : Repository<RealmComment>() {

    fun getList(productId: String): Single<List<RealmComment>> = single {
        Realm.getDefaultInstance().auto {
            it.onSuccess(where(RealmComment::class.java)
                    .equalTo("productId", productId)
                    .findAll()
                    .toList())
        }
    }

    fun storeItem(productId: String, item: RealmComment): Single<RealmComment> = single {
        Realm.getDefaultInstance().autoExecuteTransaction {
            val realmItem = copyToRealmOrUpdate(item)
            it.onSuccess(where(RealmProduct::class.java)
                    .equalTo("id", productId)
                    .findFirst()
                    .toOption()
                    .map { it.comments.add(realmItem) }
                    .let { realmItem })
        }
    }

    fun storeItems(productId: String, items: List<RealmComment>): Single<List<RealmComment>> = single {
        Realm.getDefaultInstance().autoExecuteTransaction {
            items.filter { !it.active }.forEach { it.deleteFromRealm() }
            val updated = copyToRealmOrUpdate(items.filter { it.active })
            it.onSuccess(updated.applyIf(updated.isNotEmpty()) {
                where(RealmProduct::class.java)
                        .equalTo("id", productId)
                        .findFirst()
                        .toOption()
                        .map { it.comments.addAll(this) }
            })
        }
    }
}