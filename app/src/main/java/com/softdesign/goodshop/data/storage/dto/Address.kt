package com.softdesign.goodshop.data.storage.dto

import android.os.Parcel
import android.os.Parcelable

/**
 * @author Space
 * @date 08.12.2016
 */

data class Address(val hint: String,
                   val street: String,
                   val comment: String = "",
                   val house: String = "",
                   val flat: String = "",
                   val floor: String = "",
                   val id: Int = -1) : Parcelable {

    companion object {
        @JvmField val CREATOR: Parcelable.Creator<Address> = object : Parcelable.Creator<Address> {
            override fun createFromParcel(source: Parcel): Address = Address(source)
            override fun newArray(size: Int): Array<Address?> = arrayOfNulls(size)
        }
    }

    constructor(source: Parcel) : this(source.readString(), source.readString(), source.readString(), source.readString(), source.readString(), source.readString(), source.readInt())

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel?, flags: Int) {
        dest?.writeString(hint)
        dest?.writeString(street)
        dest?.writeString(comment)
        dest?.writeString(house)
        dest?.writeString(flat)
        dest?.writeString(floor)
        dest?.writeInt(id)
    }
}