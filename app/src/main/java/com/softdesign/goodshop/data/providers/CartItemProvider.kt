package com.softdesign.goodshop.data.providers

import com.softdesign.goodshop.common.App
import com.softdesign.goodshop.data.converters.CartItemConverter
import com.softdesign.goodshop.data.managers.PreferencesManager
import com.softdesign.goodshop.data.network.RestService
import com.softdesign.goodshop.data.repositories.CartItemRepository
import com.softdesign.goodshop.data.storage.dto.CartItem
import com.softdesign.goodshop.data.storage.realm.RealmCartItem
import com.softdesign.goodshop.utils.extentions.*
import rx.Observable
import rx.Single
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.properties.Delegates

/**
 * @author Space
 * @date 10.12.2016
 */


class CartItemProvider(private val converter: CartItemConverter,
                       private val repository: CartItemRepository,
                       private val service: RestService) {

    private var lastCartUpdate: String by Delegates.preference(PreferencesManager.LAST_CART_UPDATE, provideInitDate())

    fun getList(): Observable<List<CartItem>> = getListFromDB()
            .concatWith(getListFromRest(lastCartUpdate))

    fun getListFromDB(): Single<List<CartItem>> = repository.getList<RealmCartItem>()
            .map { it.map { converter.toObject(it) } }
            .delay(500, TimeUnit.MILLISECONDS)

    fun getListFromRest(lastUpdateTime: String = provideInitDate(),
                        dateString: String = Date().convertDate()): Single<List<CartItem>> =
            service.getCartItems(App.userId, lastUpdateTime)
                    .withAuthCheck()
                    .withConnectionStatusCheck()
                    .doOnSuccess { lastCartUpdate = dateString }
                    .map { it.map { converter.toRealm(it) } }
                    .flatMap { repository.storeRestItems(it) }
                    .map { it.map { converter.toObject(it) } }

    fun delete(cartItemId: String): Single<Unit> = repository.delete(cartItemId)

    fun saveListIntoDB(items: List<CartItem>): Single<List<CartItem>> = Single.just(items)
            .flatMap { repository.storeItems(it.map { converter.toRealm(it) }) }
            .map { it.map { converter.toObject(it) } }
}