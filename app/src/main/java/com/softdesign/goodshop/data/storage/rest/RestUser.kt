package com.softdesign.goodshop.data.storage.rest

import com.google.gson.annotations.SerializedName
import java.util.*

/**
 * @author Space
 * @date 30.03.2017
 */

data class RestUser(@SerializedName("_id") val id: String = "",
                    @SerializedName("fullName") val fullName: String = "",
                    @SerializedName("avatarUrl") val avatarUrl: String = "",
                    @SerializedName("token") val token: String = "",
                    @SerializedName("phone") val phone: String = "",
                    @SerializedName("addresses") val addresses: List<RestAddress> = ArrayList<RestAddress>())