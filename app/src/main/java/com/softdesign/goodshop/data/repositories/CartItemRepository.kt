package com.softdesign.goodshop.data.repositories

import com.softdesign.goodshop.data.storage.realm.RealmCartItem
import com.softdesign.goodshop.data.storage.realm.RealmProduct
import com.softdesign.goodshop.utils.extentions.autoExecuteTransaction
import io.realm.Realm
import rx.Single
import rx.lang.kotlin.single

/**
 * @author Space
 * @date 10.12.2016
 */


class CartItemRepository : Repository<RealmCartItem>() {

    fun storeRestItems(items: List<RealmCartItem>): Single<List<RealmCartItem>> = single {
        Realm.getDefaultInstance().autoExecuteTransaction {
            val newItems = items.map { it.update(where(RealmProduct::class.java).equalTo("id", it.id).findFirst()) }
                    .filter { it.product.id.isNotBlank() && it.product.active }
            copyToRealmOrUpdate(newItems)
            it.onSuccess(where(RealmCartItem::class.java).findAll().toList().filter { it.product.active })
        }
    }

    fun delete(cartItemId: String): Single<Unit> = single {
        Realm.getDefaultInstance().autoExecuteTransaction {
            it.onSuccess(where(RealmCartItem::class.java).equalTo("id", cartItemId).findFirst()?.deleteFromRealm() ?: Unit)
        }
    }
}