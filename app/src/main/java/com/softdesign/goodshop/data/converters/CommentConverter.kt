package com.softdesign.goodshop.data.converters

import com.softdesign.goodshop.data.storage.dto.Comment
import com.softdesign.goodshop.data.storage.realm.RealmComment
import com.softdesign.goodshop.data.storage.rest.RestComment
import com.softdesign.goodshop.utils.extentions.parseDate

/**
 * @author Space
 * @date 17.12.2016
 */

class CommentConverter {
    fun toRealm(productId: String, src: RestComment): RealmComment {
        val out = RealmComment()
        out.id = src.id
        out.productId = productId
        out.userName = src.userName
        out.avatar = src.avatar
        out.rating = src.rating.toInt()
        out.commentDate = parseDate(src.commentDate)
        out.comment = src.comment
        out.active = src.active
        return out
    }

    fun toRealm(src: Comment): RealmComment {
        val out = RealmComment()
        out.id = src.id
        out.productId = src.productId
        out.userName = src.userName
        out.avatar = src.avatar
        out.rating = src.rating
        out.commentDate = src.commentDate
        out.comment = src.comment
        out.active = src.active
        return out
    }

    fun toObject(productId: String, src: RestComment) = Comment(
            id = src.id,
            productId = productId,
            userName = src.userName,
            avatar = src.avatar,
            rating = src.rating.toInt(),
            commentDate = parseDate(src.commentDate),
            comment = src.comment,
            active = src.active)

    fun toObject(src: RealmComment) = Comment(
            id = src.id,
            productId = src.productId,
            userName = src.userName,
            avatar = src.avatar,
            rating = src.rating,
            commentDate = src.commentDate,
            comment = src.comment,
            active = src.active)
}