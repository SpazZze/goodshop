package com.softdesign.goodshop.data.network

import android.support.annotation.NonNull
import com.softdesign.goodshop.data.network.api.req.AddCommentReq
import com.softdesign.goodshop.data.network.api.req.AuthReq
import com.softdesign.goodshop.data.network.api.req.SocialAuthReq
import com.softdesign.goodshop.data.network.api.res.BaseResponse
import com.softdesign.goodshop.data.storage.rest.RestCartItem
import com.softdesign.goodshop.data.storage.rest.RestComment
import com.softdesign.goodshop.data.storage.rest.RestProduct
import com.softdesign.goodshop.data.storage.rest.RestUser
import retrofit2.http.*
import rx.Single

/**
 * @author Space
 * @date 11.12.2016
 */

interface RestService {

    @GET("products")
    fun getProducts(@Header("If-Modified-Since") lastUpdateTime: String): Single<List<RestProduct>>

    @GET("error/400")
    fun testError400(): Single<BaseResponse>

    @GET("error/500")
    fun testError500(): Single<BaseResponse>

    @GET("products/{productId}/comments/")
    fun getComments(@NonNull @Path("productId") productId: String): Single<List<RestComment>>

    @POST("products/{productId}/comments/")
    fun addComment(@NonNull @Path("productId") productId: String,
                   @NonNull @Body req: AddCommentReq): Single<RestComment>

    @DELETE("comments/{commentId}")
    fun deleteComment(@NonNull @Path("commentId") commentId: String): Single<BaseResponse>

    @POST("login")
    fun auth(@NonNull @Body req: AuthReq): Single<RestUser>

    @POST("socialLogin")
    fun socialAuth(@NonNull @Body req: SocialAuthReq): Single<RestUser>

    @GET("user/{userId}/favorite")
    fun getFavourites(@NonNull @Path("userId") userId: String,
                      @Header("If-Modified-Since") lastUpdateTime: String): Single<List<String>>

    @POST("user/{userId}/favorite")
    fun addFavourites(@NonNull @Path("userId") userId: String,
                      @NonNull @Body favouritesIdsList: List<String>)

    @DELETE("user/{userId}/favorite")
    fun removeFavourites(@NonNull @Path("userId") userId: String,
                         @NonNull @Body favouritesIdsList: List<String>)

    @GET("user/{userId}/cart")
    fun getCartItems(@NonNull @Path("userId") userId: String,
                     @Header("If-Modified-Since") lastUpdateTime: String): Single<List<RestCartItem>>

    @POST("user/{userId}/cart")
    fun addToCart(@NonNull @Path("userId") userId: String,
                  @NonNull @Body cartItemsList: List<RestCartItem>): Single<BaseResponse>

    @PUT("user/{userId}/cart")
    fun updateToCart(@NonNull @Path("userId") userId: String,
                     @NonNull @Body cartItemsList: List<RestCartItem>): Single<BaseResponse>

    @DELETE("user/{userId}/cart")
    fun removeFromCart(@NonNull @Path("userId") userId: String,
                       @NonNull @Body cartItemsList: List<RestCartItem>): Single<BaseResponse>
}
