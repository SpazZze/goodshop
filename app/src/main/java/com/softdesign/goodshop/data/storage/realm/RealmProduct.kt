package com.softdesign.goodshop.data.storage.realm

import com.softdesign.goodshop.utils.extentions.applyIf
import io.realm.RealmList
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import io.realm.annotations.RealmClass

/**
 * @author Space
 * @date 08.12.2016
 */

@RealmClass
open class RealmProduct : RealmObject() {
    @PrimaryKey
    open var id: String = ""
    open var productName: String = ""
    open var imageUrl: String = ""
    open var description: String = ""
    open var price: Int = 0
    open var rating: Float = 0.0f
    open var active: Boolean = true
    open var comments: RealmList<RealmComment> = RealmList()
    open var lastUpdated: String = ""
    open var isFavourite: Boolean = false
    open var count: Int = 1

    fun update(oldProduct: RealmProduct?) = applyIf(oldProduct != null) {
        this.isFavourite = oldProduct!!.isFavourite
        this.count = oldProduct.count
    }
}