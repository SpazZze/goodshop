package com.softdesign.goodshop.data.managers

import com.softdesign.goodshop.data.providers.*
import com.softdesign.goodshop.di.DaggerService
import com.softdesign.goodshop.di.components.DataManagerComponent
import com.softdesign.goodshop.di.modules.*
import javax.inject.Inject

/**
 * @author Space
 * @date 08.12.2016
 */

class DataManager {
    @Inject
    lateinit var cartItemProvider: CartItemProvider

    @Inject
    lateinit var productProvider: ProductProvider

    @Inject
    lateinit var addressProvider: AddressProvider

    @Inject
    lateinit var commentProvider: CommentProvider

    @Inject
    lateinit var userSettingsProvider: UserSettingsProvider

    init {
        DaggerService.getComponent(
                DataManagerComponent::class.java,
                NetworkModule::class.java,
                UserSettingsModule::class.java,
                CartModule::class.java,
                ProductsModule::class.java,
                CommentModule::class.java,
                AddressesModule::class.java).inject(this)
    }
}
