package com.softdesign.goodshop.data.storage.rest

import com.google.gson.annotations.SerializedName


/**
 * @author Space
 * @date 17.12.2016
 */

data class RestComment(@SerializedName("_id") val id: String = "",
                       @SerializedName("userName") val userName: String = "",
                       @SerializedName("avatar") val avatar: String = "",
                       @SerializedName("raiting") val rating: Float = 0f,
                       @SerializedName("commentDate") val commentDate: String = "",
                       @SerializedName("comment") val comment: String = "",
                       @SerializedName("active") val active: Boolean = false)