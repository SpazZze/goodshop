package com.softdesign.goodshop.data.repositories

import com.softdesign.goodshop.data.storage.realm.RealmAddress
import com.softdesign.goodshop.data.storage.realm.RealmUserSettings
import com.softdesign.goodshop.utils.extentions.auto
import com.softdesign.goodshop.utils.extentions.autoExecuteTransaction
import io.realm.Realm
import org.funktionale.option.Option
import org.funktionale.option.toOption
import rx.Single
import rx.lang.kotlin.single

/**
 * @author Space
 * @date 10.12.2016
 */

class UserSettingsRepository {

    fun get(): Single<Option<RealmUserSettings>> = single {
        Realm.getDefaultInstance().auto {
            it.onSuccess(where(RealmUserSettings::class.java)
                    .findFirst()
                    .toOption())
        }
    }

    fun storeItem(item: RealmUserSettings): Single<RealmUserSettings> = single {
        Realm.getDefaultInstance().autoExecuteTransaction {
            delete(RealmUserSettings::class.java)
            it.onSuccess(copyToRealmOrUpdate(item))
        }
    }

    fun clearUserData(): Single<Unit> = single {
        Realm.getDefaultInstance().autoExecuteTransaction {
            delete(RealmAddress::class.java)
            it.onSuccess(delete(RealmUserSettings::class.java))
        }
    }
}