<?xml version="1.0" encoding="utf-8"?>
<layout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    tools:ignore="contentDescription">

    <data>

        <variable
            name="viewModel"
            type="com.softdesign.goodshop.mvp.models.CartModel" />
    </data>

    <LinearLayout
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:animateLayoutChanges="true"
        android:orientation="horizontal">

        <FrameLayout
            android:layout_width="0dp"
            android:layout_height="match_parent"
            android:layout_weight="1">

            <TextView
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:layout_gravity="center"
                android:gravity="center"
                android:padding="@dimen/spacing_16"
                android:text="@{viewModel.noContentText}"
                android:visibility="@{viewModel.noContentTextVisibility}"
                tools:text="@string/no_content_cart_list" />

            <android.support.v4.widget.SwipeRefreshLayout
                android:layout_width="match_parent"
                android:layout_height="match_parent"
                android:enabled="@{!viewModel.isProgressBarVisible}"
                android:orientation="vertical"
                app:setCustomColors="@{true}"
                app:setOnRefreshListener="@{viewModel::refreshItems}"
                app:setRefreshing="@{viewModel.isRefreshing}">

                <android.support.v7.widget.RecyclerView
                    android:id="@+id/recycler"
                    android:layout_width="match_parent"
                    android:layout_height="match_parent"
                    android:clipToPadding="false"
                    android:orientation="horizontal"
                    app:bindingHelper="@{viewModel.bindingHelper}"
                    app:items="@{viewModel.items}"
                    app:layoutManager="LinearLayoutManager"
                    tools:listitem="@layout/item_cart" />

            </android.support.v4.widget.SwipeRefreshLayout>

            <ProgressBar
                style="@style/ProgressBar"
                android:visibility="@{viewModel.isProgressBarVisible}" />

        </FrameLayout>

        <!--Footer-->
        <View
            android:layout_width="@dimen/line_size_1"
            android:layout_height="match_parent"
            android:background="#12000000" />

        <LinearLayout
            android:layout_width="wrap_content"
            android:layout_height="match_parent"
            android:background="#fff"
            android:gravity="bottom"
            android:orientation="vertical"
            android:padding="@dimen/spacing_16"
            android:visibility="@{viewModel.isFooterVisible}">

            <!--Quantity-->
            <LinearLayout
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:orientation="horizontal">

                <TextView
                    android:layout_width="wrap_content"
                    android:layout_height="wrap_content"
                    android:maxLines="1"
                    android:paddingRight="@dimen/spacing_8"
                    android:text="@string/header.products_quantity"
                    android:textColor="@color/colorTextSecondary"
                    android:textSize="@dimen/font_14"
                    tools:ignore="RtlSymmetry" />

                <TextView
                    android:layout_width="0dp"
                    android:layout_height="wrap_content"
                    android:layout_weight="1"
                    android:gravity="end"
                    android:maxLines="1"
                    android:text="@{@plurals/count(viewModel.totalCount, viewModel.totalCount)}"
                    android:textAlignment="textEnd"
                    android:textColor="@color/colorAccent"
                    android:textSize="@dimen/font_14"
                    app:font="@{@string/font_roboto_medium}"
                    tools:fontFamily="sans-serif-medium"
                    tools:text="2 шт." />

            </LinearLayout>

            <!--Discount-->
            <LinearLayout
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:layout_marginTop="@dimen/spacing_8"
                android:orientation="horizontal">

                <TextView
                    android:layout_width="wrap_content"
                    android:layout_height="wrap_content"
                    android:maxLines="1"
                    android:paddingRight="@dimen/spacing_8"
                    android:text="@string/header.discount"
                    android:textColor="@color/colorTextSecondary"
                    android:textSize="@dimen/font_14"
                    tools:ignore="RtlSymmetry" />

                <TextView
                    android:layout_width="0dp"
                    android:layout_height="wrap_content"
                    android:layout_weight="1"
                    android:gravity="end"
                    android:maxLines="1"
                    android:text="@{@plurals/price(viewModel.totalDiscount, viewModel.totalDiscount)}"
                    android:textAlignment="textEnd"
                    android:textColor="@color/colorAccent"
                    android:textSize="@dimen/font_14"
                    app:font="@{@string/font_roboto_medium}"
                    tools:fontFamily="sans-serif-medium"
                    tools:text="567.-" />

            </LinearLayout>

            <!--Total cost-->
            <LinearLayout
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:layout_marginTop="@dimen/spacing_8"
                android:orientation="horizontal">

                <TextView
                    android:layout_width="wrap_content"
                    android:layout_height="wrap_content"
                    android:maxLines="1"
                    android:paddingRight="@dimen/spacing_8"
                    android:text="@string/header.totalCost"
                    android:textColor="@color/colorTextSecondary"
                    android:textSize="@dimen/font_14"
                    app:font="@{@string/font_roboto_medium}"
                    tools:fontFamily="sans-serif-medium"
                    tools:ignore="RtlSymmetry" />

                <TextView
                    android:layout_width="0dp"
                    android:layout_height="wrap_content"
                    android:layout_weight="1"
                    android:gravity="end"
                    android:maxLines="1"
                    android:text="@{@plurals/price(viewModel.totalCost, viewModel.totalCost)}"
                    android:textAlignment="textEnd"
                    android:textColor="@color/colorAccent"
                    android:textSize="@dimen/font_14"
                    app:font="@{@string/font_roboto_medium}"
                    tools:fontFamily="sans-serif-medium"
                    tools:text="10567.-" />

            </LinearLayout>

            <android.support.v7.widget.AppCompatButton
                style="@style/CustomButton"
                android:layout_gravity="center_horizontal"
                android:layout_marginTop="@dimen/spacing_16"
                android:background="@drawable/selector_button"
                android:clickable="@{!viewModel.isProgressBarVisible}"
                android:enabled="@{!viewModel.isProgressBarVisible}"
                android:onClick="@{() -> viewModel.onOrderClick()}"
                android:text="@string/header.make_order"
                android:textColor="@android:color/white"
                android:theme="@style/AppTheme.Button.Accent" />

        </LinearLayout>

    </LinearLayout>

</layout>